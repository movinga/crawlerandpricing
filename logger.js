var winston = require('winston');

var logger = new winston.Logger({
  transports: [
    new winston.transports.File({
      name: 'info-file',
      filename: __dirname +  '/logs/info.log',
      level: 'info'
    }),
    new winston.transports.File({
      name: 'warning-file',
      filename: __dirname + '/logs/error.log',
      level: 'warn'
    }),
    new winston.transports.File({
      name: 'debug-file',
      filename: __dirname + '/logs/debug.log',
      level: 'debug'
    })
  ]
});

module.exports = logger;
