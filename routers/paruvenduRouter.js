var express = require('express');
var ParuvenduCrawler = require('../crawlers/paruvenduCrawler');
var ParuvenduModels = require('../models/paruvenduModels');
var fs = require('fs');
var logger = require('../logger.js');
require('../helpers.js');

var paruvenduRouter = express.Router();

paruvenduRouter.post('/start', function(req, res) {
  ParuvenduCrawler.startCrawl();
  req.flash('info', 'You have invoked the Paruvendu crawler. It will take approx. 24 hours to complete.')
  res.redirect('/status#paruvendu');
});

paruvenduRouter.post('/stop', function(req, res) {
  ParuvenduCrawler.stopCrawl();
  req.flash('info', 'You have stopped the Paruvendu crawler.')
  res.redirect('/status#paruvendu');
});

paruvenduRouter.post('/pause', function(req, res) {
  ParuvenduCrawler.pauseCrawl();
  req.flash('info', 'You have paused the Paruvendu crawler.')
  res.redirect('/status#paruvendu');
});

paruvenduRouter.post('/resume', function(req, res) {
  ParuvenduCrawler.resumeCrawl();
  req.flash('info', 'You have resumed the Paruvendu crawler.')
  res.redirect('/status#paruvendu');
});

paruvenduRouter.post('/report.csv', function(req, res) {
  var filename = __dirname + '/paruvendu-report.csv'
  logger.info('Requested a Paruvendu property report for crawling session %d.', req.body.sessionId);
  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });
  ParuvenduModels.CrawlSession.findById(req.body.sessionId).then(function(session) {
    var writeStream = fs.createWriteStream(filename);
    writeStream.write("Postcode;City;Property type;Price, €;Area, m2;Number of bedrooms\n");
    session.getProperties({raw: true}).then(function(properties) {
      properties.forEach(function(property) {
        writeStream.write("{0};{1};{2};{3};{4};{5}\n".format(
          property.postcode,
          property.city,
          property.type,
          property.price,
          property.area,
          property.bedrooms
        ));
      });
      writeStream.end();
      writeStream.on('finish', function() {
        res.sendFile(filename, function() {
          fs.unlinkSync(filename);
        });
      });
    });
  });
});

module.exports = paruvenduRouter;
