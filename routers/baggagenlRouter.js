var express = require('express');
var BaggagenlCrawler = require('../crawlers/baggagenlCrawler');
var BaggagenlModels = require('../models/baggagenlModels');
var fs = require('fs');
var logger = require('../logger.js');
var async = require('async');
require('../helpers.js');

var baggagenlRouter = express.Router();

baggagenlRouter.post('/start', function(req, res) {
  BaggagenlCrawler.startCrawl();
  req.flash('info', 'You have invoked the Baggage.nl crawler. It will take approx. 1 hour to complete.')
  res.redirect('/status#baggagenl');
});

baggagenlRouter.post('/stop', function(req, res) {
  BaggagenlCrawler.stopCrawl();
  req.flash('info', 'You have stopped the Baggage.nl crawler.')
  res.redirect('/status#baggagenl');
});

baggagenlRouter.post('/pause', function(req, res) {
  BaggagenlCrawler.pauseCrawl();
  req.flash('info', 'You have paused the Baggage.nl crawler.')
  res.redirect('/status#baggagenl');
});

baggagenlRouter.post('/resume', function(req, res) {
  BaggagenlCrawler.resumeCrawl();
  req.flash('info', 'You have resumed the Baggage.nl crawler.')
  res.redirect('/status#baggagenl');
});

baggagenlRouter.post('/report.csv', function(req, res) {
  var filename = __dirname + '/baggagenl-report.csv'
  logger.info('Requested a Baggage.nl freight rates report for crawling session %d.', req.body.sessionId);

  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });

  BaggagenlModels.CrawlSession.findById(req.body.sessionId).then(function(session) {
    async.series([
      function(callback) {
        if (req.body.hasOwnProperty('sea')) {
          console.log('Selecting Sea rates!')
          session.getSeaRates({raw: true}).then(function(seaRates) {
            seaRates.forEach(function(rate) {
              rate.rateType = 'Sea';
            });
            callback(null, seaRates);
          });
        } else {
          callback(null, []);
        }
      },
      function(callback) {
        if (req.body.hasOwnProperty('air')) {
          console.log('Selecting Air rates!')
          session.getAirRates({raw: true}).then(function(airRates) {
            airRates.forEach(function(rate) {
              rate.rateType = 'Air';
            });
            callback(null, airRates);
          });
        } else {
          callback(null, []);
        }
      },
      function(callback) {
        if (req.body.hasOwnProperty('courier')) {
          console.log('Selecting Courier rates!')
          session.getCourierRates({raw: true}).then(function(courierRates) {
            courierRates.forEach(function(rate) {
              rate.rateType = 'Courier';
            });
            callback(null, courierRates);
          });
        } else {
          callback(null, []);
        }
      },
      function(callback) {
        if (req.body.hasOwnProperty('road')) {
          console.log('Selecting Road rates!')
          session.getRoadRates({raw: true}).then(function(roadRates) {
            roadRates.forEach(function(rate) {
              rate.rateType = 'Road';
            });
            callback(null, roadRates);
          });
        } else {
          callback(null, []);
        }
      }
    ],
    function(err, results) {
      var freightRates = results[0].concat(results[1], results[2], results[3]);
      var writeStream = fs.createWriteStream(filename);
      writeStream.write("Rate type;Country;Zone;Weight, kg (Volume, m3 for Sea);Value;Rate, €;\n");
      freightRates.forEach(function(rate) {
        writeStream.write("{0};{1};{2};{3};{4};{5}\n".format(
          rate.rateType,
          rate.country,
          rate.zone,
          rate.hasOwnProperty('weight') ? rate.weight : rate.volume,
          rate.value,
          rate.rate
        ));
      });
      writeStream.end();
      writeStream.on('finish', function() {
        res.sendFile(filename, function() {
          fs.unlinkSync(filename);
        });
      });
    });
  });
});

module.exports = baggagenlRouter;
