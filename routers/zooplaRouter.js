var express = require('express');
var ZooplaCrawler = require('../crawlers/zooplaCrawler');
var ZooplaModels = require('../models/zooplaModels');
var fs = require('fs');
var logger = require('../logger.js');
require('../helpers.js');

var zooplaRouter = express.Router();

zooplaRouter.post('/start', function(req, res) {
  ZooplaCrawler.startCrawl();
  req.flash('info', 'You have invoked the Zoopla crawler. It will take approx. 2 hours to complete.')
  res.redirect('/status#zoopla');
});

zooplaRouter.post('/stop', function(req, res) {
  ZooplaCrawler.stopCrawl();
  req.flash('info', 'You have stopped the Zoopla crawler.')
  res.redirect('/status#zoopla');
});

zooplaRouter.post('/pause', function(req, res) {
  ZooplaCrawler.pauseCrawl();
  req.flash('info', 'You have paused the Zoopla crawler.')
  res.redirect('/status#zoopla');
});

zooplaRouter.post('/resume', function(req, res) {
  ZooplaCrawler.resumeCrawl();
  req.flash('info', 'You have resumed the Zoopla crawler.')
  res.redirect('/status#zoopla');
});

zooplaRouter.post('/report.csv', function(req, res) {
  var filename = __dirname + '/zoopla-report.csv'
  logger.info('Requested a Zoopla area stats report for crawling session %d.', req.body.sessionId);
  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });
  //Select Sessionid with the specified id
  var writeStream = fs.createWriteStream(filename);
  writeStream.write("Postcode;Property type;Avg. current value;Avg. £ per sq ft.;Avg. area in sq ft.;Avg. # beds;Sample size\n");
  ZooplaModels.sequelize.query(
    'SELECT * FROM area_stats AS t1 JOIN area_sample_sizes AS t2 ON t1.postcode = t2.postcode AND t1.session_id = {0} AND t2.session_id = {0}'.format(req.body.sessionId)
  ).then(function(companies) {
    //console.log(companies[0][0])
    companies[0].forEach(function(company) {
      console.log(company);
      var outputString = [
        company.postcode,
        company.type,
        company['total_price'],
        company['price_sq_feet'],
        company.area,
        company.bedrooms,
        ''
      ];
      switch (company.type) {
        case 'Flats':
          outputString[6] = company.flats;
        break;
        case 'Terraced':
          outputString[6] = company.terraced;
        break;
        case 'Semi-detached':
          outputString[6] = company['semi_detached'];
        break;
        case 'Detached':
          outputString[6] = company.detached;
        break;
      }
      writeStream.write("{0};{1};{2};{3};{4};{5};{6}\n".format(
        outputString[0],
        outputString[1],
        outputString[2],
        outputString[3],
        outputString[4],
        outputString[5],
        outputString[6]
      ));
    });
    writeStream.end();
    writeStream.on('finish', function() {
      res.sendFile(filename, function() {
        fs.unlinkSync(filename);
      });
    });
  });
});

zooplaRouter.post('/sample-size-report.csv', function(req, res) {
  var filename = __dirname + '/zoopla-sample-size.csv'

  logger.info('Requested a Zoopla sample size report for crawling session %d.', req.body.sessionId);
  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });
  //Select Sessionid with the specified id
  var writeStream = fs.createWriteStream(filename);
  writeStream.write("Postcode;Flats;Terraced;Semi-detached;Detached;Unknown\n");
  ZooplaModels.sequelize.query(
    'SELECT * FROM area_sample_sizes WHERE session_id = {0}'.format(req.body.sessionId)
  ).then(function(sampleSizes) {
    //console.log(companies[0][0])
    sampleSizes[0].forEach(function(sampleSize) {
      writeStream.write("{0};{1};{2};{3};{4};{5}\n".format(
        sampleSize.postcode,
        sampleSize.flats,
        sampleSize.terraced,
        sampleSize['semi_detached'],
        sampleSize.detached,
        sampleSize.unknown
      ));
    });
    writeStream.end();
    writeStream.on('finish', function() {
      res.sendFile(filename, function() {
        fs.unlinkSync(filename);
      });
    });
  });
});

module.exports = zooplaRouter;
