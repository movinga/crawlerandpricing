var express = require('express');
var IAMCrawler = require('../crawlers/iamCrawler');
var IAMModels = require('../models/iamModels');
var fs = require('fs');
require('../helpers.js');

var iamRouter = express.Router();

iamRouter.post('/start', function(req, res) {
  IAMCrawler.startCrawl();
  req.flash('info', 'You have invoked the IAM crawler. It will take approx. 30 minutes to complete.')
  res.redirect('/status#iam')
});

iamRouter.post('/stop', function(req, res) {
  IAMCrawler.stopCrawl();
  req.flash('info', 'You have stopped the IAM crawler')
  res.redirect('/status#iam')
});

iamRouter.post('/pause', function(req, res) {
  IAMCrawler.pauseCrawl();
  req.flash('info', 'You have paused the IAM crawler.')
  res.redirect('/status#iam')
});

iamRouter.post('/resume', function(req, res) {
  IAMCrawler.resumeCrawl();
  req.flash('info', 'You have resumed the IAM crawler.')
  res.redirect('/status#iam')
});

iamRouter.post('/report.csv', function(req, res) {
  var filename = __dirname + '/iam-output.csv';

  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });
  //Select Sessionid with the specified id
  IAMModels.CrawlSession.findById(req.body.sessionId).then(function(session) {
    var writeStream = fs.createWriteStream(filename);
    writeStream.write("Country;Company name;Membership;Affiliations;Mailing Address;Work Address;Work Phone;Mobile Phone;Fax;Email;Website;Skype;Description;Business Types;Licenses;Contact Persons\n");
    session.getCompanies({raw: true}).then(function(companies) {
      companies.forEach(function(company) {
        writeStream.write("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15}\n".format(
          company.country,
          company.name,
          company.membership,
          company.affiliations,
          company.mailingAddress,
          company.workAddress,
          company.workPhone,
          company.mobilePhone,
          company.fax,
          company.email,
          company.website,
          company.skype,
          company.description,
          company.businessTypes,
          company.licenses,
          company.contactPersons
        ));
      });
      writeStream.end();
      writeStream.on('finish', function() {
        res.sendFile(filename, function() {
          fs.unlinkSync(filename);
        });
      });
    });
  });
});

module.exports = iamRouter;
