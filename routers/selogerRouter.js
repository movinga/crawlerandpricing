var express = require('express');
var SelogerCrawler = require('../crawlers/selogerCrawler');
var SelogerModels = require('../models/selogerModels');
var fs = require('fs');
var logger = require('../logger.js');
require('../helpers.js');

var selogerRouter = express.Router();

selogerRouter.post('/start', function(req, res) {
  SelogerCrawler.startCrawl();
  req.flash('info', 'You have invoked the Seloger crawler. It will take approx. 24 hours to complete.')
  res.redirect('/status#seloger');
});

selogerRouter.post('/stop', function(req, res) {
  SelogerCrawler.stopCrawl();
  req.flash('info', 'You have stopped the Seloger crawler.')
  res.redirect('/status#seloger');
});

selogerRouter.post('/pause', function(req, res) {
  SelogerCrawler.pauseCrawl();
  req.flash('info', 'You have paused the Seloger crawler.')
  res.redirect('/status#seloger');
});

selogerRouter.post('/resume', function(req, res) {
  SelogerCrawler.resumeCrawl();
  req.flash('info', 'You have resumed the Seloger crawler.')
  res.redirect('/status#seloger');
});

selogerRouter.post('/report.csv', function(req, res) {
  var filename = __dirname + '/seloger-report.csv'
  logger.info('Requested a Seloger property report for crawling session %d.', req.body.sessionId);
  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });
  SelogerModels.CrawlSession.findById(req.body.sessionId).then(function(session) {
    var writeStream = fs.createWriteStream(filename);
    writeStream.write("Postcode;City;Property type;Price, €;Area, m2;Number of bedrooms\n");
    session.getProperties({raw: true}).then(function(properties) {
      properties.forEach(function(property) {
        writeStream.write("{0};{1};{2};{3};{4};{5}\n".format(
          property.postcode,
          property.city,
          property.type,
          property.price,
          property.area,
          property.bedrooms
        ));
      });
      writeStream.end();
      writeStream.on('finish', function() {
        res.sendFile(filename, function() {
          fs.unlinkSync(filename);
        });
      });
    });
  });
});

module.exports = selogerRouter;
