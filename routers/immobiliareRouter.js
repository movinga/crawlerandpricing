var express = require('express');
var ImmobiliareCrawler = require('../crawlers/immobiliareCrawler');
var ImmobiliareModels = require('../models/immobiliareModels');
var fs = require('fs');
var logger = require('../logger.js');
require('../helpers.js');

var immobiliareRouter = express.Router();

immobiliareRouter.post('/start', function(req, res) {
  ImmobiliareCrawler.startCrawl();
  req.flash('info', 'You have invoked the Immobiliare crawler. It will take approx. 6 hours to complete.')
  res.redirect('/status#immobiliare');
});

immobiliareRouter.post('/stop', function(req, res) {
  ImmobiliareCrawler.stopCrawl();
  req.flash('info', 'You have stopped the Immobiliare crawler.')
  res.redirect('/status#immobiliare');
});

immobiliareRouter.post('/pause', function(req, res) {
  ImmobiliareCrawler.pauseCrawl();
  req.flash('info', 'You have paused the Immobiliare crawler.')
  res.redirect('/status#immobiliare');
});

immobiliareRouter.post('/resume', function(req, res) {
  ImmobiliareCrawler.resumeCrawl();
  req.flash('info', 'You have resumed the Immobiliare crawler.')
  res.redirect('/status#immobiliare');
});

immobiliareRouter.post('/report.csv', function(req, res) {
  var filename = __dirname + '/immobiliare-report.csv'
  logger.info('Requested a Immobiliare area stats report for crawling session %d.', req.body.sessionId);
  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });
  ImmobiliareModels.CrawlSession.findById(req.body.sessionId).then(function(session) {
    var writeStream = fs.createWriteStream(filename);
    writeStream.write("Zone;Commune;Province;Property type;Price;Area;Number of bedrooms\n");
    session.getProperties({raw: true}).then(function(properties) {
      properties.forEach(function(property) {
        writeStream.write("{0};{1};{2};{3};{4};{5};{6}\n".format(
          property.zone ? property.zone : "",
          property.commune,
          property.province,
          property.type,
          property.price,
          property.area,
          property.bedrooms
        ));
      });
      writeStream.end();
      writeStream.on('finish', function() {
        res.sendFile(filename, function() {
          fs.unlinkSync(filename);
        });
      });
    });
  });
});

module.exports = immobiliareRouter;
