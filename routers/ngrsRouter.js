var express = require('express');
var NGRSCrawler = require('../crawlers/ngrsCrawler');
var NGRSModels = require('../models/ngrsModels');
var fs = require('fs');
require('../helpers.js');

var ngrsRouter = express.Router();

ngrsRouter.post('/start', function(req, res) {
  NGRSCrawler.startCrawl();
  req.flash('info', 'You have invoked the NGRS crawler. It will take approx. 10 minutes to complete.')
  res.redirect('/status#ngrs')
});

ngrsRouter.post('/stop', function(req, res) {
  NGRSCrawler.stopCrawl();
  req.flash('info', 'You have stopped the NGRS crawler.')
  res.redirect('/status#ngrs')
});

ngrsRouter.post('/pause', function(req, res) {
  NGRSCrawler.pauseCrawl();
  req.flash('info', 'You have paused the NGRS crawler.')
  res.redirect('/status#ngrs')
});

ngrsRouter.post('/resume', function(req, res) {
  NGRSCrawler.resumeCrawl();
  req.flash('info', 'You have resumed the NGRS crawler.')
  res.redirect('/status#ngrs')
});

ngrsRouter.post('/report.csv', function(req, res) {
  var filename = __dirname + '/ngrs-report.csv';
  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });
  //Select Sessionid with the specified id
  NGRSModels.CrawlSession.findById(req.body.sessionId).then(function(session) {
    var writeStream = fs.createWriteStream(filename);
    writeStream.write("Company name;Address 1;Address 2;Address 3;Address 4;Town;County;Postcode;Telephone number;Website;Email address\n");
    session.getCompanies({raw: true}).then(function(companies) {
      companies.forEach(function(company) {
        writeStream.write("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10}\n".format(
          company.name      ? company.name      : "",
          company.address1  ? company.address1  : "",
          company.address2  ? company.address2  : "",
          company.address3  ? company.address3  : "",
          company.address4  ? company.address4  : "",
          company.town      ? company.town      : "",
          company.county    ? company.county    : "",
          company.postcode  ? company.postcode  : "",
          company.phone     ? company.phone     : "",
          company.website   ? company.website   : "",
          company.email     ? company.email     : ""
        ));
      });
      writeStream.end();
      writeStream.on('finish', function() {
        res.sendFile(filename, function() {
          fs.unlinkSync(filename);
        });
      });
    });
  });
});

module.exports = ngrsRouter;
