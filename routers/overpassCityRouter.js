var express = require('express');
var OPCityCrawler = require('../crawlers/overpassCityCrawler');
var logger = require('../logger.js');
require('../helpers.js');

var opCityRouter = express.Router();

opCityRouter.post('/start', function(req, res) {
  req.flash('info', 'You have invoked the Overpass city crawler (required for FEDEMAC). It will take approx. 30 minutes to complete.');
  OPCityCrawler.startCrawl(function() {
    logger.info('Overpass crawling complete!');
  });
  res.redirect('/status#overpass');
});

opCityRouter.post('/stop', function(req, res) {
  req.flash('You have stopped the Overpass city crawler.');
  OPCityCrawler.stopCrawl();
  res.redirect('/status#overpass');
});

opCityRouter.post('/pause', function(req, res) {
  req.flash('You have paused the Overpass city crawler.');
  OPCityCrawler.pauseCrawl();
  res.redirect('/status#overpass');
});

opCityRouter.post('/resume', function(req, res) {
  req.flash('You have resumed the Overpass city crawler.');
  OPCityCrawler.resumeCrawl();
  res.redirect('/status#overpass');
});

module.exports = opCityRouter;
