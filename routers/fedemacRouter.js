var express = require('express');
var FEDEMACCrawler = require('../crawlers/fedemacCrawler');
var FEDEMACModels = require('../models/fedemacModels');
var fs = require('fs');
var logger = require('../logger.js');
require('../helpers.js');

var fedemacRouter = express.Router();

fedemacRouter.post('/start', function(req, res) {
  logger.info('Invoked FEDEMAC crawler manually.');
  FEDEMACCrawler.startCrawl();
  req.flash('info', 'You have invoked the FEDEMAC crawler. It will take approx. 15 minutes to complete.')
  res.redirect('/status#fedemac');
});

fedemacRouter.post('/stop', function(req, res) {
  FEDEMACCrawler.stopCrawl();
  req.flash('info', 'You have stopped the FEDEMAC crawler.')
  res.redirect('/status#fedemac');
});

fedemacRouter.post('/pause', function(req, res) {
  FEDEMACCrawler.pauseCrawl();
  req.flash('info', 'You have paused the FEDEMAC crawler.')
  res.redirect('/status#fedemac');
});

fedemacRouter.post('/resume', function(req, res) {
  FEDEMACCrawler.resumeCrawl();
  req.flash('info', 'You have resumed the FEDEMAC crawler.')
  res.redirect('/status#fedemac');
});

fedemacRouter.post('/report.csv', function(req, res) {
  var filename = __dirname + '/fedemac-report.csv'

  logger.info('Requested a FEDEMAC report for crawling session %d.', req.body.sessionId);
  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });
  //Select Sessionid with the specified id
  FEDEMACModels.CrawlSession.findById(req.body.sessionId).then(function(session) {
    var writeStream = fs.createWriteStream(filename);
    writeStream.write("Company name;Street;City;Country;Mobile Phone;Work Phone;Email;Website;Services;\n");
    session.getCompanies({raw: true}).then(function(companies) {
      companies.forEach(function(company) {
        writeStream.write("{0};{1};{2};{3};{4};{5};{6};{7};{8}\n".format(
          company.name,
          company.street,
          company.city,
          company.country,
          company.mobilePhone,
          company.workPhone,
          company.email,
          company.website,
          company.services
        ));
      });
      writeStream.end();
      writeStream.on('finish', function() {
        res.sendFile(filename, function() {
          fs.unlinkSync(filename);
        });
      });
    });
  });
});

module.exports = fedemacRouter;
