var express = require('express');
var BHubCrawler = require('../crawlers/bHubCrawler');
var BHubModels = require('../models/bHubModels');
var fs = require('fs');
var logger = require('../logger.js');
require('../helpers.js');

var bHubRouter = express.Router();

bHubRouter.post('/start', function(req, res) {
  BHubCrawler.startCrawl();
  req.flash('info', 'You have invoked the Baggagehub.com crawler. It will take approx. 2 hours to complete.')
  res.redirect('/status#bhub');
});

bHubRouter.post('/stop', function(req, res) {
  BHubCrawler.stopCrawl();
  req.flash('info', 'You have stopped the Baggagehub.com crawler.')
  res.redirect('/status#bhub');
});

bHubRouter.post('/pause', function(req, res) {
  BHubCrawler.pauseCrawl();
  req.flash('info', 'You have paused the Baggagehub.com crawler.')
  res.redirect('/status#bhub');
});

bHubRouter.post('/resume', function(req, res) {
  BHubCrawler.resumeCrawl();
  req.flash('info', 'You have resumed the Baggagehub.com crawler.')
  res.redirect('/status#bhub');
});

bHubRouter.post('/report.csv', function(req, res) {
  var filename = __dirname + '/baggagehub-report.csv'
  logger.info('Requested a Baggagehub.com freight rates report for crawling session %d.', req.body.sessionId);
  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });
  BHubModels.CrawlSession.findById(req.body.sessionId).then(function(session) {
    var writeStream = fs.createWriteStream(filename);
    writeStream.write("Country From;City from;Country To;City To;Number of boxes;Freight type;Delivery time;Price, £\n");
    session.getRates({raw: true}).then(function(rates) {
      rates.forEach(function(rate) {
        writeStream.write("{0};{1};{2};{3};{4};{5};{6};{7}\n".format(
          rate.countryFrom,
          rate.cityFrom,
          rate.countryTo,
          rate.cityTo,
          rate.numberOfBoxes,
          rate.rateType,
          rate.rateTime,
          rate.rate
        ));
      });
      writeStream.end();
      writeStream.on('finish', function() {
        res.sendFile(filename, function() {
          fs.unlinkSync(filename);
        });
      });
    });
  });
});

module.exports = bHubRouter;
