var express = require('express');
var ADACCrawler = require('../crawlers/adacCrawler');
var ADACModels = require('../models/adacModels');
var fs = require('fs');
var logger = require('../logger.js');
var async = require('async');
require('../helpers.js');

var ADACRouter = express.Router();

ADACRouter.post('/start', function(req, res) {
  ADACCrawler.startCrawl();
  req.flash('info', 'You have invoked the ADAC crawler. It will take approx. 3 hours to complete.')
  res.redirect('/status#adac');
});

ADACRouter.post('/stop', function(req, res) {
  ADACCrawler.stopCrawl();
  req.flash('info', 'You have stopped the ADAC crawler.')
  res.redirect('/status#adac');
});

ADACRouter.post('/pause', function(req, res) {
  ADACCrawler.pauseCrawl();
  req.flash('info', 'You have paused the ADAC crawler.')
  res.redirect('/status#adac');
});

ADACRouter.post('/resume', function(req, res) {
  ADACCrawler.resumeCrawl();
  req.flash('info', 'You have resumed the ADAC crawler.')
  res.redirect('/status#adac');
});

ADACRouter.post('/report.csv', function(req, res) {
  console.log(req.body);
  var filename = __dirname + '/adac-report.csv'
  logger.info('Requested an ADAC car info report');

  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });

  var writeStream = fs.createWriteStream(filename);
  var header = [];
  if (req.body.hasOwnProperty('Modell')) {
    header.push('Name', 'Typ', 'HSN/TSN', 'Neuwagenpreis');
  }
  if (req.body.hasOwnProperty('Allgemein')) {
    header.push('Marke', 'Modell', 'Typ', 'Baureihe', 'Herstellerinterne Baureihenbezeichnung', 'Modellstart', 'Modellende', 'Baureihenstart', 'Baureihenende', 'HSN Schlüsselnummer', 'TSN Schlüsselnummer', 'TSN Schlüsselnummer 2', 'KFZ-Steuer pro Jahr', 'CO2-Effizienzklasse', 'Grundpreis');
  }
  if (req.body.hasOwnProperty('MotorUndAntrieb')) {
    header.push('Motorart', 'Kraftstoffart', 'Kraftstoffart (2.Antrieb)', 'Abgasreinigung', 'Motorbauart', 'Anzahl Zylinder', 'Gemischaufbereitung', 'Aufladung', 'Anzahl Ventile', 'Hubraum', 'Leistung in kW', 'Leistung in PS', 'Leistung maximal bei U/min', 'Drehmoment', 'Drehmoment maximal bei U/min.', 'Antriebsart', 'Getriebeart', 'Anzahl Gänge', 'Start-/Stopp-Automatik', 'Schaltpunktanzeige', 'Schadstoffklasse');
  }
  if (req.body.hasOwnProperty('MasseUndGewichte')) {
    header.push('Länge', 'Breite', 'Höhe', 'Radstand', 'Bodenfreiheit maximal', 'Wendekreis', 'Böschungswinkel vorne', 'Böschungswinkel hinten', 'Rampenwinkel', 'Wattiefe', 'Steigung maximal', 'Kofferraumvolumen normal', 'Kofferraumvolumen fensterhoch mit umgeklappter Rücksitzbank', 'Kofferraumvolumen dachhoch mit umgeklappter Rücksitzbank', 'Rücksitzbank umklappbar', 'Leergewicht (EU)', 'Zul. Gesamtgewicht', 'Zuladung', 'Anhängelast gebremst 12%', 'Anhängelast ungebremst', 'Gesamtzuggewicht', 'Stützlast', 'Dachlast');
  }
  if (req.body.hasOwnProperty('KarosserieUndFahrwerk')) {
    header.push('Karosserie', 'Türanzahl', 'Anzahl der Schiebetüren serienmäßig/auf Wunsch', 'Fahrzeugklasse', 'Sitzanzahl', 'Sitzanzahl maximal', 'Federung vorne', 'Federung hinten', 'Servolenkung', 'Bremse vorne', 'Bremse hinten', 'Reifengröße', 'Reifengröße hinten (abweichend)', 'Reifendruckkontrolle', 'Reifendruckkontrolle - Bezeichnung', 'Runflat', 'Runflat - Bezeichnung');
  }
  if (req.body.hasOwnProperty('MesswerteHersteller')) {
    header.push('Beschleunigung', 'Höchstgeschwindigkeit', 'Verbrauch Innerorts', 'Verbrauch Innerorts (2.Antrieb)', 'Verbrauch Außerorts', 'Verbrauch Außerorts (2.Antrieb)', 'Verbrauch Gesamt', 'Verbrauch Gesamt (2.Antrieb)', 'CO2-Ausstoss', 'Tankgröße', 'Tankgröße (2.Antrieb)', 'Tankeinbauort');
  }
  if (req.body.hasOwnProperty('Sicherheitsausstattung')) {
    header.push('Airbag Fahrer', 'Airbag Beifahrer', 'Seitenairbag vorne', 'Seitenairbag vorne - Bezeichnung', 'Seitenairbag hinten', 'Seitenairbag hinten - Bezeichnung', 'Kopfairbag vorne', 'Kopfairbag vorne - Bezeichnung', 'Kopfairbag hinten', 'Kopfairbag hinten - Bezeichnung', 'Airbag Sonstige', 'Airbag Sonstige - Bezeichnung', 'Airbag Deaktivierung', 'Airbag Deaktivierung - Bezeichnung', 'PreCrash-System', 'PreCrash-System - Bezeichnung', '3-Punkt-Gurt hinten Mitte', '3-Punkt-Gurt hinten Mitte - Bezeichnung', 'Kopfstützen hinten', 'Kopfstützen hinten Mitte', 'Aktive Kopfstützen', 'Isofix', 'Isofix - Bezeichnung', 'Kindersitz integriert', 'ABS', 'Bremsassistent', 'Bremsassistent - Bezeichnung', 'Kurvenbremskontrolle', 'Antriebsschlupfregelung', 'Antriebsschlupfregelung - Bezeichnung', 'Fahrdynamikregelung', 'Fahrdynamikregelung - Bezeichnung', 'Fahrdynamikregelung - Anhänger', 'Ladezustandskontrolle', 'Bremslicht dynamisch', 'Bremslicht dynamisch - Bezeichnung', 'Aktivlenkung', 'Nebelscheinwerfer', 'Abbiegelicht', 'Kurvenlicht', 'Variable Lichtverteilung', 'Xenon-Scheinwerfer', 'LED-Scheinwerfer', 'Lichtsensor', 'Regensensor', 'Autom. Abstandsregelung', 'Autom. Abstandsregelung - Bezeichnung', 'Nachtsicht-Assistent', 'Müdigkeitserkennung', 'Spurhalteassistent', 'Spurwechselassistent', 'Fußgängerschutz-System', 'Verkehrsschild-Erkennung', 'Head-up-Display (HUD)', 'Speed-Limiter', 'Fernlichtassistent');
  }
  if (req.body.hasOwnProperty('Herstellergarantien')) {
    header.push('Garantie (Fahrzeug)', 'Lackgarantie', 'Durchrostung', 'Zusätzliche Garantien', 'Sonstiges');
  }
  if (req.body.hasOwnProperty('PreiseUndAusstattung')) {
    header.push('Grundpreis', 'Klassenübliche Ausstattung nach ADAC-Vorgabe', 'Klimaanlage', 'Zentralverriegelung', 'Fensterheber elektr. vorne', 'Fensterheber elektr. hinten', 'Einparkhilfe', 'Einparkhilfe - Bezeichnung', 'Einparkassistent', 'Berganfahrassistent', 'Radio', 'Radio - Bezeichnung', 'Navigation', 'Navigation - Bezeichnung', 'Notruffunktion', 'Alufelgen', 'Lederausstattung', 'Metallic-Lackierung');
  }
  writeStream.write(header.join(';') + '\n');

  ADACModels.Modell.findAll().then(function(models) {
    async.eachSeries(models, function(model, outCallback) {
      async.series([
        function(callback) {
          if (req.body.hasOwnProperty('Modell')) {
            var m = model;
            var row = [m.name, m.typ, m.HSNTSN, m.neuwagenpreis];
            callback(null, row);
          } else {
            callback(null, []);
          }
        },
        function(callback) {
          if (req.body.hasOwnProperty('Allgemein')) {
            ADACModels.Allgemein.findOne({where: {ModellId: model.id}, raw: true}).then(function(a) {
              var row = [
                a.marke,
                a.modell,
                a.typ,
                a.baureihe,
                a.herstellerinterneBaureihenbezeichnung,
                a.modellstart,
                a.modellende,
                a.baureihenstart,
                a.baureihenende,
                a.hSNSchluesselnummer,
                a.tSNSchluesselnummer,
                a.tSNSchluesselnummer2,
                a.kFZSteuerProJahr,
                a.cO2Effizienzklasse,
                a.grundpreis
              ];
              callback(null, row);
            });
          } else {
            callback(null, []);
          }
        },
        function(callback) {
          if (req.body.hasOwnProperty('MotorUndAntrieb')) {
            ADACModels.MotorUndAntrieb.findOne({where: {ModellId: model.id}, raw: true}).then(function(m) {
              var row = [
                m.motorart,
                m.kraftstoffart,
                m.kraftstoffart2Antrieb,
                m.abgasreinigung,
                m.motorbauart,
                m.anzahlZylinder,
                m.gemischaufbereitung,
                m.aufladung,
                m.anzahlVentile,
                m.hubraum,
                m.leistungInKW,
                m.leistungInPS,
                m.leistungMaximalBeiUMin,
                m.drehmoment,
                m.drehmomentMaximalBeiUMin,
                m.antriebsart,
                m.getriebeart,
                m.anzahlGaenge,
                m.startStoppAutomatik,
                m.schaltpunktanzeige,
                m.schadstoffklasse
              ];
              callback(null, row);
            });
          } else {
            callback(null, []);
          }
        },
        function(callback) {
          if (req.body.hasOwnProperty('MasseUndGewichte')) {
            ADACModels.MasseUndGewichte.findOne({where: {ModellId: model.id}, raw: true}).then(function(m) {
              var row = [
                m.laenge,
                m.breite,
                m.hoehe,
                m.radstand,
                m.bodenfreiheitMaximal,
                m.wendekreis,
                m.boeschungswinkelVorne,
                m.boeschungswinkelHinten,
                m.rampenwinkel,
                m.wattiefe,
                m.steigungMaximal,
                m.kofferraumvolumenNormal,
                m.kofferraumvolumenFensterhochMitUmgeklappterRuecksitzbank,
                m.kofferraumvolumenDachhochMitUmgeklappterRuecksitzbank,
                m.ruecksitzbankUmklappbar,
                m.leergewichtEU,
                m.zulGesamtgewicht,
                m.zuladung,
                m.anhaengelastGebremst12,
                m.anhaengelastUngebremst,
                m.gesamtzuggewicht,
                m.stuetzlast,
                m.dachlast
              ];
              callback(null, row);
            });
          } else {
            callback(null, []);
          }
        },
        function(callback) {
          if (req.body.hasOwnProperty('KarosserieUndFahrwerk')) {
            ADACModels.KarosserieUndFahrwerk.findOne({where: {ModellId: model.id}, raw: true}).then(function(k) {
              var row = [
                k.karosserie,
                k.tueranzahl,
                k.anzahlDerSchiebetuerenSerienmaessigAufWunsch,
                k.fahrzeugklasse,
                k.sitzanzahl,
                k.sitzanzahlMaximal,
                k.federungVorne,
                k.federungHinten,
                k.servolenkung,
                k.bremseVorne,
                k.bremseHinten,
                k.reifengroesse,
                k.reifengroesseHintenAbweichend,
                k.reifendruckkontrolle,
                k.reifendruckkontrolleBezeichnung,
                k.runflat,
                k.runflatBezeichnung
              ];
              callback(null, row);
            });
          } else {
            callback(null, []);
          }
        },
        function(callback) {
          if (req.body.hasOwnProperty('MesswerteHersteller')) {
            ADACModels.MesswerteHersteller.findOne({where: {ModellId: model.id}, raw: true}).then(function(m) {
              var row = [
                m.beschleunigung,
                m.hoechstgeschwindigkeit,
                m.verbrauchInnerorts,
                m.verbrauchInnerorts2Antrieb,
                m.verbrauchAusserorts,
                m.verbrauchAusserorts2Antrieb,
                m.verbrauchGesamt,
                m.verbrauchGesamt2Antrieb,
                m.cO2Ausstoss,
                m.tankgroesse,
                m.tankgroesse2Antrieb,
                m.tankeinbauort
              ];
              callback(null, row);
            });
          } else {
            callback(null, []);
          }
        },
        function(callback) {
          if (req.body.hasOwnProperty('Sicherheitsausstattung')) {
            ADACModels.Sicherheitsausstattung.findOne({where: {ModellId: model.id}, raw: true}).then(function(s) {
              var row = [
                s.airbagFahrer,
                s.airbagBeifahrer,
                s.seitenairbagVorne,
                s.seitenairbagVorneBezeichnung,
                s.seitenairbagHinten,
                s.seitenairbagHintenBezeichnung,
                s.kopfairbagVorne,
                s.kopfairbagVorneBezeichnung,
                s.kopfairbagHinten,
                s.kopfairbagHintenBezeichnung,
                s.airbagSonstige,
                s.airbagSonstigeBezeichnung,
                s.airbagDeaktivierung,
                s.airbagDeaktivierungBezeichnung,
                s.preCrashSystem,
                s.preCrashSystemBezeichnung,
                s['3PunktGurtHintenMitte'],
                s['3PunktGurtHintenMitteBezeichnung'],
                s.kopfstuetzenHinten,
                s.kopfstuetzenHintenMitte,
                s.aktiveKopfstuetzen,
                s.isofix,
                s.isofixBezeichnung,
                s.kindersitzIntegriert,
                s.aBS,
                s.bremsassistent,
                s.bremsassistentBezeichnung,
                s.kurvenbremskontrolle,
                s.antriebsschlupfregelung,
                s.antriebsschlupfregelungBezeichnung,
                s.fahrdynamikregelung,
                s.fahrdynamikregelungBezeichnung,
                s.fahrdynamikregelungAnhaenger,
                s.ladezustandskontrolle,
                s.bremslichtDynamisch,
                s.bremslichtDynamischBezeichnung,
                s.aktivlenkung,
                s.nebelscheinwerfer,
                s.abbiegelicht,
                s.kurvenlicht,
                s.variableLichtverteilung,
                s.xenonScheinwerfer,
                s.lEDScheinwerfer,
                s.lichtsensor,
                s.regensensor,
                s.automAbstandsregelung,
                s.automAbstandsregelungBezeichnung,
                s.nachtsichtAssistent,
                s.muedigkeitserkennung,
                s.spurhalteassistent,
                s.spurwechselassistent,
                s.fussgaengerschutzSystem,
                s.verkehrsschildErkennung,
                s.headUpDisplayHUD,
                s.speedLimiter,
                s.fernlichtassistent
              ];
              callback(null, row);
            });
          } else {
            callback(null, '');
          }
        },
        function(callback) {
          if (req.body.hasOwnProperty('Herstellergarantien')) {
            ADACModels.Herstellergarantien.findOne({where: {ModellId: model.id}, raw: true}).then(function(h) {
              var row = [
                h.garantieFahrzeug,
                h.lackgarantie,
                h.durchrostung,
                h.zusaetzlicheGarantien,
                h.sonstiges
              ];
              callback(null, row);
            });
          } else {
            callback(null, []);
          }
        },
        function(callback) {
          if (req.body.hasOwnProperty('PreiseUndAusstattung')) {
            ADACModels.PreiseUndAusstattung.findOne({where: {ModellId: model.id}, raw: true}).then(function(p) {
              var row = [
                p.grundpreis,
                p.klassenueblicheAusstattungNachADACVorgabe,
                p.klimaanlage,
                p.zentralverriegelung,
                p.fensterheberElektrVorne,
                p.fensterheberElektrHinten,
                p.einparkhilfe,
                p.einparkhilfeBezeichnung,
                p.einparkassistent,
                p.berganfahrassistent,
                p.radio,
                p.radioBezeichnung,
                p.navigation,
                p.navigationBezeichnung,
                p.notruffunktion,
                p.alufelgen,
                p.lederausstattung,
                p.metallicLackierung
              ];
              callback(null, row);
            });
          } else {
            callback(null, []);
          }
        }
      ], function(err, results) {
        console.log('Reached finish!');
        var completeRow = results[0].concat(results[1], results[2], results[3], results[4], results[5], results[6], results[7], results[8]);
        writeStream.write(completeRow.join(';') + '\n');
        outCallback();
      });
    }, function() {
      writeStream.end();
    });
    writeStream.on('finish', function() {
      res.sendFile(filename, function() {
        fs.unlinkSync(filename);
      });
    });
  });
});

module.exports = ADACRouter;
