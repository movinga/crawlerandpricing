var express = require('express');
var HamSudCrawler = require('../crawlers/hamsudCrawler');
var HamSudModels = require('../models/hamsudModels');
var fs = require('fs');
require('../helpers.js');

var hamsudRouter = express.Router();

hamsudRouter.post('/start', function(req, res) {
  HamSudCrawler.startCrawl();
  req.flash('info', 'You have invoked the Hamburg Süd crawler. It will take approx. 10 minutes to complete.')
  res.redirect('/status#hamsud')
});

hamsudRouter.post('/stop', function(req, res) {
  HamSudCrawler.stopCrawl();
  req.flash('info', 'You have stopped the Hamburg Süd crawler.')
  res.redirect('/status#hamsud')
});

hamsudRouter.post('/pause', function(req, res) {
  HamSudCrawler.pauseCrawl();
  req.flash('info', 'You have paused the Hamburg Süd crawler.')
  res.redirect('/status#hamsud')
});

hamsudRouter.post('/resume', function(req, res) {
  HamSudCrawler.resumeCrawl();
  req.flash('info', 'You have resumed the Hamburg Süd crawler.')
  res.redirect('/status#hamsud')
});

hamsudRouter.post('/report.csv', function(req, res) {
  var filename = __dirname + '/hamsud-report.csv';
  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });
  //Select Sessionid with the specified id
  HamSudModels.CrawlSession.findById(req.body.sessionId).then(function(session) {
    var writeStream = fs.createWriteStream(filename);
    writeStream.write("Port name;Port code;Rate type;Container type;Rate;Currency\n");
    session.getRates({raw: true}).then(function(rates) {
      rates.forEach(function(rate) {
        writeStream.write("{0};{1};{2};{3};{4};{5}\n".format(
          rate.portName,
          rate.portId,
          rate.rateType,
          rate.containerType,
          rate.rate,
          rate.currency
        ));
      });
      writeStream.end();
      writeStream.on('finish', function() {
        res.sendFile(filename, function() {
          fs.unlinkSync(filename);
        });
      });
    });
  });
});

module.exports = hamsudRouter;
