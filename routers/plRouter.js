var express = require('express');
var PlCrawler = require('../crawlers/plCrawler');
var PlModels = require('../models/plModels');
var fs = require('fs');
var logger = require('../logger.js');
require('../helpers.js');

var plRouter = express.Router();

plRouter.post('/start', function(req, res) {
  PlCrawler.startCrawl();
  req.flash('info', 'You have invoked the Primelocation crawler. It will take approx. 3 hours to complete.')
  res.redirect('/status#seloger');
});

plRouter.post('/stop', function(req, res) {
  PlCrawler.stopCrawl();
  req.flash('info', 'You have stopped the Primelocation crawler.')
  res.redirect('/status#seloger');
});

plRouter.post('/pause', function(req, res) {
  PlCrawler.pauseCrawl();
  req.flash('info', 'You have paused the Primelocation crawler.')
  res.redirect('/status#seloger');
});

plRouter.post('/resume', function(req, res) {
  PlCrawler.resumeCrawl();
  req.flash('info', 'You have resumed the Primelocation crawler.')
  res.redirect('/status#seloger');
});

plRouter.post('/report.csv', function(req, res) {
  var filename = __dirname + '/primelocation-report.csv'
  logger.info('Requested a Primelocation property report for crawling session %d.', req.body.sessionId);
  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });
  PlModels.CrawlSession.findById(req.body.sessionId).then(function(session) {
    var writeStream = fs.createWriteStream(filename);
    writeStream.write("Postcode;Property type;Price, £;Area, sq.ft.;Num. of bedrooms\n");
    session.getProperties({raw: true}).then(function(properties) {
      properties.forEach(function(property) {
        writeStream.write("{0};{1};{2};{3};{4}\n".format(
          property.postcode,
          property.propertyType,
          property.price,
          property.area,
          property.bedrooms
        ));
      });
      writeStream.end();
      writeStream.on('finish', function() {
        res.sendFile(filename, function() {
          fs.unlinkSync(filename);
        });
      });
    });
  });
});

module.exports = plRouter;
