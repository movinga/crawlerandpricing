var express = require('express');
var PostcodeCrawler = require('../crawlers/postcodeCrawler');
var logger = require('../logger.js');
require('../helpers.js');

var postcodeRouter = express.Router();

postcodeRouter.post('/start', function(req, res) {
  req.flash('info', 'You have invoked the UK postcode crawler (required for Zoopla). It will take approx. 2 hours to complete.');
  PostcodeCrawler.startCrawl(function() {
    logger.info('UK postcode crawling complete!');
  });
  res.redirect('/status#post');
});

postcodeRouter.post('/stop', function(req, res) {
  req.flash('You have stopped the UK postcode crawler.');
  PostcodeCrawler.stopCrawl();
  res.redirect('/status#post');
});

postcodeRouter.post('/pause', function(req, res) {
  req.flash('You have paused the UK postcode crawler.');
  PostcodeCrawler.pauseCrawl();
  res.redirect('/status#post');
});

postcodeRouter.post('/resume', function(req, res) {
  req.flash('You have resumed the UK postcode crawler.');
  PostcodeCrawler.resumeCrawl();
  res.redirect('/status#post');
});

module.exports = postcodeRouter;
