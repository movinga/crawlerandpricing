var PostcodeModels = require('../models/postcodeModels');

var express = require('express');
var fs = require('fs');
require('../helpers.js');

var rest = require('restler');
var htmlparser = require('htmlparser2');

var utilityRouter = express.Router();

utilityRouter.post('/districtPostcodes.csv', function(req, res) {
  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });

  PostcodeModels.uk.DistrictPostcode.findAll({raw: true}).then(function(postcodes) {
    var writeStream = fs.createWriteStream(__dirname + '/districtPostcodes.csv');
    writeStream.write("Postcode;City;County\n")
    postcodes.forEach(function(postcode) {
      writeStream.write("{0};{1};{2}\n".format(
        postcode.postcode,
        postcode.address1,
        postcode.address2
      ));
    });
    writeStream.end();
    writeStream.on('finish', function() {
      res.sendFile(__dirname + '/districtPostcodes.csv', function() {
        fs.unlinkSync(__dirname + '/districtPostcodes.csv');
      });
    });
  });
});

utilityRouter.post('/fullPostcodes.csv', function(req, res) {
  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });

  PostcodeModels.uk.FullPostcode.findAll({raw: true}).then(function(postcodes) {
    var writeStream = fs.createWriteStream(__dirname + '/fullPostcodes.csv');
    writeStream.write("Postcode;City;County;District;Region;Latitude;Longitude\n")
    postcodes.forEach(function(postcode) {
      writeStream.write("{0};{1};{2};{3};{4};{5};{6}\n".format(
        postcode.postcode,
        postcode.address1,
        postcode.address2,
        postcode.address3,
        postcode.address4,
        postcode.latitude,
        postcode.longitude
      ));
    });
    writeStream.end();
    writeStream.on('finish', function() {
      res.sendFile(__dirname + '/fullPostcodes.csv', function() {
        fs.unlinkSync(__dirname + '/fullPostcodes.csv');
      });
    });
  });
});

utilityRouter.post('/inventoryItems.csv', function(req, res) {
  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });

  var filename = __dirname + '/inventoryItems.csv';
  var writeStream = fs.createWriteStream(filename);
  writeStream.write('itemName;itemWeight;itemVolume;itemType\n');

  var inventoryItems = [];
  var inventoryItem = {};
  var inventoryTable = false;
  var key = undefined;

  var inventoryParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'table' && attributes.class === 'WeightsTable') {
        inventoryTable = true;
      }

      if (name === 'tr' && inventoryTable) {
        inventoryItem = {
          name: '',
          weight: '',
          volume: ''
        };
      }

      if (name === 'td' && inventoryTable) {
        if (attributes.class === 'ItemName') {
          key = 'name';
        } else if (attributes.class === 'Weight') {
          key = 'weight';
        } else if (attributes.class === 'CBF') {
          key = 'volume';
        } else {
          key = undefined;
        }
      }
    },

    onclosetag: function(name) {
      if (name === 'table' && inventoryTable) {
        inventoryTable = false;
      }

      if (name === 'tr' && inventoryTable) {
        if (inventoryItem.name !== '') {
          var item = inventoryItems.find(function(element) {
            return element.name === inventoryItem.name;
          });
          if (item === undefined) {
            inventoryItems.push(inventoryItem);
          }
        }
      }

      if (name === 'td' && inventoryTable) {
        key = undefined;
      }
    },

    ontext: function(text) {
      if (key && inventoryTable) {
        inventoryItem[key] = text;
      }
    }
  });

  rest.get('http://www.movingtoolsinc.com/Tools/household-furniture-dimensional-weight-calculator.aspx', {
  }).on('complete', function(data) {
    console.log(data);
    inventoryParser.write(data);
    inventoryParser.end();
    inventoryItems.sort(function(a, b) {
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      return 0;
    });
    console.log(inventoryItems);
    inventoryItems.forEach(function(item) {
      writeStream.write('{0};{1};{2};unit\n'.format(
        item.name,
        item.weight,
        item.volume
      ));
    });
    writeStream.end();
    writeStream.on('finish', function() {
      res.sendFile(filename, function() {
        fs.unlinkSync(filename);
      });
    });
  });
});

module.exports = utilityRouter;
