var express = require('express');
var WFRCrawler = require('../crawlers/wfrCrawler');
var WFRModels = require('../models/wfrModels');
var fs = require('fs');
var async = require('async');
require('../helpers.js');

var logger = require('../logger.js');

var wfrRouter = express.Router();

wfrRouter.post('/start', function(req, res) {
  WFRCrawler.startCrawl();
  req.flash('info', 'You have invoked the WorldFreightRates.com crawler. It will take approx. 72 hours to complete.')
  res.redirect('/status#wfr');
});

wfrRouter.post('/stop', function(req, res) {
  WFRCrawler.stopCrawl();
  req.flash('info', 'You have stopped the WorldFreightRates.com crawler.')
  res.redirect('/status#wfr');
});

wfrRouter.post('/pause', function(req, res) {
  console.log("Request invoked");
  WFRCrawler.pauseCrawl();
  req.flash('info', 'You have paused the WorldFreightRates.com crawler.')
  res.redirect('/status#wfr');
});

wfrRouter.post('/resume', function(req, res) {
  console.log("Request invoked");
  WFRCrawler.resumeCrawl();
  req.flash('info', 'You have resumed the WorldFreightRates.com crawler.')
  res.redirect('/status#wfr');
});

wfrRouter.post('/report.csv', function(req, res) {
  var filename = __dirname + '/wfr-report.csv';

  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });
  //Select Sessionid with the specified id
  WFRModels.CrawlSession.findById(req.body.sessionId).then(function(session) {
    //Select all that apply
    var sizesArray = [];
    var insuranceArray = [];
    var outputString = "";
    if (req.body.size20) {sizesArray.push(20);}
    if (req.body.size40) {sizesArray.push(40);}
    if (req.body.size45) {sizesArray.push(45);}
    if (req.body.withInsurance) {insuranceArray.push(true);}
    if (req.body.withoutInsurance) {insuranceArray.push(false);}

    var writeStream = fs.createWriteStream(filename);
    writeStream.write("Origin city;Origin country;Destination city;Destination country;Container size;Insurance;Price rate\n")
    session.getRates({where: {containerSize: sizesArray, insurance: insuranceArray}, raw: true}).then(function(rates) {
      rates.forEach(function(rate) {
        writeStream.write("{0};{1};{2};{3};{4};{5};{6}\n".format(
          rate.fromCity,
          rate.fromCountry,
          rate.toCity,
          rate.toCountry,
          rate.containerSize,
          rate.insurance,
          rate.rate
        ));
      });
      writeStream.end();
      writeStream.on('finish', function() {
        res.sendFile(filename, function() {
          fs.unlinkSync(filename);
        });
      });
    });
  });
});

module.exports = wfrRouter;
