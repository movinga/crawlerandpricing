var Account = {
  accounts: [
    {username: 'movinga', admin: false, password: 'Fhk6TjCe2sgc'},
    {username: 'admin', admin: true, password: 'efhr7dzT5DeW'}
  ],

  findAccount: function(username) {
    for (var i = 0; i < this.accounts.length; i++) {
      if (this.accounts[i].username === username) {
        return this.accounts[i];
      }
    }
    return undefined;
  },

  verifyPassword: function(user, password) {
    return user.password === password;
  }
};

module.exports = Account;
