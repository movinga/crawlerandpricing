var express = require('express');
var app = express();

var settings = require('./settings.js');
var logger = require('./logger.js');

var schedule = require('node-schedule');
var bodyParser = require('body-parser');
var hbs = require('hbs');
var async = require('async');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var Account = require('./account');

var WFRCrawler = require('./crawlers/wfrCrawler');
var WFRModels = require('./models/wfrModels');
var WFRRouter = require('./routers/wfrRouter');

var NGRSCrawler = require('./crawlers/ngrsCrawler');
var NGRSModels = require('./models/ngrsModels');
var NGRSRouter = require('./routers/ngrsRouter');

var IAMCrawler = require('./crawlers/iamCrawler');
var IAMModels = require('./models/iamModels');
var IAMRouter = require('./routers/iamRouter');

var FEDEMACCrawler = require('./crawlers/fedemacCrawler');
var FEDEMACModels = require('./models/fedemacModels');
var FEDEMACRouter = require('./routers/fedemacRouter');

var OPCityCrawler = require('./crawlers/overpassCityCrawler');
var OPCityRouter = require('./routers/overpassCityRouter');

var ZooplaCrawler = require('./crawlers/zooplaCrawler');
var ZooplaModels = require('./models/zooplaModels');
var ZooplaRouter = require('./routers/zooplaRouter');

var PostcodeCrawler = require('./crawlers/postcodeCrawler');
var PostcodeModels = require('./models/postcodeModels');
var PostcodeRouter = require('./routers/postcodeRouter');

var ImmobiliareCrawler = require('./crawlers/immobiliareCrawler');
var ImmobiliareModels = require('./models/immobiliareModels');
var ImmobiliareRouter = require('./routers/immobiliareRouter');

var SelogerCrawler = require('./crawlers/selogerCrawler');
var SelogerModels = require('./models/selogerModels');
var SelogerRouter = require('./routers/selogerRouter');

var BaggagenlCrawler = require('./crawlers/baggagenlCrawler');
var BaggagenlModels = require('./models/baggagenlModels');
var BaggagenlRouter = require('./routers/baggagenlRouter');

var ADACCrawler = require('./crawlers/adacCrawler');
var ADACModels = require('./models/adacModels');
var ADACRouter = require('./routers/adacRouter');

var HamSudCrawler = require('./crawlers/hamsudCrawler');
var HamSudModels = require('./models/hamsudModels');
var HamSudRouter = require('./routers/hamsudRouter');

var PlCrawler = require('./crawlers/plCrawler');
var PlModels = require('./models/plModels');
var PlRouter = require('./routers/plRouter');

var BHubCrawler = require('./crawlers/bHubCrawler');
var BHubModels = require('./models/bHubModels');
var BHubRouter = require('./routers/bHubRouter');

var ParuvenduCrawler = require('./crawlers/paruvenduCrawler');
var ParuvenduModels = require('./models/paruvenduModels');
var ParuvenduRouter = require('./routers/paruvenduRouter');

var UtilityRouter = require('./routers/utilityRouter');

var crawlers = [WFRCrawler, NGRSCrawler, IAMCrawler, OPCityCrawler,
  FEDEMACCrawler, PostcodeCrawler, ZooplaCrawler, ImmobiliareCrawler,
  SelogerCrawler, BaggagenlCrawler, ADACCrawler, HamSudCrawler,
  PlCrawler, BHubCrawler, ParuvenduCrawler];
var modelsWithCrawlSession = [WFRModels, FEDEMACModels, IAMModels,
  ZooplaModels, NGRSModels, ImmobiliareModels, SelogerModels,
  BaggagenlModels, HamSudModels, PlModels, BHubModels, ParuvenduModels];

var timeout = require('connect-timeout');

app.use(timeout(120000));
app.use(haltOnTimedout);

function haltOnTimedout(req, res, next){
  if (!req.timedout) next();
}

app.use(express.static(__dirname + '/public'));
app.use(require('express-promise')());
app.use(express.static('public'));
app.set('view engine', 'hbs');
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cookieParser('adljhlskdafgvqe,fiuvglaef'));
app.use(session({
  secret: 'ladfhkljwhflowhoebviljsbd'
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(function(username, password, done) {
  var user = Account.findAccount(username);
  if (!user) {
    return done(null, false, {message: 'User {0} not found.'.format(username)});
  }
  if (!Account.verifyPassword(user, password)) {
    return done(null, false, {message: 'Wrong password!'});
  }
  return done(null, user);
}));

passport.serializeUser(function(user, done) {
  return done(null, user);
});

passport.deserializeUser(function(user, done) {
  return done(null, user);
});

var authenticate = function(req, res, next) {
  if (!req.path.hasSubstring('auth')) {
    if (req.user) {
      next()
    } else {
      req.flash('info', 'You need to be authorized to access this page.');
      res.redirect('/auth/login');
    }
  } else {
    next();
  }
}

app.use(authenticate);

app.get('/auth/login', function(req, res) {
  var flash = req.flash('info');
  var message = flash.length ? undefined : flash[0]
  res.render('login', {message: message});
});

app.post('/auth/login', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) {
      return next(err);
    } if (!user) {
      return res.render('login', {message: info.message});
    }
    req.logIn(user, function(err) {
      if (err) {return next(err);}
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/auth/logout', function(req, res) {
  req.logout();
  req.flash('info', 'Logout successful!');
  res.redirect('/');
});

var crawlJobs = {
  wfr: schedule.scheduleJob(settings.crawlers.wfr, function() {
    logger.info('Started World Freight Rates crawling job.');
    WFRCrawler.startCrawl();
  }),

  ngrs: schedule.scheduleJob(settings.crawlers.ngrs, function() {
    logger.info('Started NGRS crawling job.');
    NGRSCrawler.startCrawl();
  }),

  iam: schedule.scheduleJob(settings.crawlers.iam, function() {
    logger.info('Started IAM crawling job.');
    IAMCrawler.startCrawl();
  }),

  fedemac: schedule.scheduleJob(settings.crawlers.fedemac, function() {
    logger.info('Started FEDEMAC crawling job.');
    FEDEMACCrawler.startCrawl();
  }),

  zoopla: schedule.scheduleJob(settings.crawlers.zoopla, function() {
    logger.info('Started Zoopla crawling job.');
    ZooplaCrawler.startCrawl();
  }),

  overpass: schedule.scheduleJob(settings.crawlers.overpass, function() {
    logger.info('Started Overpass crawling job.');
    OPCityCrawler.startCrawl(function() {
      logger.info('Overpass crawling complete!');
    });
  }),

  post: schedule.scheduleJob(settings.crawlers.post, function() {
    logger.info('Started UK postcode crawling job.');
    PostcodeCrawler.startCrawl(function() {
      logger.info('UK postcode crawling complete!');
    });
  }),

  immobiliare: schedule.scheduleJob(settings.crawlers.immobiliare, function() {
    logger.info('Started Immobiliare crawling job.');
    ImmobiliareCrawler.startCrawl();
  }),

  seloger: schedule.scheduleJob(settings.crawlers.seloger, function() {
    logger.info('Started seloger crawling job.');
    SelogerCrawler.startCrawl();
  }),

  baggagenl: schedule.scheduleJob(settings.crawlers.baggagenl, function() {
    logger.info('Started Baggage.nl crawling job.');
    BaggagenlCrawler.startCrawl();
  }),

  adac: schedule.scheduleJob(settings.crawlers.adac, function() {
    logger.info('Started ADAC crawling job.');
    ADACCrawler.startCrawl();
  }),

  hamsud: schedule.scheduleJob(settings.crawlers.hamsud, function() {
    logger.info('Started Hamburg Süd crawling job.');
    HamSudCrawler.startCrawl();
  }),

  pl: schedule.scheduleJob(settings.crawlers.pl, function() {
    logger.info('Started Primelocation crawling job.');
    PlCrawler.startCrawl();
  }),

  bhub: schedule.scheduleJob(settings.crawlers.bhub, function() {
    logger.info('Started Baggagehub.com crawling job.');
    BHubCrawler.startCrawl();
  }),

  paruvendu: schedule.scheduleJob(settings.crawlers.paruvendu, function() {
    logger.info('Started Paruvendu.fr crawling job.');
    ParuvenduCrawler.startCrawl();
  }),

}

schedule.scheduleJob(settings.cleardb, function() {
  modelsWithCrawlSession.forEach(function(model) {
    model.CrawlSession.destroy({
      where: {
        'created_at': {
          $lt: new Date(new Date() - 30 * 24 * 60 * 60 * 1000)
        }
      }
    })
  });
});

app.get('/', function(req, res) {
  logger.info('Received GET /');
  //Select all available crawls from the database
  var flash = req.flash('info');
  var props = {};
  props.message = flash.length == 0 ? undefined : flash;
  props.user = req.user;
  console.log(props.user);

  async.eachSeries(modelsWithCrawlSession, function(model, callback) {
    parseCrawlSessions(model, props, callback);
  }, function(err) {
    res.render('index', props);
  });
});

var parseCrawlSessions = function(model, props, callback) {
  model.CrawlSession.findAll({raw: true}).then(function(sessions) {
    sessions.forEach(function(session, index, array) {
      session.created_at = session.created_at.toString();
      session.checked = index == sessions.length - 1 ? true : false;
    });
    props[model.shortName] = {
      crawlingDate: crawlJobs[model.shortName].nextInvocation().toString(),
      sessions: sessions
    }
    callback();
  });
}

app.get('/status', function(req, res) {
  logger.info('Received GET /status');
  //Select all available crawls from the database
  var flash = req.flash('info');
  var props = {
    crawlers: []
  }
  props.message = flash.length == 0 ? undefined : flash;
  props.user = req.user;

  crawlers.forEach(function(crawler) {
    props.crawlers.push({
      longName: crawler.longName,
      shortName: crawler.shortName,
      crawlingDate: crawlJobs[crawler.shortName].nextInvocation().toString()
    });
  });
  res.render('status', props);
});

app.post('/status', function(req, res) {
  var response = [];
  crawlers.forEach(function(crawler) {
    response.push({
      shortName: crawler.shortName,
      status: crawler.status,
      progress: crawler.progress()
    });
  });
  res.json(response);
});

app.use('/ngrs', NGRSRouter);
app.use('/wfr', WFRRouter);
app.use('/iam', IAMRouter);
app.use('/fedemac', FEDEMACRouter);
app.use('/zoopla', ZooplaRouter);
app.use('/utility', UtilityRouter);
app.use('/overpass', OPCityRouter);
app.use('/post', PostcodeRouter);
app.use('/immobiliare', ImmobiliareRouter);
app.use('/seloger', SelogerRouter);
app.use('/baggagenl', BaggagenlRouter);
app.use('/adac', ADACRouter);
app.use('/hamsud', HamSudRouter);
app.use('/pl', PlRouter);
app.use('/bhub', BHubRouter);
app.use('/paruvendu', ParuvenduRouter);

var server = app.listen(3001, function() {
  var host = server.address().address;
  var port = server.address().port;

  var models = [
    WFRModels.CrawlSession,
    WFRModels.City,
    WFRModels.Rate,
    NGRSModels.CrawlSession,
    NGRSModels.Company,
    IAMModels.CrawlSession,
    IAMModels.Company,
    FEDEMACModels.CrawlSession,
    FEDEMACModels.City,
    FEDEMACModels.Company,
    PostcodeModels.uk.DistrictPostcode,
    PostcodeModels.uk.FullPostcode,
    PostcodeModels.FRPostcode,
    PostcodeModels.ITPostcode,
    ZooplaModels.CrawlSession,
    ZooplaModels.AreaStats,
    ZooplaModels.AreaSampleSize,
    ImmobiliareModels.CrawlSession,
    ImmobiliareModels.Property,
    ImmobiliareModels.Commune,
    ImmobiliareModels.Zone,
    SelogerModels.CrawlSession,
    SelogerModels.Property,
    BaggagenlModels.CrawlSession,
    BaggagenlModels.AirRate,
    BaggagenlModels.SeaRate,
    BaggagenlModels.CourierRate,
    BaggagenlModels.RoadRate,
    ADACModels.Modell,
    ADACModels.Allgemein,
    ADACModels.MotorUndAntrieb,
    ADACModels.MasseUndGewichte,
    ADACModels.KarosserieUndFahrwerk,
    ADACModels.MesswerteHersteller,
    ADACModels.Sicherheitsausstattung,
    ADACModels.Herstellergarantien,
    ADACModels.PreiseUndAusstattung,
    HamSudModels.CrawlSession,
    HamSudModels.Rate,
    PlModels.CrawlSession,
    PlModels.Property,
    BHubModels.CrawlSession,
    BHubModels.Rate,
    ParuvenduModels.CrawlSession,
    ParuvenduModels.Property
  ];

  async.eachSeries(models, function(model, callback) {
    model.sync().then(function() {
      callback();
    });
  });

  console.log('Application started at http://%s:%s', host, port);
  logger.info('Application started at http://%s:%s', host, port);
});

module.exports = app;
