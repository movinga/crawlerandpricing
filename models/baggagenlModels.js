var Sequelize = require('sequelize');
var sequelize = require('./sequelize').connect('baggagenl');

//Defining Sequelize models
var AirRate = sequelize.define('AirRate', {
  country:          {type: Sequelize.STRING(100), allowNull: false},
  zone:             {type: Sequelize.STRING(100), allowNull: false},
  weight:           {type: Sequelize.DOUBLE, allowNull: false},
  value:            {type: Sequelize.DOUBLE, allowNull: false},
  rate:             {type: Sequelize.DECIMAL(10, 2), allowNull: false, defaultValue: 0.00}
}, {
  timestamps: false,
  tableName: 'rates_air'
});

var SeaRate = sequelize.define('SeaRate', {
  country:          {type: Sequelize.STRING(100), allowNull: false},
  zone:             {type: Sequelize.STRING(100), allowNull: false},
  volume:           {type: Sequelize.DOUBLE, allowNull: false},
  value:            {type: Sequelize.DOUBLE, allowNull: false},
  rate:             {type: Sequelize.DECIMAL(10, 2), allowNull: false, defaultValue: 0.00}
}, {
  timestamps: false,
  tableName: 'rates_sea'
});

var CourierRate = sequelize.define('CourierRate', {
  country:          {type: Sequelize.STRING(100), allowNull: false},
  zone:             {type: Sequelize.STRING(100), allowNull: false},
  weight:           {type: Sequelize.DOUBLE, allowNull: false},
  value:            {type: Sequelize.DOUBLE, allowNull: false},
  rate:             {type: Sequelize.DECIMAL(10, 2), allowNull: false, defaultValue: 0.00}
}, {
  timestamps: false,
  tableName: 'rates_courier'
});

var RoadRate = sequelize.define('RoadRate', {
  country:          {type: Sequelize.STRING(100), allowNull: false},
  zone:             {type: Sequelize.STRING(100), allowNull: false},
  weight:           {type: Sequelize.DOUBLE, allowNull: false},
  value:            {type: Sequelize.DOUBLE, allowNull: false},
  rate:             {type: Sequelize.DECIMAL(10, 2), allowNull: false, defaultValue: 0.00}
}, {
  timestamps: false,
  tableName: 'rates_road'
});

var CrawlSession = sequelize.define('CrawlSession', {
}, {
  timestamps: true,
  updatedAt: false,
  underscored: true,
  tableName: 'crawl_sessions'
});

CrawlSession.hasMany(AirRate, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'airRates',
    singular: 'airRate'
  },
  onDelete: 'CASCADE'
});

CrawlSession.hasMany(SeaRate, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'seaRates',
    singular: 'seaRate'
  },
  onDelete: 'CASCADE'
});

CrawlSession.hasMany(CourierRate, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'courierRates',
    singular: 'courierRate'
  },
  onDelete: 'CASCADE'
});

CrawlSession.hasMany(RoadRate, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'roadRates',
    singular: 'roadRate'
  },
  onDelete: 'CASCADE'
});

module.exports = {
  shortName: 'baggagenl',
  AirRate: AirRate,
  SeaRate: SeaRate,
  CourierRate: CourierRate,
  RoadRate: RoadRate,
  CrawlSession: CrawlSession
};
