var Sequelize = require('sequelize');
var sequelize = require('./sequelize').connect('ngrs');

//Defining Sequelize models
var Company = sequelize.define('Company', {
  name:     {type: Sequelize.STRING(100), allowNull: false},
  address1: {type: Sequelize.STRING(100), field: 'address_1'},
  address2: {type: Sequelize.STRING(100), field: 'address_2'},
  address3: {type: Sequelize.STRING(100), field: 'address_3'},
  address4: {type: Sequelize.STRING(100), field: 'address_4'},
  town:     {type: Sequelize.STRING(50)},
  county:   {type: Sequelize.STRING(50)},
  postcode: {type: Sequelize.STRING(10), allowNull: false},
  phone:    {type: Sequelize.STRING(30)},
  website:  {type: Sequelize.STRING(100)},
  email:    {type: Sequelize.STRING(100)}
}, {
  timestamps: false,
  tableName: 'companies'
});

var CrawlSession = sequelize.define('CrawlSession', {
}, {
  timestamps: true,
  updatedAt: false,
  underscored: true,
  tableName: 'crawl_sessions'
});

CrawlSession.hasMany(Company, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'companies',
    singular: 'company'
  },
  onDelete: 'CASCADE'
});

module.exports = {
  shortName: 'ngrs',
  Company: Company,
  CrawlSession: CrawlSession
};
