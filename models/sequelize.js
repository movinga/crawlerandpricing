var Sequelize = require('sequelize');
var settings = require('../settings');
require('../helpers.js');

var connect = function(database) {
  var connectionString = "postgres://{0}:{1}@{2}:{3}/{4}".format(
    settings.database.username,
    settings.database.password,
    settings.database.host,
    settings.database.port,
    database
  );
  return new Sequelize(connectionString);
}

module.exports = {
  connect: connect
};
