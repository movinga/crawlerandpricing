var Sequelize = require('sequelize');
var sequelize = require('./sequelize').connect('fedemac');

var City = sequelize.define('City', {
  name        : {type:Sequelize.STRING(100), allowNull: false},
  country     : {type:Sequelize.STRING(100), allowNull: false},
  population  : {type:Sequelize.INTEGER},
  type        : {type: Sequelize.STRING(100), allowNull: false}
}, {
  timestamps: false,
  tableName: 'cities',
  indexes: [{unique: true, fields: ['name', 'country']}]
});

//Defining Sequelize models
var Company = sequelize.define('Company', {
  name          : {type: Sequelize.STRING(200), allowNull: false},
  street        : {type: Sequelize.STRING(200)},
  city          : {type: Sequelize.STRING(50)},
  country       : {type: Sequelize.STRING(50)},
  mobilePhone   : {type: Sequelize.STRING(100), field: 'mobile_phone'},
  workPhone     : {type: Sequelize.STRING(100), field: 'work_phone'},
  email         : {type: Sequelize.STRING(100)},
  website       : {type: Sequelize.STRING(100)},
  services      : {type: Sequelize.STRING(1000)},
}, {
  timestamps: false,
  tableName: 'companies',
  indexes: [{unique: true, fields: ['name', 'street', 'city', 'country', 'session_id']}]
});

var CrawlSession = sequelize.define('CrawlSession', {
}, {
  timestamps: true,
  updatedAt: false,
  underscored: true,
  tableName: 'crawl_sessions'
});

CrawlSession.hasMany(Company, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'companies',
    singular: 'company'
  },
  onDelete: 'CASCADE'
});

module.exports = {
  shortName: 'fedemac',
  City: City,
  Company: Company,
  CrawlSession: CrawlSession,
  sequelize: sequelize
};
