var Sequelize = require('sequelize');
var sequelize = require('./sequelize').connect('adac');

// Defining Sequelize models
var Modell = sequelize.define('Modell', {
  name:           {type: Sequelize.STRING(200), allowNull: false},
  typ:            {type: Sequelize.STRING(100), allowNull: false},
  HSNTSN:         {type: Sequelize.STRING(100), allowNull: false},
  neuwagenpreis:  {type: Sequelize.STRING(50), allowNull: false}
}, {
  timestamps: false,
  tableName: 'modell',
  indexes: [{unique: true, fields: ['name']}]
});

var Allgemein = sequelize.define('Allgemein', {
  marke: {type: Sequelize.STRING(100)},
  modell: {type: Sequelize.STRING(200)},
  typ: {type: Sequelize.STRING(100)},
  baureihe: {type: Sequelize.STRING(100)},
  herstellerinterneBaureihenbezeichnung: {type: Sequelize.STRING(100)},
  modellstart: {type: Sequelize.STRING(100)},
  modellende: {type: Sequelize.STRING(100)},
  baureihenstart: {type: Sequelize.STRING(100)},
  baureihenende: {type: Sequelize.STRING(100)},
  hSNSchluesselnummer: {type: Sequelize.STRING(100)},
  tSNSchluesselnummer: {type: Sequelize.STRING(100)},
  tSNSchluesselnummer2: {type: Sequelize.STRING(100)},
  kFZSteuerProJahr: {type: Sequelize.STRING(100)},
  cO2Effizienzklasse: {type: Sequelize.STRING(100)},
  grundpreis: {type: Sequelize.STRING(100)}
}, {
  timestamps: false,
  tableName: 'allgemein',
  indexes: [{unique: true, fields: ['ModellId']}]
});

var MotorUndAntrieb = sequelize.define('MotorUndAntrieb', {
  motorart: {type: Sequelize.STRING(100)},
  kraftstoffart: {type: Sequelize.STRING(100)},
  kraftstoffart2Antrieb: {type: Sequelize.STRING(100)},
  abgasreinigung: {type: Sequelize.STRING(100)},
  motorbauart: {type: Sequelize.STRING(100)},
  anzahlZylinder: {type: Sequelize.STRING(50)},
  gemischaufbereitung: {type: Sequelize.STRING(100)},
  aufladung: {type: Sequelize.STRING(100)},
  anzahlVentile: {type: Sequelize.STRING(50)},
  hubraum: {type: Sequelize.STRING(100)},
  leistungInKW: {type: Sequelize.STRING(50)},
  leistungInPS: {type: Sequelize.STRING(50)},
  leistungMaximalBeiUMin: {type: Sequelize.STRING(100)},
  drehmoment: {type: Sequelize.STRING(100)},
  drehmomentMaximalBeiUMin: {type: Sequelize.STRING(100)},
  antriebsart: {type: Sequelize.STRING(100)},
  getriebeart: {type: Sequelize.STRING(100)},
  anzahlGaenge: {type: Sequelize.STRING(50)},
  startStoppAutomatik: {type: Sequelize.STRING(100)},
  schaltpunktanzeige: {type: Sequelize.STRING(100)},
  schadstoffklasse: {type: Sequelize.STRING(100)}
}, {
  timestamps: false,
  tableName: 'motor_antrieb',
  indexes: [{unique: true, fields: ['ModellId']}]
});

var MasseUndGewichte = sequelize.define('MasseUndGewichte', {
  laenge: {type: Sequelize.STRING(100)},
  breite: {type: Sequelize.STRING(100)},
  hoehe: {type: Sequelize.STRING(100)},
  radstand: {type: Sequelize.STRING(100)},
  bodenfreiheitMaximal: {type: Sequelize.STRING(100)},
  wendekreis: {type: Sequelize.STRING(100)},
  boeschungswinkelVorne: {type: Sequelize.STRING(100)},
  boeschungswinkelHinten: {type: Sequelize.STRING(100)},
  rampenwinkel: {type: Sequelize.STRING(100)},
  wattiefe: {type: Sequelize.STRING(100)},
  steigungMaximal: {type: Sequelize.STRING(100)},
  kofferraumvolumenNormal: {type: Sequelize.STRING(100)},
  kofferraumvolumenFensterhochMitUmgeklappterRuecksitzbank: {type: Sequelize.STRING(100)},
  kofferraumvolumenDachhochMitUmgeklappterRuecksitzbank: {type: Sequelize.STRING(100)},
  ruecksitzbankUmklappbar: {type: Sequelize.STRING(100)},
  leergewichtEU: {type: Sequelize.STRING(100)},
  zulGesamtgewicht: {type: Sequelize.STRING(100)},
  zuladung: {type: Sequelize.STRING(100)},
  anhaengelastGebremst12: {type: Sequelize.STRING(100)},
  anhaengelastUngebremst: {type: Sequelize.STRING(100)},
  gesamtzuggewicht: {type: Sequelize.STRING(100)},
  stuetzlast: {type: Sequelize.STRING(100)},
  dachlast: {type: Sequelize.STRING(100)}
}, {
  timestamps: false,
  tableName: 'masse_gewichte',
  indexes: [{unique: true, fields: ['ModellId']}]
});

var KarosserieUndFahrwerk = sequelize.define('KarosserieUndFahrwerk', {
  karosserie: {type: Sequelize.STRING(100)},
  tueranzahl: {type: Sequelize.STRING(50)},
  anzahlDerSchiebetuerenSerienmaessigAufWunsch: {type: Sequelize.STRING(100)},
  fahrzeugklasse: {type: Sequelize.STRING(100)},
  sitzanzahl: {type: Sequelize.STRING(50)},
  sitzanzahlMaximal: {type: Sequelize.STRING(50)},
  federungVorne: {type: Sequelize.STRING(100)},
  federungHinten: {type: Sequelize.STRING(100)},
  servolenkung: {type: Sequelize.STRING(100)},
  bremseVorne: {type: Sequelize.STRING(100)},
  bremseHinten: {type: Sequelize.STRING(100)},
  reifengroesse: {type: Sequelize.STRING(100)},
  reifengroesseHintenAbweichend: {type: Sequelize.STRING(100)},
  reifendruckkontrolle: {type: Sequelize.STRING(100)},
  reifendruckkontrolleBezeichnung: {type: Sequelize.STRING(100)},
  runflat: {type: Sequelize.STRING(100)},
  runflatBezeichnung: {type: Sequelize.STRING(100)}
}, {
  timestamps: false,
  tableName: 'karosserie_fahrwerk',
  indexes: [{unique: true, fields: ['ModellId']}]
});

var MesswerteHersteller = sequelize.define('MesswerteHersteller', {
  beschleunigung: {type: Sequelize.STRING(100)},
  hoechstgeschwindigkeit: {type: Sequelize.STRING(100)},
  verbrauchInnerorts: {type: Sequelize.STRING(100)},
  verbrauchInnerorts2Antrieb: {type: Sequelize.STRING(100)},
  verbrauchAusserorts: {type: Sequelize.STRING(100)},
  verbrauchAusserorts2Antrieb: {type: Sequelize.STRING(100)},
  verbrauchGesamt: {type: Sequelize.STRING(100)},
  verbrauchGesamt2Antrieb: {type: Sequelize.STRING(100)},
  cO2Ausstoss: {type: Sequelize.STRING(100)},
  tankgroesse: {type: Sequelize.STRING(100)},
  tankgroesse2Antrieb: {type: Sequelize.STRING(100)},
  tankeinbauort: {type: Sequelize.STRING(100)}
}, {
  timestamps: false,
  tableName: 'messwerte_hersteller',
  indexes: [{unique: true, fields: ['ModellId']}]
});

var Sicherheitsausstattung = sequelize.define('Sicherheitsausstattung', {
  airbagFahrer: {type: Sequelize.STRING(100)},
  airbagBeifahrer: {type: Sequelize.STRING(100)},
  seitenairbagVorne: {type: Sequelize.STRING(100)},
  seitenairbagVorneBezeichnung: {type: Sequelize.STRING(100)},
  seitenairbagHinten: {type: Sequelize.STRING(100)},
  seitenairbagHintenBezeichnung: {type: Sequelize.STRING(100)},
  kopfairbagVorne: {type: Sequelize.STRING(100)},
  kopfairbagVorneBezeichnung: {type: Sequelize.STRING(100)},
  kopfairbagHinten: {type: Sequelize.STRING(100)},
  kopfairbagHintenBezeichnung: {type: Sequelize.STRING(100)},
  airbagSonstige: {type: Sequelize.STRING(100)},
  airbagSonstigeBezeichnung: {type: Sequelize.STRING(100)},
  airbagDeaktivierung: {type: Sequelize.STRING(100)},
  airbagDeaktivierungBezeichnung: {type: Sequelize.STRING(100)},
  preCrashSystem: {type: Sequelize.STRING(100)},
  preCrashSystemBezeichnung: {type: Sequelize.STRING(100)},
  '3PunktGurtHintenMitte': {type: Sequelize.STRING(100)},
  '3PunktGurtHintenMitteBezeichnung': {type: Sequelize.STRING(100)},
  kopfstuetzenHinten: {type: Sequelize.STRING(100)},
  kopfstuetzenHintenMitte: {type: Sequelize.STRING(100)},
  aktiveKopfstuetzen: {type: Sequelize.STRING(100)},
  isofix: {type: Sequelize.STRING(100)},
  isofixBezeichnung: {type: Sequelize.STRING(100)},
  kindersitzIntegriert: {type: Sequelize.STRING(100)},
  aBS: {type: Sequelize.STRING(100)},
  bremsassistent: {type: Sequelize.STRING(100)},
  bremsassistentBezeichnung: {type: Sequelize.STRING(100)},
  kurvenbremskontrolle: {type: Sequelize.STRING(100)},
  antriebsschlupfregelung: {type: Sequelize.STRING(100)},
  antriebsschlupfregelungBezeichnung: {type: Sequelize.STRING(100)},
  fahrdynamikregelung: {type: Sequelize.STRING(100)},
  fahrdynamikregelungBezeichnung: {type: Sequelize.STRING(100)},
  fahrdynamikregelungAnhaenger: {type: Sequelize.STRING(100)},
  ladezustandskontrolle: {type: Sequelize.STRING(100)},
  bremslichtDynamisch: {type: Sequelize.STRING(100)},
  bremslichtDynamischBezeichnung: {type: Sequelize.STRING(100)},
  aktivlenkung: {type: Sequelize.STRING(100)},
  nebelscheinwerfer: {type: Sequelize.STRING(100)},
  abbiegelicht: {type: Sequelize.STRING(100)},
  kurvenlicht: {type: Sequelize.STRING(100)},
  variableLichtverteilung: {type: Sequelize.STRING(100)},
  xenonScheinwerfer: {type: Sequelize.STRING(100)},
  lEDScheinwerfer: {type: Sequelize.STRING(100)},
  lichtsensor: {type: Sequelize.STRING(100)},
  regensensor: {type: Sequelize.STRING(100)},
  automAbstandsregelung: {type: Sequelize.STRING(100)},
  automAbstandsregelungBezeichnung: {type: Sequelize.STRING(100)},
  nachtsichtAssistent: {type: Sequelize.STRING(100)},
  muedigkeitserkennung: {type: Sequelize.STRING(100)},
  spurhalteassistent: {type: Sequelize.STRING(100)},
  spurwechselassistent: {type: Sequelize.STRING(100)},
  fussgaengerschutzSystem: {type: Sequelize.STRING(100)},
  verkehrsschildErkennung: {type: Sequelize.STRING(100)},
  headUpDisplayHUD: {type: Sequelize.STRING(100)},
  speedLimiter: {type: Sequelize.STRING(100)},
  fernlichtassistent: {type: Sequelize.STRING(100)}
}, {
  timestamps: false,
  tableName: 'sicherheitsausstattung',
  indexes: [{unique: true, fields: ['ModellId']}]
});

var Herstellergarantien = sequelize.define('Herstellergarantien', {
  garantieFahrzeug: {type: Sequelize.STRING(1000)},
  lackgarantie: {type: Sequelize.STRING(100)},
  durchrostung: {type: Sequelize.STRING(100)},
  zusaetzlicheGarantien: {type: Sequelize.STRING(1000)},
  sonstiges: {type: Sequelize.STRING(1000)}
}, {
  timestamps: false,
  tableName: 'herstellergarantien',
  indexes: [{unique: true, fields: ['ModellId']}]
});

var PreiseUndAusstattung = sequelize.define('PreiseUndAusstattung', {
  grundpreis: {type: Sequelize.STRING(100)},
  klassenueblicheAusstattungNachADACVorgabe: {type: Sequelize.STRING(100)},
  klimaanlage: {type: Sequelize.STRING(100)},
  zentralverriegelung: {type: Sequelize.STRING(100)},
  fensterheberElektrVorne: {type: Sequelize.STRING(100)},
  fensterheberElektrHinten: {type: Sequelize.STRING(100)},
  einparkhilfe: {type: Sequelize.STRING(100)},
  einparkhilfeBezeichnung: {type: Sequelize.STRING(100)},
  einparkassistent: {type: Sequelize.STRING(100)},
  berganfahrassistent: {type: Sequelize.STRING(100)},
  radio: {type: Sequelize.STRING(100)},
  radioBezeichnung: {type: Sequelize.STRING(100)},
  navigation: {type: Sequelize.STRING(100)},
  navigationBezeichnung: {type: Sequelize.STRING(100)},
  notruffunktion: {type: Sequelize.STRING(100)},
  alufelgen: {type: Sequelize.STRING(100)},
  lederausstattung: {type: Sequelize.STRING(100)},
  metallicLackierung: {type: Sequelize.STRING(100)}
}, {
  timestamps: false,
  tableName: 'preise_asstattung',
  indexes: [{unique: true, fields: ['ModellId']}]
});

Modell.hasOne(Allgemein);
Modell.hasOne(MotorUndAntrieb);
Modell.hasOne(MasseUndGewichte);
Modell.hasOne(KarosserieUndFahrwerk);
Modell.hasOne(MesswerteHersteller);
Modell.hasOne(Sicherheitsausstattung);
Modell.hasOne(Herstellergarantien);
Modell.hasOne(PreiseUndAusstattung);

module.exports = {
  shortName: 'adac',
  Modell: Modell,
  Allgemein: Allgemein,
  MotorUndAntrieb: MotorUndAntrieb,
  MasseUndGewichte: MasseUndGewichte,
  KarosserieUndFahrwerk: KarosserieUndFahrwerk,
  MesswerteHersteller: MesswerteHersteller,
  Sicherheitsausstattung: Sicherheitsausstattung,
  Herstellergarantien: Herstellergarantien,
  PreiseUndAusstattung: PreiseUndAusstattung
};
