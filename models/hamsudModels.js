var Sequelize = require('sequelize');
var sequelize = require('./sequelize').connect('hamsud');

//Defining Sequelize models
var Rate = sequelize.define('Rate', {
  portName:       {type: Sequelize.STRING(50), allowNull: false},
  portId:         {type: Sequelize.STRING(5), allowNull: false},
  rateType:       {type: Sequelize.STRING(50), allowNull: false},
  containerType:  {type: Sequelize.STRING(50), allowNull: false},
  rate:           {type: Sequelize.DECIMAL(10, 2), allowNull: false, defaultValue: 0.00},
  currency:       {type: Sequelize.STRING(5), allowNull: false}
}, {
  timestamps: false,
  tableName: 'rates'
});

var CrawlSession = sequelize.define('CrawlSession', {
}, {
  timestamps: true,
  updatedAt: false,
  underscored: true,
  tableName: 'crawl_session'
});

CrawlSession.hasMany(Rate, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'rates',
    singular: 'rate'
  },
  onDelete: 'CASCADE'
});

module.exports = {
  shortName: 'hamsud',
  Rate: Rate,
  CrawlSession: CrawlSession
};
