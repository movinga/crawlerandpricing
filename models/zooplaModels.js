var Sequelize = require('sequelize');
var sequelize = require('./sequelize').connect('zoopla');

var AreaStats = sequelize.define('AreaStats', {
  postcode    : {type: Sequelize.STRING(10), allowNull: false},
  type        : {type: Sequelize.STRING(20), allowNull: false},
  totalPrice  : {type: Sequelize.DOUBLE, allowNull: false, field: 'total_price'},
  priceSqFeet : {type: Sequelize.DOUBLE, field: 'price_sq_feet'},
  area        : {type: Sequelize.DOUBLE},
  bedrooms    : {type: Sequelize.DOUBLE}
}, {
  timestamps: false,
  tableName: 'area_stats',
  indexes: [{unique: true, fields: ['postcode', 'type', 'session_id']}]
});

var AreaSampleSize = sequelize.define('AreaSampleSize', {
  postcode    : {type: Sequelize.STRING(10), allowNull: false},
  flats       : {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
  semiDetached: {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0, field: 'semi_detached'},
  detached    : {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
  terraced    : {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0},
  unknown     : {type: Sequelize.INTEGER, allowNull: false, defaultValue: 0}
}, {
  timestamps: false,
  tableName: 'area_sample_sizes',
  indexes: [{unique: true, fields: ['postcode', 'session_id']}]
});

var CrawlSession = sequelize.define('CrawlSession', {
}, {
  timestamps: true,
  updatedAt: false,
  underscored: true,
  tableName: 'crawl_sessions'
});

CrawlSession.hasMany(AreaStats, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'areaStats',
    singular: 'areaStat'
  },
  onDelete: 'CASCADE'
});

CrawlSession.hasMany(AreaSampleSize, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'areaSampleSize',
    singular: 'areaSampleSizes'
  },
  onDelete: 'CASCADE'
});

module.exports = {
  shortName: 'zoopla',
  sequelize: sequelize,
  AreaStats: AreaStats,
  CrawlSession: CrawlSession,
  AreaSampleSize: AreaSampleSize
};
