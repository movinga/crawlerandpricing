var Sequelize = require('sequelize');
var sequelize = require('./sequelize').connect('iam');

//Defining Sequelize models
var Company = sequelize.define('Company', {
  country       : {type: Sequelize.STRING(3), allowNull: false},
  name          : {type: Sequelize.STRING(200), allowNull: false},
  membership    : {type: Sequelize.STRING(500)},
  affiliations  : {type: Sequelize.STRING(500)},
  mailingAddress: {type: Sequelize.STRING(500), field: 'mailing_address'},
  workAddress   : {type: Sequelize.STRING(500), field: 'work_address'},
  workPhone     : {type: Sequelize.STRING(100), field: 'work_phone'},
  mobilePhone   : {type: Sequelize.STRING(100), field: 'mobile_phone'},
  fax           : {type: Sequelize.STRING(100)},
  email         : {type: Sequelize.STRING(500)},
  website       : {type: Sequelize.STRING(100)},
  skype         : {type: Sequelize.STRING(50)},
  description   : {type: Sequelize.STRING(500)},
  businessTypes : {type: Sequelize.STRING(500)},
  licenses      : {type: Sequelize.STRING(500)},
  contactPersons: {type: Sequelize.STRING(500)},
}, {
  timestamps: false,
  tableName: 'companies'
});

var CrawlSession = sequelize.define('CrawlSession', {
}, {
  timestamps: true,
  updatedAt: false,
  underscored: true,
  tableName: 'crawl_sessions'
});

CrawlSession.hasMany(Company, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'companies',
    singular: 'company'
  },
  onDelete: 'CASCADE'
});

module.exports = {
  shortName: 'iam',
  Company: Company,
  CrawlSession: CrawlSession
};
