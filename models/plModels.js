var Sequelize = require('sequelize');
var sequelize = require('./sequelize').connect('primelocation');

var Property = sequelize.define('Property', {
  postcode: {type: Sequelize.STRING(10), allowNull: false},
  propertyType: {type: Sequelize.STRING(50), allowNull: false},
  price: {type: Sequelize.INTEGER},
  area: {type: Sequelize.DOUBLE, allowNull: false},
  bedrooms: {type: Sequelize.INTEGER}
}, {
  timestamps: false,
  tableName: 'properties',
});

var CrawlSession = sequelize.define('CrawlSession', {
}, {
  timestamps: true,
  updatedAt: false,
  underscored: true,
  tableName: 'crawl_sessions'
});

CrawlSession.hasMany(Property, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'properties',
    singular: 'property'
  },
  onDelete: 'CASCADE'
});

module.exports = {
  shortName: 'pl',
  sequelize: sequelize,
  Property: Property,
  CrawlSession: CrawlSession
};
