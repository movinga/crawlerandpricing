var Sequelize = require('sequelize');
var sequelize = require('./sequelize').connect('paruvendu');

var Property = sequelize.define('Property', {
  postcode: {type: Sequelize.STRING(5), allowNull: false},
  city: {type: Sequelize.STRING(100), allowNull: false},
  type: {type: Sequelize.STRING(50), allowNull: false},
  price: {type: Sequelize.DOUBLE},
  area: {type: Sequelize.DOUBLE, allowNull: false},
  bedrooms: {type: Sequelize.DOUBLE}
}, {
  timestamps: false,
  tableName: 'property'
});

var CrawlSession = sequelize.define('CrawlSession', {
}, {
  timestamps: true,
  updatedAt: false,
  underscored: true,
  tableName: 'crawl_sessions'
});

CrawlSession.hasMany(Property, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'properties',
    singular: 'property'
  },
  onDelete: 'CASCADE'
});

module.exports = {
  shortName: 'paruvendu',
  sequelize: sequelize,
  Property: Property,
  CrawlSession: CrawlSession
};
