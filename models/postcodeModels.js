var Sequelize = require('sequelize');
var sequelize = require('./sequelize').connect('postcodes');

var UKDistrictPostcode = sequelize.define('UKDistrictPostcode', {
  postcode: {type: Sequelize.STRING(10), allowNull: false},
  address1: {type: Sequelize.STRING(100), allowNull: false, field: 'address_1'},
  address2: {type: Sequelize.STRING(100), field: 'address_2'},
}, {
  timestamps: false,
  tableName: 'uk_district_postcodes',
  indexes: [{unique: true, fields: ['postcode', 'address_1']}]
});

var UKFullPostcode = sequelize.define('UKFullPostcode', {
  postcode:   {type: Sequelize.STRING(10), allowNull: false},
  address1:   {type: Sequelize.STRING(100), field: 'address_1'},
  address2:   {type: Sequelize.STRING(100), field: 'address_2'},
  address3:   {type: Sequelize.STRING(100), field: 'address_3'},
  address4:   {type: Sequelize.STRING(100), field: 'address_4'},
  latitude:   {type: Sequelize.DOUBLE},
  longitude:  {type: Sequelize.DOUBLE}
}, {
  timestamps: false,
  tableName: 'uk_full_postcodes',
  indexes: [{unique: true, fields: ['postcode']}]
});

var ITPostcode = sequelize.define('ItalyPostcode', {
  postcode:   {type: Sequelize.STRING(10), allowNull: false},
  address1:   {type: Sequelize.STRING(100), field: 'address_1'},
  address2:   {type: Sequelize.STRING(100), field: 'address_2'},
  address3:   {type: Sequelize.STRING(100), field: 'address_3'},
  address4:   {type: Sequelize.STRING(100), field: 'address_4'},
  latitude:   {type: Sequelize.DOUBLE},
  longitude:  {type: Sequelize.DOUBLE}
}, {
  timestamps: false,
  tableName: 'it_postcodes',
  indexes: [{unique: true, fields: ['postcode', 'address_1']}]
});

var FRPostcode = sequelize.define('FrancePostcode', {
  postcode:   {type: Sequelize.STRING(10), allowNull: false},
  address1:   {type: Sequelize.STRING(100), field: 'address_1'},
  address2:   {type: Sequelize.STRING(100), field: 'address_2'},
  address3:   {type: Sequelize.STRING(100), field: 'address_3'},
  address4:   {type: Sequelize.STRING(100), field: 'address_4'},
  latitude:   {type: Sequelize.DOUBLE},
  longitude:  {type: Sequelize.DOUBLE}
}, {
  timestamps: false,
  tableName: 'fr_postcodes',
  indexes: [{unique: true, fields: ['postcode', 'address_1']}]
});

module.exports = {
  shortName: 'postcode',
  uk: {
    DistrictPostcode: UKDistrictPostcode,
    FullPostcode: UKFullPostcode,
  },
  FRPostcode: FRPostcode,
  ITPostcode: ITPostcode,
  sequelize: sequelize
};
