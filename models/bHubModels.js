var Sequelize = require('sequelize');
var sequelize = require('./sequelize').connect('baggagehub');

//Defining Sequelize models
var Rate = sequelize.define('Rate', {
  countryFrom:   {type: Sequelize.STRING(50), allowNull: false},
  cityFrom:      {type: Sequelize.STRING(50), allowNull: false},
  countryTo:     {type: Sequelize.STRING(50), allowNull: false},
  cityTo:        {type: Sequelize.STRING(50), allowNull: false},
  rateType:      {type: Sequelize.STRING(50), allowNull: false},
  rateTime:      {type: Sequelize.STRING(50), allowNull: false},
  numberOfBoxes: {type: Sequelize.INTEGER, allowNull: false},
  rate:          {type: Sequelize.DECIMAL(10, 2), allowNull: false, defaultValue: 0.00}
}, {
  timestamps: false,
  tableName: 'rates'
});

var CrawlSession = sequelize.define('CrawlSession', {
}, {
  timestamps: true,
  updatedAt: false,
  underscored: true,
  tableName: 'crawl_sessions'
});

CrawlSession.hasMany(Rate, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'rates',
    singular: 'rate'
  },
  onDelete: 'CASCADE'
});

module.exports = {
  shortName: 'bhub',
  Rate: Rate,
  CrawlSession: CrawlSession
};
