var Sequelize = require('sequelize');
var sequelize = require('./sequelize').connect('worldfreightrates');

//Defining Sequelize models
var City = sequelize.define('City', {
  id:       {type: Sequelize.CHAR(24), primaryKey: true},
  city:     {type: Sequelize.STRING(100), allowNull: false},
  country:  {type: Sequelize.STRING(30), allowNull: false}
}, {
  timestamps: false,
  tableName: 'cities_ocean',
  indexes: [{unique: true, fields: ['id']}]
});

var Rate = sequelize.define('Rate', {
  fromCity:         {type: Sequelize.STRING(100), allowNull: false, field: 'from_city'},
  fromCountry:      {type: Sequelize.STRING(100), allowNull: false, field: 'from_country'},
  toCity:           {type: Sequelize.STRING(100), allowNull: false, field: 'to_city'},
  toCountry:        {type: Sequelize.STRING(100), allowNull: false, field: 'to_country'},
  containerSize:    {type: Sequelize.INTEGER, allowNull: false, field: 'container_size'},
  insurance:        {type: Sequelize.BOOLEAN, allowNull: false, defaultValue: false},
  rate:             {type: Sequelize.DECIMAL(10, 2), allowNull: false, defaultValue: 0.00}
}, {
  timestamps: false,
  tableName: 'rates_ocean'
});

var CrawlSession = sequelize.define('CrawlSession', {
}, {
  timestamps: true,
  updatedAt: false,
  underscored: true,
  tableName: 'crawl_sessions_ocean'
});

CrawlSession.hasMany(Rate, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'rates',
    singular: 'rate'
  },
  onDelete: 'CASCADE'
});

module.exports = {
  shortName: 'wfr',
  City: City,
  Rate: Rate,
  CrawlSession: CrawlSession
};
