var Sequelize = require('sequelize');
var sequelize = require('./sequelize').connect('immobiliare');

var Property = sequelize.define('Property', {
  zone: {type: Sequelize.STRING(100)},
  commune: {type: Sequelize.STRING(100), allowNull: false},
  province: {type: Sequelize.STRING(100), allowNull: false},
  type: {type: Sequelize.STRING(50), allowNull: false},
  price: {type: Sequelize.DOUBLE, allowNull: false},
  area: {type: Sequelize.DOUBLE, allowNull: false},
  bedrooms: {type: Sequelize.DOUBLE, allowNull: false}
}, {
  timestamps: false,
  tableName: 'property'
});

var Commune = sequelize.define('Commune', {
  name: {type: Sequelize.STRING(100), allowNull: false},
  province: {type: Sequelize.STRING(100), allowNull: false},
  withZones: {type: Sequelize.BOOLEAN, allowNull: false}
}, {
  timestamps: false,
  tableName: 'communes'
});

var Zone = sequelize.define('Zone', {
  name: {type: Sequelize.STRING(100), allowNull: false}
}, {
  timestamps: false,
  tableName: 'zones',
});

var CrawlSession = sequelize.define('CrawlSession', {
}, {
  timestamps: true,
  updatedAt: false,
  underscored: true,
  tableName: 'crawl_sessions'
});

CrawlSession.hasMany(Property, {
  foreignKey: {
    name: 'session_id',
  },
  as: {
    plural: 'properties',
    singular: 'property'
  },
  onDelete: 'CASCADE'
});

Commune.hasMany(Zone, {
  foreignKey: {
    name: 'commune_id',
  },
  as: {
    plural: 'zones',
    singular: 'zone'
  },
  onDelete: 'CASCADE'
});

module.exports = {
  shortName: 'immobiliare',
  sequelize: sequelize,
  Property: Property,
  Zone: Zone,
  Commune: Commune,
  CrawlSession: CrawlSession
};
