var ParuvenduModels = require('../models/paruvenduModels');
var Property = ParuvenduModels.Property;
var CrawlSession = ParuvenduModels.CrawlSession;

var rest = require('restler');
var htmlparser = require('htmlparser2');
var async = require('async');
require('../helpers.js');
var logger = require('../logger.js');

function populateRequestQueue(requestQueue, crawlSessionId) {
  rest.get('http://www.paruvendu.fr/immobilier/annonceimmofo/liste/listeAnnonces', {
    timeout: 10000,
    query: {
      tt: 1,
      nbp0: 99,
      pa: 'FR',
      tbApp: 1,
      tbAtl: 1,
      tbCha: 1,
      tbDup: 1,
      tbFer: 1,
      tbHot: 1,
      tbLof: 1,
      tbMai: 1,
      tbMou: 1,
      tbPla: 1,
      tbPro: 1,
      tbVil: 1
    }
  }).on('success', function(data) {
    var numberOfPages = parseNumberOfPages(data);
    parsePropertiesPage(data);
    crawler.initialQueueLength = numberOfPages;
    for (var i = 2; i <= numberOfPages; i++) {
      requestQueue.push(i, function(page, properties) {
        saveProperties(properties, crawlSessionId);
        logger.debug('page %s/%s done!', page, numberOfPages);
      });
    }
  }).on('timeout', function() {
    logger.warn('[Paruvendu] Timeout in populateRequestQueue!');
    populateRequestQueue(requestQueue, crawlSessionId);
  });
}

function parseNumberOfPages(data) {
  var numberOfPages = 0;

  var parser = new htmlparser.Parser({
    insideDiv: false,
    insideStrong: false,

    onopentag: function(name, attributes) {
      if (name === 'div' && attributes.class === 'affin15rechlist-nbann') {
        this.insideDiv = true;
      }
      if (this.insideDiv && name === 'strong') {
        this.insideStrong = true;
      }
    },

    onclosetag: function(name) {
      if (this.insideStrong && name === 'strong') {
        this.insideStrong = false;
      } else if (this.insideDiv && name === 'div') {
        this.insideDiv = false;
      }
    },

    ontext: function(text) {
      if (this.insideStrong) {
        totalPages = parseInt(text.replace(/[^0-9]/g, ''));
        numberOfPages = Math.floor(totalPages / 15) + 1;
      }
    }
  });

  parser.write(data);
  parser.end();
  console.log(numberOfPages);
  return numberOfPages;
}

function requestPropertiesPage(page, callback) {
  rest.get('http://www.paruvendu.fr/immobilier/annonceimmofo/liste/listeAnnonces', {
    timeout: 10000,
    query: {
      p: page,
      tt: 1,
      nbp0: 99,
      pa: 'FR',
      tbApp: 1,
      tbAtl: 1,
      tbCha: 1,
      tbDup: 1,
      tbFer: 1,
      tbHot: 1,
      tbLof: 1,
      tbMai: 1,
      tbMou: 1,
      tbPla: 1,
      tbPro: 1,
      tbVil: 1
    }
  }).on('success', function(data) {
    var properties = parsePropertiesPage(data);
    callback(page, properties);
  }).on('timeout', function() {
    logger.warn('[Paruvendu] Timeout in requestPropertiesPage!');
    requestPropertiesPage(page, callback);
  })
}

function parsePropertiesPage(data) {
  var properties = [];

  var parser = new htmlparser.Parser({
    insideAd: false,
    insideDesc: false,
    insideCity: false,
    insidePrice: false,

    property: {
      postcode: undefined,
      city: undefined,
      type: undefined,
      price: undefined,
      area: undefined,
      bedrooms: undefined
    },

    onopentag: function(name, attributes) {
      if (this.insideDesc) {
        if (name === 'cite') {
          this.insideCity = true;
        }
      }

      if (this.insideAd) {
        if (name === 'h3') {
          this.insideDesc = true;
        }
        if (name === 'span' && attributes.class === 'price') {
          this.insidePrice = true;
        }
      }

      if (name === 'div' && attributes.hasOwnProperty('class') && attributes.class.startsWith('lazyload_bloc annonce')) {
        this.insideAd = true;
      }
    },

    onclosetag: function(name) {
      if (this.insideCity && name === 'cite') {
        this.insideCity = false;
      } else if (this.insideDesc && name === 'h3') {
        this.insideDesc = false;
      } else if (this.insidePrice && name === 'span') {
        this.insidePrice = false;
      } else if (this.insideAd && name === 'div') {
        this.insideAd = false;

        if (this.property.city && this.property.postcode && this.property.area) {

          properties.push({
            postcode: this.property.postcode,
            city: this.property.city,
            type: this.property.type,
            price: this.property.price,
            area: this.property.area,
            bedrooms: this.property.bedrooms
          });

          this.property = {
            postcode: undefined,
            city: undefined,
            type: undefined,
            price: undefined,
            area: undefined,
            bedrooms: undefined
          };
        }
      }
    },

    ontext: function(text) {
      if (this.insideDesc) {
        if (this.insideCity) {
          // Retrieve city and postcode info
          // Example: Arces-Dilo (89320)
          text = text.trim();
          var propertyPattern = new RegExp("^.*?\([0-9]\).*$");
          if (propertyPattern.test(text)) {
            var splittedText = text.split(' ');
            this.property.postcode = splittedText.pop().replace(/[^0-9]/g, '');
            this.property.city = splittedText.join(' ');
          } else if (text.startsWith('Paris') ||
                      text.startsWith('Lyon') ||
                      text.startsWith('Marseille')) {
            logger.info(text);
            this.property.city = text;
            this.property.postcode = 0;
            //TODO: Find postcode in FR Postcodes database
          } else {
            logger.warn('%s has not passed regexp', text);
          }
        } else {
          // Retrieve property type, area and number of bedrooms info
          // Example: Vente - Maison - 97 m² environ - 6 pièces
          // Sometimes the number of rooms and area are messed up
          splittedText = text.split('\r\n- ');
          if (splittedText.length >= 3) {
            splittedText.shift(); //Remove 'Vente'
            this.property.type = splittedText.shift();
            splittedText.forEach(function(segment) {
              if (segment.hasSubstring('pi�ces')) {
                //It's the number of bedrooms
                var bedrooms = segment.replace(/[^0-9\/]/g, '');
                if (bedrooms.hasSubstring('/')) {
                  //Border case: 2/3 or 3/4 bedrooms
                  splittedBedrooms = bedrooms.split('/');
                  this.property.bedrooms = (parseInt(splittedBedrooms[0]) + parseInt(splittedBedrooms[1])) / 2
                } else {
                  this.property.bedrooms = bedrooms;
                }
              } else if (segment.hasSubstring('m�')) {
                //It's an area
                this.property.area = segment.replace(/[^0-9]/g, '');
              }
            }.bind(this));
          }
        }
      } else if (this.insidePrice) {
        if (text.hasSubstring('&euro;')) {
          // Retrieve price by using magic regex replace
          this.property.price = text.replace(/[^0-9]/g, '');
        }
      }
    }
  });

  parser.write(data);
  parser.end();
  return properties;
}

function saveProperties(properties, crawlSessionId) {
  // Assuming that all sanity checks have been performed before
  properties.forEach(function(property) {
    property['session_id'] = crawlSessionId;
    Property.create(property);
  });
}

var crawler = {
  longName: 'Paruvendu.fr property details crawler',
  shortName: 'paruvendu',
  status: 'off',
  initialQueueLength: 0,
  crawlingSession: {},

  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(requestParams, callback) {
    requestPropertiesPage(requestParams, callback);
  }, 2),

  startCrawl: function() {
    crawler.status = 'on';

    crawler.requestQueue.drain = function() {
      crawler.status = 'off';
      crawler.crawlingSession = {};
      logger.info('Paruvendu crawling successful!');
    };

    logger.info('Started crawling Paruvendu');
    //Create a new crawl session
    CrawlSession.create().then(function(session) {
      crawler.crawlingSession = session;
      populateRequestQueue(crawler.requestQueue, session.id);
    });
  },

  pauseCrawl: function() {
    logger.info('Paused crawling Paruvendu');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling Paruvendu');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling Paruvendu');
    crawler.status = 'off';
    crawler.crawlSession = {};
    crawler.requestQueue.kill();
  }
}

module.exports = crawler;
