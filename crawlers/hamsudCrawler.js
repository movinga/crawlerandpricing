var HamSudModels = require('../models/hamsudModels');
var Rate = HamSudModels.Rate;
var CrawlSession = HamSudModels.CrawlSession;

var rest = require('restler');
var htmlparser = require('htmlparser2');
var async = require('async');
var he = require('he');
require('../helpers.js');

var logger = require('../logger.js');

var requestParameters = {};
var cookies = {};

var populateRequestQueue = function(requestQueue, crawlSessionId) {
  //TODO: initiate GET request to the main page
  // Extract cookies and session variables
  // Parse port names and push them to the request queue
  rest.get('https://ecom.hamburgsud.com/ecom/en/ecommerce_portal/tarifs_and_surcharges/thc_calc/ep_thc_calculator.xhtml?lang=EN', {
    timeout: 10000
  }).on('complete', function(data, response) {
    cookies = response.headers['set-cookie'];
    parseRequestParameters(data);
    var ports = parsePorts(data);
    crawler.initialQueueLength = ports.length;
    ports.forEach(function(port) {
      requestQueue.push({
        port: port,
        crawlSessionId: crawlSessionId
      }, function() {
        logger.debug('Finished parsing rates for %s', port.name);
      });
    });
  }).on('timeout', function() {
    logger.error('[HamSud] Timeout reached at populateRequestQueue');
    populateRequestQueue(requestQueue);
  });
}

var parseRequestParameters = function(data) {
  var parser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'input' && attributes.type === 'hidden') {
        requestParameters[attributes.name] = attributes.value;
      }
    }
  });

  parser.write(data);
  parser.end();
}

var parsePorts = function(data) {
  var ports = [];

  var parser = new htmlparser.Parser({
    port: {
      name: '',
      id: ''
    },
    insideOption: false,

    onopentag: function(name, attributes) {
      if (name === 'option') {
        this.insideOption = true;
        this.port.id = attributes.value;
      }
    },

    onclosetag: function(name) {
      if (this.insideOption && name === 'option') {
        this.insideOption = false;
        ports.push({
          name: this.port.name,
          id: this.port.id
        });
        this.port.name = '';
        this.port.id = '';
      }
    },

    ontext: function(text) {
      if (this.insideOption) {
        //Remove the port code
        var splittedText = text.split(' ');
        splittedText.pop();
        this.port.name = splittedText.join(' ');
      }
    }
  });

  parser.write(data);
  parser.end();
  return ports;
}

var requestRates = function(requestVariables, callback) {
  rest.post('https://ecom.hamburgsud.com/ecom/en/ecommerce_portal/tarifs_and_surcharges/thc_calc/ep_thc_calculator.xhtml?lang=EN', {
    headers: {
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:43.0) Gecko/20100101 Firefox/43.0',
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'Accept-Encoding': 'gzip, deflate',
      'Cookie': cookies.join(';')
    },
    data: {
      'j_idt34:j_idt36': requestParameters['j_idt34:j_idt36'],
      'javax.faces.ViewState': requestParameters['javax.faces.ViewState'],
      'ice.window': requestParameters['ice.window'],
      'ice.view': requestParameters['ice.view'],
      'j_idt34:j_idt36:thcPort': requestVariables.port.id,
      'icefacesCssUpdates': '',
      'javax.faces.source': 'j_idt34:j_idt36:thcPort',
      'javax.faces.partial.execute': 'j_idt34:j_idt36:thcPort',
      'javax.faces.partial.render': '@all',
      'ice.focus': 'j_idt34:j_idt36:thcPort',
      'ice.event.target': 'j_idt34:j_idt36:thcPort',
      'ice.event.captured': 'j_idt34:j_idt36:thcPort',
      'ice.event.type': 'onunknown',
      'javax.faces.behavior.event': 'change',
      'javax.faces.partial.event': 'change',
      'javax.faces.partial.ajax': 'true'
    }
  }).on('complete', function(data) {
    parseFreightRates(data, requestVariables.port, requestVariables.crawlSessionId);
    callback();
  });
}

var parseFreightRates = function(data, port, crawlSessionId) {
  var parser = new htmlparser.Parser({
    insideRow: false,
    insideRateType: false,
    insideCurrency: false,
    insideRate: false,

    rate: {
      'session_id': crawlSessionId,
      portName: port.name,
      portId: port.id,
      rateType: '',
      currency: '',
      containerType: '',
      rate: ''
    },

    onopentag: function(name, attributes) {
      if (name === 'tr' && attributes.hasOwnProperty('id') && attributes.id.startsWith('j_idt34:j_idt36:searchResult')) {
        this.insideRow = true;
      }

      if (this.insideRow && name === 'span' && attributes.hasOwnProperty('id')) {
        var splittedId = attributes.id.split(':');
        switch (splittedId.pop()) {
          case 'j_idt44':
            this.insideRateType = true;
            break;
          case 'j_idt47':
            this.insideCurrency = true;
            break;
          case 'j_idt50':
            this.rate.containerType = '20\' Dry';
            this.insideRate = true;
            break;
          case 'j_idt53':
            this.rate.containerType = '40\' Dry';
            this.insideRate = true;
            break;
          case 'j_idt56':
            this.rate.containerType = '20\' Reefer';
            this.insideRate = true;
            break;
          case 'j_idt59':
            this.rate.containerType = '40\' Reefer';
            this.insideRate = true;
            break;
        }
      }
    },

    onclosetag: function(name) {
      if (name === 'span') {
        if (this.insideRate) {
          this.insideRate = false;
          Rate.create(this.rate);
        } else if (this.insideCurrency) {
          this.insideCurrency = false;
        } else if (this.insideRateType) {
          this.insideRateType = false;
        }
      } else if (this.insideRow && name === 'tr') {
        this.insideRow = false;
        this.rate.rateType = '';
        this.rate.currency = '';
        this.rate.containerType = '';
        this.rate.rate = '';
      }

    },

    ontext: function(text) {
      if (this.insideRateType) {
        this.rate.rateType = text;
      } else if (this.insideCurrency) {
        this.rate.currency = text;
      } else if (this.insideRate) {
        this.rate.rate = text;
      }
    }
  });

  parser.write(data);
  parser.end();
}

var crawler = {
  longName: 'Hamburg Süd freight rates crawler',
  shortName: 'hamsud',
  status: 'off',
  initialQueueLength: 0,
  crawlingSession: {},

  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(requestVariables, callback) {
    requestRates(requestVariables, callback);
  }, 1),

  startCrawl: function() {
    crawler.status = 'on';

    crawler.requestQueue.drain = function() {
      crawler.status = 'off';
      crawler.crawlingSession = {};
      logger.info('Hamburg Süd crawling successful!');
    };

    logger.info('Started crawling Hamburg Süd');
    CrawlSession.create().then(function(session) {
      crawler.crawlingSession = session;
      populateRequestQueue(crawler.requestQueue, session.id);
    });
  },

  pauseCrawl: function() {
    logger.info('Paused crawling Hamburg Süd');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling Hamburg Süd');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling Hamburg Süd');
    crawler.status = 'off';
    crawler.crawlSession = {};
    crawler.requestQueue.kill();
  }
}

module.exports = crawler;
