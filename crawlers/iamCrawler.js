var IAMModels = require('../models/iamModels');
var Company = IAMModels.Company;
var CrawlSession = IAMModels.CrawlSession;

var rest = require('restler');
var async = require('async');
var htmlparser = require('htmlparser2');
require('../helpers.js');

var logger = require('../logger.js');

var populateRequestQueue = function(requestQueue, crawlSessionId) {
  var isCountrySelect = false;
  var countries = [];

  var initialParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      //Grab country information:
      if (name == 'select' && attributes.name == 'p_country_cd') {
        isCountrySelect = true;
      }

      if (name == 'option' && isCountrySelect) {
        countries.push(attributes.value);
      }
    },

    onclosetag: function(name) {
      if (name == 'select' && isCountrySelect) {
        isCountrySelect = false;
      }
    }
  });

  rest.get('https://online.iamovers.org/iamssa/iamcenssacustlkup.query_page', {
  }).on('complete', function(data) {
    initialParser.write(data);
    initialParser.end();
    countries.shift();
    crawler.initialQueueLength = countries.length;
    console.log(countries.length);
    countries.forEach(function(country) {
      console.log('Added country %s', country);
      requestQueue.push({
        crawlSessionId: crawlSessionId,
        country: country
      }, function() {
        logger.debug('Item parsing complete!');
      })
    });
  });
};

var performSearchQuery = function(requestParams, callback) {
  var crawlSessionId = requestParams.crawlSessionId;
  var country = requestParams.country
  var dataString = 'p_cust_id=&p_debug_fl=&p_subgroup_id=IAM&p_subclass_cd=&p_company_nm=&p_city_nm=&p_state_cd=%25null%25&p_postal_cd=&p_country_cd={0}&p_email=&p_configured_parm1_ty=&p_configured_parm1=&p_configured_parm2_ty=&p_configured_parm2=&p_configured_parm3_ty=&p_configured_parm3=&p_configured_parm4_ty=&p_configured_parm4=&p_configured_parm5_ty=&p_configured_parm5=&p_configured_parm6_ty=&p_configured_parm6=&p_configured_parm7_ty=&p_configured_parm7=&p_configured_parm8_ty=&p_configured_parm8=&p_configured_parm9_ty=&p_configured_parm9=&p_configured_parm10_ty=&p_configured_parm10=&p_recs_per_page=50&p_partial_match_fl=Y&p_match_on=ALL&SubmitButton=Query'.format(country);
  rest.post('https://online.iamovers.org/iamssa/iamcenssacustlkup.result_page', {
    data: dataString
  }).on('complete', function(data) {
    parseSearchResults(crawlSessionId, country, data, callback);
  });
};

var parseSearchResults = function(crawlSessionId, country, data, callback) {
  var companyTable = false;
  var nameTag = false;
  var categoryTag = false;
  var dataString = false;
  var skipText = false;
  var category = '';
  var subcategory = '';

  var company = {};

  var searchResultsParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      //When we enter the table -> set companyTable
      if (name === 'table' && attributes.class === 'aaCenssacustlkupLogo') {
        companyTable = true;
        //Reset the company
        company = {
          name: '',
          membership: [],
          affiliations: [],
          mailingAddress: [],
          workAddress: [],
          workPhone: [],
          mobilePhone: [],
          fax: [],
          email: [],
          website: '',
          skype: '',
          description: '',
          businessTypes: [],
          licenses: [],
          contactPersons: []
        };
      }
      //When we enter category tag -> set categoryTag
      if (name === 'b' && companyTable) {
        category = '';
        categoryTag = true;
      }
      //When we enter table data for -> set dataString
      if (name === 'td' && companyTable) {
        if (attributes.valign === 'TOP') {
          console.log('Found a name tag!');
          nameTag = true;
        } else {
          dataString = true
        }
      }
    },

    onclosetag: function(name) {
      //When we exit the table -> send the company
      if (name === 'table') {
        if (companyTable) {
          console.log(company);
          Company.create({
            'session_id': crawlSessionId,
            country: country,
            name: company.name,
            membership: company.membership.join(', '),
            affiliations: company.affiliations.join(', '),
            mailingAddress: company.mailingAddress.join(', '),
            workAddress: company.workAddress.join(', '),
            workPhone: company.workPhone.join(', '),
            mobilePhone: company.mobilePhone.join(', '),
            fax: company.fax.join(', '),
            email: company.email.join(', '),
            website: company.website,
            skype: company.skype,
            description: company.description,
            businessTypes: company.businessTypes.join(', '),
            licenses: company.licenses.join(', '),
            contactPersons: company.contactPersons.join(', ')
          }, {
            raw: true
          });
        }
        companyTable = false;
      }
      //When we exit the categoryTag -> deselect categoryTag
      if (name === 'b') {
        categoryTag = false;
      }
      //When we exit table data for -> deselect dataString
      if (name === 'td') {
        dataString = false;
        nameTag = false;
      }
    },

    ontext: function(text) {
      text = text.replace(/(\r\n|\n|\r)/gm,"").trim();
      splittedText = text.split(': ');
      if (companyTable && text.length > 0) {
        //if categoryTag -> Set category
        if (nameTag) {
          company.name = text;
        } else if (categoryTag) {
          category = text;
          subcategory = '';
        } else if (dataString) {
          switch (category) {
            case 'Membership':
              //Just push membership data to array;
              company.membership.push(text);
              break;
            case 'Addresses':
              //Set the subcategory according to the string contents
              if (text.hasSubstring('Mailing Address')) {
                subcategory = 'mailing_address';
                skipText = true;
              }
              if (text.hasSubstring('Work Address')) {
                subcategory = 'work_address';
                skipText = true;
              }
              break;
            case 'Phones':
              //Split the text by ': ', set the category according to the first part.
              if (splittedText[0].hasSubstring('Work Phone')) {
                subcategory = 'work_phone';
              }
              if (splittedText[0].hasSubstring('Mobile')) {
                subcategory = 'mobile_phone';
              }
              if (splittedText[0].hasSubstring('Fax')) {
                subcategory = 'fax';
              }
              break;
            case 'E-Contact':
              //Check the first text, than skip the line
              if (text.hasSubstring('Email')) {
                subcategory = 'email';
                skipText = true;
              }
              if (text.hasSubstring('Website')) {
                subcategory = 'website';
                skipText = true;
              }
              if (text.hasSubstring('Skype')) {
                subcategory = 'skype';
              }
              break;
            case 'Affiliation(s)':
              //Just push affiliations data to an array.
              company.affiliations.push(text);
              break;
            case 'Company Description':
              //Just set the company description
              company.description = text;
              break;
            case 'Business Type(s)':
              //Just push business types to an array.
              company.businessTypes.push(text);
              break;
            case 'Company Licenses':
              //Just push company to an array.
              company.licenses.push(text);
              break;
            case 'Point(s) of Contact':
              //Just push the contact person to an array;
              company.contactPersons.push(text);
              break;
          }

          switch (subcategory) {
            case 'work_address':
              if (skipText) {
                skipText = false;
              } else {
                company.workAddress.push(text);
              }
              break;
            case 'mailing_address':
              if (skipText) {
                skipText = false;
              } else {
                company.mailingAddress.push(text);
              }
              break;
            case 'work_phone':
              company.workPhone.push(splittedText[1]);
              break;
            case 'mobile_phone':
              company.mobilePhone.push(splittedText[1]);
              break;
            case 'fax':
              break;
            case 'email':
              if (skipText) {
                skipText = false
              } else {
                company.email.push(text);
              }
              break;
            case 'website':
              if (skipText) {
                skipText = false
              } else {
                company.website = text;
              }
              break;
            case 'skype':
              company.skype = splittedText[1];
              break;
          }
        }
      }
    }
  });

  searchResultsParser.write(data);
  searchResultsParser.end();
  callback();
}

var crawler = {
  longName: 'International Association of Movers',
  shortName: 'iam',
  status: 'off',
  initialQueueLength: 0,
  crawlingSession: {},
  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(requestParams, callback) {
    performSearchQuery(requestParams, callback);
  }, 1),

  startCrawl: function() {
    logger.info('Started crawling IAM');
    crawler.status = 'on';
    crawler.requestQueue.drain = function() {
      crawler.status = 'off';
      crawler.crawlingSession = {};
      console.log('Parsing successful!');
    };
    //Create a new crawl session
    CrawlSession.create().then(function(session) {
      crawler.crawlingSession = session;
      populateRequestQueue(crawler.requestQueue, session.id);
    });
  },

  pauseCrawl: function() {
    logger.info('Paused crawling IAM');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling IAM');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling IAM');
    crawler.status = 'off';
    crawler.crawlSession = {};
    crawler.requestQueue.kill();
  }
};

module.exports = crawler;
