var PlModels = require('../models/plModels');
var Property = PlModels.Property;
var CrawlSession = PlModels.CrawlSession;

var PostcodeCrawler = require('../crawlers/postcodeCrawler');
var PostcodeModels = require('../models/postcodeModels');
var DistrictPostcode = PostcodeModels.uk.DistrictPostcode;

var rest = require('restler');
var htmlparser = require('htmlparser2');
var async = require('async');
var logger = require('../logger.js');
require('../helpers.js');

var checkPostcodes = function(callback) {
  //Count district postcodes. If the number is > 1000 -> no futher crawling needed.
  PostcodeModels.sequelize.query('SELECT count(DISTINCT(postcode)) from uk_district_postcodes').then(function(response) {
    var count = response[0][0].count;
    callback(count > 1000 ? false : true);
  });
}

var populateRequestQueue = function(requestQueue, crawlSessionId) {
  PostcodeModels.sequelize.query('SELECT DISTINCT postcode FROM uk_district_postcodes', {model: DistrictPostcode, raw: true}).then(function(postcodes) {
    logger.info('Total district posctode count: %s', postcodes.length);
    crawler.initialQueueLength = postcodes.length;
    postcodes.forEach(function(postcode) {
      requestQueue.push({
        crawlSessionId: crawlSessionId,
        postcode: postcode.postcode
      }, function() {
        logger.debug('Request complete for postcode %s', postcode.postcode);
      });
    });
  });
}

var parseProperties = function(data, requestParams) {
  var postcode = requestParams.postcode;
  var crawlSessionId = requestParams.crawlSessionId;
  var parser = new htmlparser.Parser({
    property: {
      propertyType: undefined,
      price: undefined,
      area: undefined,
      bedrooms: 0
    },

    insideAd: false,
    insidePrice: false,
    insideBedrooms: false,
    insideArea: false,
    insideType: false,
    insideInterface: false,
    insideAddress: false,

    onopentag: function(name, attributes) {
      if (this.insideBedrooms || this.insideArea) {
        if (name === 'span' && attributes.hasOwnProperty('class') && attributes.class.hasSubstring('interface')) {
          this.insideInterface = true;
        }
      }
      if (this.insideAd) {
        if (name === 'a' && attributes.hasOwnProperty('class') && attributes.class.hasSubstring('listing-results-price')) {
          this.insidePrice = true;
        }
        if (name === 'a' && attributes.hasOwnProperty('class') && attributes.class.hasSubstring('listing-results-address')) {
          this.insideAddress = true;
        }
        if (name === 'a' && attributes.hasOwnProperty('itemprop') && attributes.itemprop === 'name') {
          this.insideType = true;
        }
        if (name === 'span' && attributes.hasOwnProperty('class')) {
          if (attributes.class.hasSubstring('num-beds')) {
            this.insideBedrooms = true;
          }
          if (attributes.class.hasSubstring('num-sqft')) {
            this.insideArea = true;
          }
        }
      }
      if (name === 'li' && attributes.hasOwnProperty('data-listing-id')) {
        this.insideAd = true;
      }
    },

    onclosetag: function(name) {
      if (this.insideInterface && name === 'span') {
        this.insideInterface = false;
      } else if (this.insideAddress && name === 'a') {
        this.insideAddress = false;
      } else if (this.insideType && name === 'a') {
        this.insideType = false;
      } else if (this.insideArea && name === 'span') {
        this.insideArea = false;
      } else if (this.insideBedrooms && name === 'span') {
        this.insideBedrooms = false;
      } else if (this.insidePrice && name === 'a') {
        this.insidePrice = false;
      } else if (this.insideAd && name === 'li') {
        this.insideAd = false;
        saveProperty({
          'session_id': crawlSessionId,
          postcode: postcode,
          propertyType: this.property.propertyType,
          price: this.property.price,
          area: this.property.area,
          bedrooms: this.property.bedrooms
        });
        this.property.propertyType = undefined;
        this.property.price = undefined;
        this.property.area = undefined;
        this.property.bedrooms = 0;
      }
    },

    ontext: function(text) {
      if (this.insideType) {
        text = text.toLowerCase();
        //Look for text occurences here
        if (text.hasSubstring('terraced') || text.hasSubstring('end terrace') || text.hasSubstring('mews')) {
          this.property.propertyType = 'Terraced';
        } else if (text.hasSubstring('semi-detached') || text.hasSubstring('town house')) {
          this.property.propertyType = 'Semi-detached';
        } else if (text.hasSubstring('detached') || text.hasSubstring('cottage') || text.hasSubstring('barn') || text.hasSubstring('lodge') || text.hasSubstring('duplex') || text.hasSubstring('country house') || text.hasSubstring('farmhouse') || text.hasSubstring('mobile/park') || text.hasSubstring('bungalow')) {
          this.property.propertyType = 'Detached';
        } else if (text.hasSubstring('flat') || text.hasSubstring('apartment') || text.hasSubstring('maisonette') || text.hasSubstring('studio') || text.hasSubstring('penthouse')) {
          this.property.propertyType = 'Flat';
        } else if (text.hasSubstring('property') || text.hasSubstring('garage') || text.hasSubstring('block of flats') || text.hasSubstring('houseboat') || text.hasSubstring('land')) {
          //Do not include
          this.property.propertyType = undefined;
        } else {
          //Do not include
          this.property.propertyType = undefined;
          logger.warn('New property type found: %s', text);
        }
      } else if (this.insideAddress) {
        text = text.split(' ');
        if (postcode !== text[text.length - 1]) {
          //Do not include
          this.property.propertyType = undefined;
        }
      } else if (this.insideArea && !this.insideInterface) {
        text = text.replace(/[^0-9\-]/g, '');
        if (text.hasSubstring('-')) {
          text = text.split('-');
          this.property.area = Math.floor((parseInt(text[0]) + parseInt(text[1])) / 2);
        } else if (!isNaN(parseInt(text))) {
          this.property.area = text;
        }
      } else if (this.insideBedrooms && !this.insideInterface) {
        text = text.replace(/\D/g, '');
        if (!isNaN(parseInt(text))) {
          this.property.bedrooms = text;
        }
      } else if (this.insidePrice) {
        text = text.replace(/\D/g, '');
        if (!isNaN(parseInt(text))) {
          this.property.price = text;
        }
      }
    }
  });

  parser.write(data);
  parser.end();
}

var saveProperty = function(property) {
  if (property.area && property.propertyType) {
    Property.create(property);
  }
}

var parseNumberOfPages = function(data) {
  var pageCount = 0;
  var parser = new htmlparser.Parser({
    insidePageCount: false,

    onopentag: function(name, attributes) {
      if (name === 'span' && attributes.hasOwnProperty('class') && attributes.class === 'listing-results-utils-count') {
        this.insidePageCount = true;
      }
    },

    onclosetag: function(name) {
      if (this.insidePageCount && name === 'span') {
        this.insidePageCount = false;
      }
    },

    ontext: function(text) {
      if (this.insidePageCount) {
        totalPages = parseInt(text.split('of')[1]);
        pageCount = Math.floor(totalPages / 100) + 1;
      }
    }
  });

  parser.write(data);
  parser.end();
  return pageCount;
}

var requestProperties = function(requestParams, requestCallback) {
  var postcode = requestParams.postcode;
  var crawlSessionId = requestParams.crawlSessionId;
  rest.get('http://www.primelocation.com/for-sale/property/{0}'.format(postcode), {
    query: {
      'include_retirement_homes': true,
      'new_homes': 'include',
      'include_shared_ownership': true,
      'radius': 0,
      'include_sold': true,
      'page_size': 100
    },
    timeout: 10000
  }).on('complete', function(data) {
    parseProperties(data, requestParams);
    var numberOfPages = parseNumberOfPages(data);
    if (numberOfPages > 1) {
      var currentPage =  2;
      async.whilst(
        function() {return currentPage <= numberOfPages},
        function(callback) {
          requestNextPage(postcode, currentPage, function(data) {
            parseProperties(data, requestParams);
            currentPage++;
            callback();
          });
        },
        function() {
          requestCallback();
        }
      )
    } else {
      requestCallback()
    }
  }).on('timeout', function() {
    logger.warn('[Primelocation] Timeout in requestProperties!');
    requestProperties(requestParams, requestCallback);
  });
}

var requestNextPage = function(postcode, page, callback) {
  rest.get('http://www.primelocation.com/for-sale/property/{0}'.format(postcode), {
    query: {
      'include_retirement_homes': true,
      'new_homes': 'include',
      'include_shared_ownership': true,
      'radius': 0,
      'include_sold': true,
      'page_size': 100,
      'pn': page
    },
    timeout: 10000
  }).on('complete', function(data) {
    callback(data);
  }).on('timeout', function() {
    logger.warn('[Primelocation] Timeout in requestNextPage!');
    requestNextPage(postcode, page, callback);
  });
}

var crawler = {
  longName: 'Primelocation area stats crawler',
  shortName: 'pl',
  status: 'off',
  initialQueueLength: 0,
  crawlingSession: {},
  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(requestParams, callback) {
    requestProperties(requestParams, callback);
  }, 2),

  startCrawl: function() {
    crawler.status = 'on';
    crawler.requestQueue.drain = function() {
      crawler.status = 'off';
      crawler.crawlingSession = {};
      logger.info('Primelocation crawling successful!');
    };

    logger.info('Started crawling Primelocation');
    //Create a new crawl session
    CrawlSession.create().then(function(session) {
      crawler.crawlingSession = session;
      checkPostcodes(function(noPostcodes) {
        if (noPostcodes) {
          logger.warn('Postcodes not found! Starting PostcodeCrawler');
          PostcodeCrawler.startCrawl(function() {
            populateRequestQueue(crawler.requestQueue, session.id);
          });
        } else {
          populateRequestQueue(crawler.requestQueue, session.id);
        }
      });
    });
  },

  pauseCrawl: function() {
    logger.info('Paused crawling Primelocation');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling Primelocation');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling Primelocation');
    crawler.status = 'off';
    crawler.crawlSession = {};
    crawler.requestQueue.kill();
  }
};

module.exports = crawler;
