var FEDEMACModels = require('../models/fedemacModels');
var City = FEDEMACModels.City;

var rest = require('restler');
var htmlparser = require('htmlparser2');
var async = require('async');
require('../helpers.js');

var logger = require('../logger.js');


var populateRequestQueue = function(requestQueue, countries) {
  crawler.initialQueueLength = countries.length * 2;
  countries.forEach(function(country) {
    ['city', 'town'].forEach(function(cityType) {
      requestQueue.push({
        country: country,
        cityType: cityType
      }, function() {
        logger.debug('%s extraction for %s is complete!', cityType, country);
      })
    });
  });
}

var requestCities = function(requestParams, callback) {
  var country = requestParams.country;
  var cityType = requestParams.cityType;
  if (country === 'Great Britain') {
    country = 'United Kingdom';
  }
  rest.post('http://overpass-api.de/api/interpreter', {
    data: {
      data: '[out:json];area["name:en"="{0}"];(node[place="{1}"](area););out;'.format(country, cityType)
    },
    timeout: 600000
  }).on('complete', function(data) {
    if (data.elements) {
      logger.debug('Returned %d values.', data.elements.length);
      data.elements.forEach(function(element) {
        City.upsert({
          country: country,
          type: cityType,
          name: element.tags.hasOwnProperty('name:en') ? element.tags['name:en'] : element.tags.name,
          population: element.tags.hasOwnProperty('population') ? element.tags.population.replace(/[.,]/g,"") : undefined
        });
      });
    } else {
      logger.debug('No values found!');
    }
    callback();
  });
}

var getCountries = function(callback) {
  rest.get('https://www.fedemac.eu/Find-Movers/Find-your-mover', {
    headers: {
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/601.2.7 (KHTML, like Gecko) Version/9.0.1 Safari/601.2.7',
      'Accept-Encoding': 'gzip, deflate'
    }
  })
  .on('complete', function(data, response) {
    parseCountries(data, callback);
  });
}

var parseCountries = function(data, callback) {
  var countries = [];
  var insideSelect = false;

  var countriesParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'select' && attributes.id === 'dnn_ctr484_CompanySearch_UC_CompanyFilterMask_ddl_country') {
        insideSelect = true;
      }

      if (name === 'option' && insideSelect) {
        countries.push(attributes.value);
      }
    },

    onclosetag: function(name) {
      if (name === 'select') {
        insideSelect = false;
      }
    },
  });

  countriesParser.write(data);
  countriesParser.end();
  callback(countries);
}

var crawler = {
  longName: 'Overpass city crawler (required for FEDEMAC)',
  shortName: 'overpass',
  status: 'off',
  initialQueueLength: 0,
  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(requestParams, callback) {
    requestCities(requestParams, callback);
  }, 1),

  startCrawl: function(callback) {
    crawler.status = 'on';
    crawler.requestQueue.drain = function() {
      crawler.status = 'off';
      console.log('Parsing successful!');
      callback();
    };

    logger.info('Started crawling city data from Overpass');
    getCountries(function(countries) {
      populateRequestQueue(crawler.requestQueue, countries);
    });
  },

  pauseCrawl: function() {
    logger.info('Paused crawling Overpass');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling Overpass');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling Overpass');
    crawler.status = 'off';
    crawler.crawlSession = {};
    crawler.requestQueue.kill();
  }
};

module.exports = crawler;
