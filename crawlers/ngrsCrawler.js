var NGRSModels = require('../models/ngrsModels');
var Company = NGRSModels.Company;
var CrawlSession = NGRSModels.CrawlSession;

var rest = require('restler');
var async = require('async');
var htmlparser = require('htmlparser2');
require('../helpers.js');

var logger = require('../logger.js');

var populateRequestQueue = function(requestQueue, crawlSessionId) {
  var uniqueId = "";
  var sessionId = "";

  var initialParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      //Grab all hidden inputs
      if (name == "input" && attributes.type == "hidden") {
        //Hidden input with unique session identifier
        if (attributes.name.length == 32) {
          uniqueId = attributes.name;
        }
        //Hidden input with another session id
        if (attributes.name == "ssid") {
          sessionId = attributes.value;
        }
      }
    }
  });

  rest.get('http://www.ngrs.co.uk/find-a-remover', {
    headers: {
      Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/601.2.7 (KHTML, like Gecko) Version/9.0.1 Safari/601.2.7',
      'Accept-Encoding': 'gzip, deflate'
    }
  })
  .on('complete', function(data) {
    initialParser.write(data);
    initialParser.end();
    performSearchQuery(requestQueue, crawlSessionId, uniqueId, sessionId);
  });
};


var performSearchQuery = function(requestQueue, crawlSessionId, uniqueId, sessionId) {
  var dataObject = {}
  dataObject['mj_rs_center_selector'] = 'Douglas IM1 4HR, Isle of Man';
  dataObject['mj_rs_radius_selector'] = '1000';
  dataObject['mj_rs_ref_lat'] = '54.1465178';
  dataObject['mj_rs_ref_lng'] = '-4.48761490000004';
  dataObject['ssid'] = sessionId;
  dataObject['sid'] = '54';
  dataObject['task'] = 'search.search';
  dataObject['option'] = 'com_sobipro';
  dataObject['Itemid'] = '158';
  dataObject[uniqueId] = '1';

  var cookie = 'SPro_ssid=' + sessionId;

  rest.post('http://www.ngrs.co.uk/index.php', {
    data: dataObject
  }).on('complete', function(data) {
    parseSearchResults(requestQueue, crawlSessionId, data, cookie);
  });
};

var parseSearchResults = function(requestQueue, crawlSessionId, data, cookie) {
  var numOfPages = 0;

  var searchResultsParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      //Link to the particular component
      if (name == "a" && attributes.href.startsWith('/component/sobipro')) {
        requestQueue.push({
          url: 'http://www.ngrs.co.uk' + attributes.href,
          cookie: cookie,
          crawlSessionId: crawlSessionId
        }, function() {
          logger.debug('Item parsing complete!');
        });
      }
    },

    //Detect total number of pages
    ontext: function(text) {
      if (text.startsWith('Found ')) {
        splittedLabel = text.split(' ');
        var totalResults = parseInt(splittedLabel[1]);
        logger.info('Total results - %s', totalResults);
        crawler.initialQueueLength = totalResults;
        if (totalResults % 8 == 0) {
          numOfPages = totalResults / 8;
        } else {
          numOfPages = Math.floor(totalResults / 8) + 1;
        }
      }
    }
  });

  //Parse the first page and get total number of pages
  searchResultsParser.write(data);
  searchResultsParser.end();
  logger.info('Total number of pages - %s', numOfPages);
  if (numOfPages > 1) {
    var currentPage = 2;
    async.whilst(
      function() {return currentPage <= numOfPages},
      function(callback) {
        requestPage(requestQueue, crawlSessionId, currentPage, cookie, callback);
        currentPage++;
      },
      function() {
        requestQueue.resume();
      }
    );
  }
}

var requestPage = function(requestQueue, crawlSessionId, pageNumber, cookie, callback) {
  logger.debug("Parsing page #%s", pageNumber);
  var pageParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      //Link to the particular component
      if (name == "a" && attributes.href.startsWith('/component/sobipro')) {
        requestQueue.push({
          url: 'http://www.ngrs.co.uk' + attributes.href,
          cookie: cookie,
          crawlSessionId: crawlSessionId
        }, function() {
          logger.debug('Item parsing complete!');
        });
      }
    }
  });

  rest.get('http://www.ngrs.co.uk/find-a-remover?task=search.results&sid=54&site=' + pageNumber, {
    headers: {
      Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/601.2.7 (KHTML, like Gecko) Version/9.0.1 Safari/601.2.7',
      'Accept-Encoding': 'gzip, deflate',
      Cookie: cookie
    }
  }).on('complete', function(data) {
    pageParser.write(data);
    pageParser.end();
    callback();
  });
};

var requestItem = function(requestParams, callback) {
  var url = requestParams.url;
  var cookie = requestParams.cookie;
  var crawlSessionId = requestParams.crawlSessionId

  logger.debug("Parsing item with url=%s", url);

  rest.get(url, {
    headers: {
      Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/601.2.7 (KHTML, like Gecko) Version/9.0.1 Safari/601.2.7',
      'Accept-Encoding': 'gzip, deflate',
      Cookie: cookie
    }
  }).on('complete', function(data) {
    parseItem(crawlSessionId, data, callback);
  });
};

var parseItem = function(crawlSessionId, data, callback) {
  var category = "";
  var skip = false;
  var dataHash = {};

  var itemParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name == 'strong') {
        skip = true;
      }

      if (attributes.hasOwnProperty('class')) {

        if (name == 'h1' && attributes.class == 'SPTitle') {
          category = 'field_name';
        }

        if (attributes.class.startsWith('spFieldsData')) {
          var dataFieldName = attributes.class.split(' ')[1];
          if (dataFieldName == 'field_website' || dataFieldName == 'field_email') {
            dataHash[dataFieldName] = attributes.href;
          } else {
            category = dataFieldName;
          }
        }
      }
    },

    ontext: function(text) {
      if (category != "") {
        if (skip) {
          skip = false;
        } else {
          dataHash[category] = text;
          category = '';
        }
      }
    }
  });

  itemParser.write(data);
  itemParser.end();
  //Add item to the database
  Company.create({
    'session_id': crawlSessionId,
    name:         dataHash['field_name'].replace(/&amp;/g, '&'),
    address1:     dataHash['field_addres1'],
    address2:     dataHash['field_addres2'],
    address3:     dataHash['field_addres3'],
    address4:     dataHash['field_addres4'],
    town:         dataHash['field_town'],
    county:       dataHash['field_county'],
    postcode:     dataHash['field_postcode'],
    phone:        dataHash['field_phone_number'],
    website:      dataHash['field_website'],
    email:        dataHash.hasOwnProperty('field_email') ? dataHash['field_email'].split(':')[1] : undefined
  }, {
    raw: true
  });
  callback();
}

var crawler = {
  longName: 'The national guild of removers and storers (NGRS)',
  shortName: 'ngrs',
  status: 'off',
  initialQueueLength: 0,
  crawlingSession: {},
  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(requestParams, callback) {
    requestItem(requestParams, callback);
  }, 1),

  startCrawl: function() {
    crawler.status = 'on';
    crawler.requestQueue.pause();
    crawler.requestQueue.drain = function() {
      crawler.status = 'off';
      crawler.crawlingSession = {};
      console.log('Parsing successful!');
    };

    logger.info('Started crawling NGRS');
    //Create a new crawl session
    CrawlSession.create().then(function(session) {
      crawler.crawlingSession = session;
      populateRequestQueue(crawler.requestQueue, session.id);
    });
  },

  pauseCrawl: function() {
    logger.info('Paused crawling NGRS');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling NGRS');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling NGRS');
    crawler.status = 'off';
    crawler.crawlSession = {};
    crawler.requestQueue.kill();
  }
};

module.exports = crawler;
