var BHubModels = require('../models/bHubModels');
var Rate = BHubModels.Rate;
var CrawlSession = BHubModels.CrawlSession;

var rest = require('restler');
var htmlparser = require('htmlparser2');
var async = require('async');
require('../helpers.js');
var logger = require('../logger.js');

var Chance = require('chance');
var chance = new Chance();

function getCountries(callback) {
  rest.get('http://www.baggagehub.com/get-a-quote', {
    timeout: 10000
  }).on('complete', function(data, response) {
    var countries = parseCountries(data);
    callback(countries);
  }).on('timeout', function() {
    logger.warn('[BHub] Timeout in getSessionVariables');
    getSessionVariables(callback);
  })
}

function getApiToken(callback) {
  rest.get('http://www.baggagehub.com/get-a-quote', {
    timeout: 10000
  }).on('complete', function(data, response) {
    var apiToken = parseApiToken(data);
    callback(apiToken);
  }).on('timeout', function() {
    logger.warn('[BHub] Timeout in getSessionVariables');
    getSessionVariables(callback);
  })
}

function parseApiToken(data) {
  var apiToken = '';

  var parser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'meta' && attributes.name === '_token') {
        apiToken = attributes.content;
      }
    }
  });

  parser.write(data);
  parser.end();
  return apiToken;
}

function parseCountries(data) {
  var countries = [];

  var parser = new htmlparser.Parser({
    insideSelect: false,
    insideOption: false,
    country: {
      id: undefined,
      text: undefined,
    },

    onopentag: function(name, attributes) {
      if (this.insideSelect && name === 'option') {
        this.insideOption = true;
        this.country.id = attributes.value;
      }
      if (name === 'select' && attributes.name === 'o_fcountry') {
        this.insideSelect = true;
      }
    },

    onclosetag: function(name) {
      if (this.insideOption && name === 'option') {
        this.insideOption = false;
        if (this.country.id && this.country.text) {
          countries.push({
            id: this.country.id,
            text: this.country.text
          });
          this.country.id = undefined;
          this.country.text = undefined;
        }
      }
      if (this.insideSelect && name === 'select') {
        this.insideSelect = false;
      }
    },

    ontext: function(text) {
      if (this.insideOption) {
        this.country.text = text;
      }
    }
  });

  parser.write(data);
  parser.end();
  //Remove the first element of the array, because it's 'Select Country...'
  return countries;
}

function requestCities(countryId, callback) {
  getApiToken(function(apiToken) {
    rest.post('http://www.baggagehub.com/booking-app/ajax-load-cities', {
      timeout: 10000,
      query: {
        '_token': apiToken,
        'req_city': '',
        'req_country': countryId
      },
      parser: rest.parsers.json,
    }).on('complete', function(data) {
      callback(data.cities);
    }).on('timeout', function() {
      logger.warn('[BHub] Timeout in requestCity');
      requestCities(countryId, apiToken, callback);
    });
  });
}

function populateRequestQueue(requestQueue, crawlSessionId) {
  getCountries(function(countries) {
    async.eachSeries(countries, function(country, callback) {
      requestCities(country.id, function(cities) {
        var boxes = [1, 3, 6, 10, 15, 20]
        crawler.initialQueueLength += cities.length * boxes.length;
        cities.forEach(function(city) {
          getApiToken(function(apiToken) {
            if (city.id !== 0) {
              var identity = {
                firstName: chance.first(),
                lastName: chance.last(),
                phone: chance.phone({ country: 'uk', mobile: true }),
                email: chance.email()
              };
              boxes.forEach(function(numberOfBoxes) {
                requestQueue.push({
                  crawlSessionId: crawlSessionId,
                  apiToken: apiToken,
                  identity: identity,
                  countryTo: country,
                  cityTo: city,
                  numberOfBoxes: numberOfBoxes
                }, function() {
                  logger.debug('[BHub] Finished parsing rate London, UK -> %s, %s', country.text, city.text);
                });
              });
            }
          });
        });
        callback();
      });
    });
  });
}

function requestRate(requestParams, callback) {
  rest.post('http://www.baggagehub.com/booking-app/ajax-get-a-quote', {
    timeout: 15000,
    query: {
      '_token': requestParams.apiToken,
      'base_country': '1',
      'email': requestParams.identity.email,
      'first_name': requestParams.identity.firstName,
      'last_name': requestParams.identity.lastName,
      'moving_type': 'mini_move',
      'o_boxes': '{"1":"{0}"}'.format(requestParams.numberOfBoxes),
      'o_boxes_approx_weight': '{"1":"25"}',
      'o_custom_box_dim': '{}',
      'o_fcity': 1596,
      'o_fcountry': 1,
      'o_fpostcode': 0,
      'o_next_custom_box': 1,
      'o_selected_sub_freight': '',
      'o_storage_fdate': '',
      'o_storage_tdate': '',
      'o_tcity': requestParams.cityTo.id,
      'o_tcountry': requestParams.countryTo.id,
      'o_total_custom_box': 50,
      'o_tpostcode': 0,
      'o_value_of_goods': 0,
      'quote_ec': 'no',
      'service_type': 'moving_only',
      'user_phone': requestParams.identity.phone
    },
    parser: rest.parsers.json
  }).on('complete', function(data, response) {
    /*
    console.log(data);
    if (data.hasOwnProperty('addons')) {
      console.log(data.addons);
    }
    */
    var rateCount = 0;

    if (data.hasOwnProperty('eu_move')) {
      rateCount++
      saveRate(requestParams, data['eu_move']);
    }
    if (data.hasOwnProperty('mini_move')) {
      rateCount++;
      saveRate(requestParams, data['mini_move']);
    }
    if (data.hasOwnProperty('ww_move')) {
      if (data['ww_move'].hasOwnProperty('air')) {
        rateCount++;
        saveRate(requestParams, data['ww_move']['air']);
      }
      if (data['ww_move'].hasOwnProperty('sea_d2d')) {
        rateCount++;
        saveRate(requestParams, data['ww_move']['sea_d2d']);
      }
    }
    if (rateCount === 0) {
      console.log(data);
    }
    callback();
  }).on('timeout', function() {
    logger.warn('[BHub] Timeout in requestRate');
    requestRate(requestParams, callback);
  })
}

function saveRate(requestParams, rate) {
  Rate.create({
    'session_id': requestParams.crawlSessionId,
    countryFrom: 'United Kingdom',
    cityFrom: 'London',
    countryTo: requestParams.countryTo.text,
    cityTo: requestParams.cityTo.text,
    numberOfBoxes: requestParams.numberOfBoxes,
    rateType: rate.description,
    rateTime: rate.transit,
    rate: parseFloat(rate['display_cost'])
  });
}

var crawler = {
  longName: 'Baggagehub.com freight rates crawler',
  shortName: 'bhub',
  status: 'off',
  initialQueueLength: 0,
  crawlingSession: {},

  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(requestParams, callback) {
    setTimeout(function() {
      requestRate(requestParams, callback);
    }, 5000)
  }, 1),

  startCrawl: function() {
    crawler.status = 'on';

    crawler.requestQueue.drain = function() {
      crawler.status = 'off';
      crawler.crawlingSession = {};
      logger.info('Baggageub crawling successful!');
    };

    logger.info('Started crawling Baggageub');
    //Create a new crawl session
    CrawlSession.create().then(function(session) {
      crawler.crawlingSession = session;
      populateRequestQueue(crawler.requestQueue, session.id);
    });
  },

  pauseCrawl: function() {
    logger.info('Paused crawling Baggageub');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling Baggageub');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling Baggageub');
    crawler.status = 'off';
    crawler.crawlSession = {};
    crawler.requestQueue.kill();
  }
}

module.exports = crawler;
