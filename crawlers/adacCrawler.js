var ADACModels = require('../models/adacModels');
var Modell = ADACModels.Modell;
var Allgemein = ADACModels.Allgemein;
var MotorUndAntrieb = ADACModels.MotorUndAntrieb;
var MasseUndGewichte = ADACModels.MasseUndGewichte;
var KarosserieUndFahrwerk = ADACModels.KarosserieUndFahrwerk;
var MesswerteHersteller = ADACModels.MesswerteHersteller;
var Sicherheitsausstattung = ADACModels.Sicherheitsausstattung;
var Herstellergarantien = ADACModels.Herstellergarantien;
var PreiseUndAusstattung = ADACModels.PreiseUndAusstattung;

var rest = require('restler');
var htmlparser = require('htmlparser2');
var async = require('async');
var he = require('he');
require('../helpers.js');

var logger = require('../logger.js');

var requestVariables = {
    '__EVENTTARGET': '',
    '__EVENTARGUMENT': '',
    '__LASTFOCUS': '',
    '__VIEWSTATE': '',
    '__VIEWSTATEGENERATOR': '',
    '__PREVIOUSPAGE': '',
};

var cookies = undefined;

var getRequestVariables = function(data) {
  var parser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'input' && attributes.type === 'hidden' && attributes.name.startsWith('__')) {
        requestVariables[attributes.name] = attributes.value;
      }
    }
  });

  parser.write(data);
  parser.end();
}

var getCookies = function(callback) {
  rest.get('https://www.adac.de/infotestrat/autodatenbank/default.aspx', {
    timeout: 10000
  }).on('complete', function(data, response) {
    getRequestVariables(data);
    cookies = response.headers['set-cookie'];
    callback();
  }).on('timeout', function() {
    logger.error('[ADAC] Timeout reached in getCookies.');
    getCookies(callback);
  });
}

var performSearchQuery = function(callback) {
  //TODO: POST request to the search form, returns the first page of the result
  rest.post('https://www.adac.de/infotestrat/autodatenbank/suchergebnis.aspx', {
    headers: {
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:43.0) Gecko/20100101 Firefox/43.0',
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'Accept-Encoding': 'gzip, deflate',
      'Cookie': cookies.join(';'),
    },
    data: {
      '__EVENTTARGET': requestVariables['__EVENTTARGET'],
      '__EVENTARGUMENT': requestVariables['__EVENTARGUMENT'],
      '__LASTFOCUS': requestVariables['__LASTFOCUS'],
      '__VIEWSTATE': requestVariables['__VIEWSTATE'],
      '__VIEWSTATEGENERATOR': requestVariables['__VIEWSTATEGENERATOR'],
      '__PREVIOUSPAGE': requestVariables['__PREVIOUSPAGE'],
      'ctl00$ctl00$ctl17$keywordsearch': 'Suche',
      'hiddenPdfField': '',
      'HiddenControlId': 'wucAutodatenbankSL1',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$HiddenOtherSearchCriteriaOpened': 'false',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$DropDownHersteller': '',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$DropDownModell': '',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$ValidatedSearch.1': 'Marke/Modell',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$ImageButtonSuche1.x': '37',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$ImageButtonSuche1.y': '8',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$rb123': 'RadiobuttonAvailableModels',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$RadioButtonFahrzeugklasse': 'RadioButtonFahrzeugklasse_0',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$DropDownListAufbau': '',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$DropDownTueren': '',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$DropDownGetriebe': '',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$DropDownMotorart': '',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$DropDownSchadstoffklasse': '',
      'cdbSlider1Option': 'on',
      'cdbSlider1Value': '1,6',
      'cdbSlider2Option': 'on',
      'cdbSlider2Value': '1,6',
      'cdbSlider3Option': 'on',
      'cdbSlider3Value': '1',
      'cdbSlider4Option': 'on',
      'cdbSlider4Value': '1',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$TextBoxHSN': '',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucAutodatenbankSL1$TextBoxTSN': '',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucSocialMediaLinks1$hidSocialMediaUrl': 'https://www.adac.de/infotestrat/autodatenbank/default.aspx',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucSocialMediaLinks1$hidSocialMediaTitle': '',
      'ctl00$ctl00$cphContentRow$cphROW2spaltigerContent$cphMittelContent$wucSocialMediaLinks1$hidWhatsApp': 'ADAC WhatsApp Share-Button: https://www.adac.de/infotestrat/autodatenbank/default.aspx',
      'ctl00$ctl00$onlineRegStandard$PanelLogin$ValidatedEmailLogin.1': '',
      'ctl00$ctl00$onlineRegStandard$PanelLogin$ValidatedPasswordLogin.1': '',
      'ctl00$ctl00$onlineRegStandard$PanelLogin$HiddenDeviceMob': '',
    }
  }).on('complete', function(data) {
    getRequestVariables(data);
    callback(data);
  });
}

var requestNextPage = function(callback) {
  rest.post('https://www.adac.de/infotestrat/autodatenbank/suchergebnis.aspx', {
    headers: {
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:43.0) Gecko/20100101 Firefox/43.0',
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'Accept-Encoding': 'gzip, deflate',
      'Cookie': cookies.join(';'),
    },
    data: {
      'ctl00$ctl00$cphContentRow$ScriptManager1': 'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$updatePanel1|ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$ResultListPagerFahrzeugDaten$lnkPageNext',
      '__EVENTTARGET': 'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$ResultListPagerFahrzeugDaten$lnkPageNext',
      '__EVENTARGUMENT': requestVariables['__EVENTARGUMENT'],
      '__LASTFOCUS': requestVariables['__LASTFOCUS'],
      '__VIEWSTATE': requestVariables['__VIEWSTATE'],
      '__VIEWSTATEGENERATOR': requestVariables['__VIEWSTATEGENERATOR'],
      'ctl00$ctl00$ctl17$keywordsearch': 'Suche',
      'hiddenPdfField': '',
      'HiddenControlId': 'wucAutodatenbankSL1',
      'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$HiddenOtherSearchCriteriaOpened': 'false',
      'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$DropDownHersteller': '',
      'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$DropDownModell': '',
      'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$ValidatedSearch.1': 'Marke/Modell',
      'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$rb123': 'RadiobuttonAvailableModels',
      'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$RadioButtonFahrzeugklasse': 'RadioButtonFahrzeugklasse_0',
      'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$DropDownListAufbau': '',
      'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$DropDownTueren': '',
      'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$DropDownGetriebe': '',
      'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$DropDownMotorart': '',
      'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$DropDownSchadstoffklasse': '',
      'cdbSlider1Option': 'on',
      'cdbSlider1Value': '1,6',
      'cdbSlider2Option': 'on',
      'cdbSlider2Value': '1,6',
      'cdbSlider3Option': 'on',
      'cdbSlider3Value': '1',
      'cdbSlider4Option': 'on',
      'cdbSlider4Value': '1',
      'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$TextBoxHSN': '',
      'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$TextBoxTSN': '',
      'ctl00$ctl00$cphContentRow$cphROWBreiterTopContent$cphBreiterTopContent$wucAutodatenbankSL1$ddlPageSize': '10',
      'ctl00$ctl00$onlineRegStandard$PanelLogin$ValidatedEmailLogin.1': '',
      'ctl00$ctl00$onlineRegStandard$PanelLogin$ValidatedPasswordLogin.1': '',
      'ctl00$ctl00$onlineRegStandard$PanelLogin$HiddenDeviceMob': '',
    }
  }).on('complete', function(data) {
    getRequestVariables(data);
    callback(data);
  });
}

var parseSearchResultsPage = function(requestQueue, data) {
  var parser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'a' && attributes.class === 'box-link') {
        requestQueue.push('https://www.adac.de/' + attributes.href, function(carName) {
          if (carName) {
             logger.debug('Finished parsing ' + carName);
          } else {
            logger.error('Cannot find a car at url %s', attributes.href);
          }
        });
      }
    }
  });

  parser.write(data);
  parser.end();
}

var parseNumberOfPages = function(data) {
  var numberOfPages = 0;

  var parser = new htmlparser.Parser({
    insideLink: false,

    onopentag: function(name, attributes) {
      if (name === 'a' && attributes.id === 'ctl00_ctl00_cphContentRow_cphROWBreiterTopContent_cphBreiterTopContent_wucAutodatenbankSL1_ResultListPagerFahrzeugDaten_lnkPageLast') {
        this.insideLink = true;
      }
    },

    ontext: function(text) {
      if (this.insideLink) {
        numberOfPages = parseInt(text);
        this.insideLink = false;
      }
    }
  });

  parser.write(data);
  parser.end();
  logger.debug('Total number of pages - %s', numberOfPages);
  return numberOfPages;
}

var populateRequestQueue = function(requestQueue) {
  getCookies(function() {
    performSearchQuery(function(data) {
      parseSearchResultsPage(requestQueue, data);
      var numberOfPages = parseNumberOfPages(data);
      var currentPage = 2;
      async.whilst(
        function() {return currentPage <= numberOfPages},
        function(callback) {
          requestNextPage(function(data) {
            parseSearchResultsPage(requestQueue, data);
            logger.debug('Finished parsing page %s', currentPage);
            currentPage++;
            crawler.initialQueueLength = 10 * currentPage;
            callback();
          });
        },
        function() {
          logger.info('Finished populating the queue');
        }
      );
    });
  });
}

var requestCarPage = function(url, callback) {
  rest.get(url, {
    timeout: 10000
  }).on('complete', function(data) {
    var carData = parseCarPage(data);
    if (carData.modell.name) {
      saveCar(carData, function() {
        callback(carData.modell.name);
      });
    } else {
      callback(undefined);
    }

  }).on('timeout', function() {
    logger.error('[ADAC] Timeout reached at requestCarPage!');
    requestCarPage(url, callback);
  });
}

var parseCarPage = function(data) {
  //Parsing car page
  var carObject = {
    modell: {},
    allgemein: {},
    motorUndAntrieb: {},
    masseUndGewichte: {},
    karosserieUndFahrwerk: {},
    messwerteHersteller: {},
    sicherheitsausstattung: {},
    herstellergarantien: {},
    preiseUndAusstattung: {}
  }

  var headerParser = new htmlparser.Parser({
    isName: false,
    insideDetails: false,
    insideLeft: false,

    onopentag: function(name, attributes) {
      if (name === 'h1' && attributes.class === 'pb11') {
        headerParser.isName = true;
      }
      if (name === 'div' && attributes.id === 'wucAutodatenbankDetail-5') {
        headerParser.insideDetails = true;
      }
      if (headerParser.insideDetails && name === 'div' && attributes.class === 'left') {
        headerParser.insideLeft = true;
      }

    },

    onclosetag: function(name) {
      if (name === 'div') {
        if (headerParser.insideLeft) {
          headerParser.insideLeft = false;
        } else if (headerParser.insideDetails) {
          headerParser.insideDetails = false;
        }
      }
      if (name === 'h1') {
        headerParser.isName = false;
      }
    },

    ontext: function(text) {
      text = text.replace(/[\n\r]/g, '').trim();
      if (headerParser.isName) {
        carObject.modell.name = text;
      } else if (headerParser.insideLeft) {
        if (text.startsWith('Neuwagenpreis:')) {
          //Crop and add
          carObject.modell.neuwagenpreis = text.replace(/\D/g, '');
        } else if (text.startsWith('HSN/TSN:')) {
          //Crop and add
          carObject.modell.HSNTSN = text.substring(9);
        } else if (text.length > 0) {
          //It's a type
          carObject.modell.typ = text;
        }
      }
    }
  });

  var parametersParser = new htmlparser.Parser({
    currentSection: 0,
    sectionMapping: {
      1: 'allgemein',
      2: 'motorUndAntrieb',
      3: 'masseUndGewichte',
      4: 'karosserieUndFahrwerk',
      5: 'messwerteHersteller',
      6: 'sicherheitsausstattung',
      7: 'herstellergarantien',
      8: 'preiseUndAusstattung'
    },
    insideSection: false,
    insideList: false,
    insideRow: false,
    insideColumn: false,
    currentColumn: 0,
    parameter: {
      name: '',
      value: ''
    },

    onopentag: function(name, attributes) {
      if (name === 'li' && attributes.hasOwnProperty('id') && attributes.id.match(/^listitem-\d+?-2345$/g)) {
        this.insideSection = true;
        this.currentSection = attributes.id.split('-')[1];
      }
      if (this.insideSection && name === 'div' && attributes.class === 'module-adress-divtable-01') {
        this.insideList = true;
      }
      if (this.insideList && name === 'div' && attributes.class.hasSubstring('box-row')) {
        this.insideRow = true
      }
      if (this.insideRow && name === 'div') {
        this.insideColumn = true;
        if (attributes.class === 'box-col1') {
          this.currentColumn = 1
        } else if (attributes.class === 'box-col2') {
          this.currentColumn = 2
        }
      }
    },

    onclosetag: function(name) {
      if (name === 'div') {
        if (this.insideColumn) {
          this.insideColumn = false;
        } else if (this.insideRow) {
          this.insideRow = false;
          this.currentColumn = 0;
          carObject[this.sectionMapping[this.currentSection]][this.parameter.name] = this.parameter.value;
          this.parameter = {
            name: '',
            value: ''
          };
        } else if (this.insideList) {
          this.insideList = false;
        }
      }

      if (this.insideSection && name === 'li') {
        this.insideSection = false;
        this.currentSection = 0;
      }
    },

    ontext: function(text) {
      if (this.insideColumn && this.currentSection > 0 && this.currentColumn > 0) {
        if (this.currentColumn === 1) {
          this.parameter.name = he.decode(text).replaceUmlaute().toCamelCase();
        } else if (this.currentColumn === 2) {
          this.parameter.value = he.decode(text);
        }
      }
    }
  });

  headerParser.write(data);
  headerParser.end();
  parametersParser.write(data);
  parametersParser.end();
  return carObject;
}

saveCar = function(car, globalCallback) {
  //Find model by name. If not found - create, if found - update
  var carArray = [car.allgemein, car.motorUndAntrieb,
    car.masseUndGewichte, car.karosserieUndFahrwerk,
    car.messwerteHersteller, car.sicherheitsausstattung,
    car.herstellergarantien, car.preiseUndAusstattung]
  Modell.upsert(car.modell).then(function() {
    Modell.findOne({where: {name: car.modell.name}}).then(function(modell) {
      var ModellId = modell.id;
      carArray.forEach(function(element) {
        element.ModellId = modell.id;
      });
      async.series([
        function(callback) {
          Allgemein.upsert(carArray[0]).then(function() {
            callback(null);
          });
        },
        function(callback) {
          MotorUndAntrieb.upsert(carArray[1]).then(function() {
            callback(null);
          });
        },
        function(callback) {
          MasseUndGewichte.upsert(carArray[2]).then(function() {
            callback(null);
          });
        },
        function(callback) {
          KarosserieUndFahrwerk.upsert(carArray[3]).then(function() {
            callback(null);
          });
        },
        function(callback) {
          MesswerteHersteller.upsert(carArray[4]).then(function() {
            callback(null);
          });
        },
        function(callback) {
          Sicherheitsausstattung.upsert(carArray[5]).then(function() {
            callback(null);
          });
        },
        function(callback) {
          Herstellergarantien.upsert(carArray[6]).then(function() {
            callback(null);
          });
        },
        function(callback) {
          PreiseUndAusstattung.upsert(carArray[7]).then(function() {
            callback(null);
          });
        }
      ], function(err) {
        globalCallback();
      });
    });
  });
}

var crawler = {
  longName: 'ADAC car information crawler',
  shortName: 'adac',
  status: 'off',
  initialQueueLength: 0,

  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(url, callback) {
    requestCarPage(url, callback);
  }, 1),

  startCrawl: function() {
    crawler.status = 'on';

    crawler.requestQueue.drain = function() {
      crawler.status = 'off';
      logger.info('ADAC crawling successful!');
    };

    logger.info('Started crawling ADAC');
    populateRequestQueue(crawler.requestQueue);
  },

  pauseCrawl: function() {
    logger.info('Paused crawling ADAC');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling ADAC');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling ADAC');
    crawler.status = 'off';
    crawler.crawlSession = {};
    crawler.requestQueue.kill();
  }
}

module.exports = crawler;
