var ZooplaModels = require('../models/zooplaModels');
var PostcodeCrawler = require('../crawlers/postcodeCrawler');
var PostcodeModels = require('../models/postcodeModels');
var DistrictPostcode = PostcodeModels.uk.DistrictPostcode;
var AreaStats = ZooplaModels.AreaStats;
var AreaSampleSize = ZooplaModels.AreaSampleSize;
var CrawlSession = ZooplaModels.CrawlSession;

var rest = require('restler');
var htmlparser = require('htmlparser2');
var async = require('async');
require('../helpers.js');

var logger = require('../logger.js');

var checkPostcodes = function(callback) {
  //Count district postcodes. If the number is > 1000 -> no futher crawling needed.
  PostcodeModels.sequelize.query('SELECT count(DISTINCT(postcode)) from uk_district_postcodes').then(function(response) {
    var count = response[0][0].count;
    logger.debug('Distinct district postcodes: %s', count);
    callback(count > 1000 ? false : true);
  });
}

var populateRequestQueue = function(requestQueue, crawlSessionId) {
  PostcodeModels.sequelize.query('SELECT DISTINCT postcode FROM uk_district_postcodes', {model: DistrictPostcode, raw: true}).then(function(postcodes) {
    logger.info('Total district posctode count: %s', postcodes.length);
    crawler.initialQueueLength = postcodes.length;
    postcodes.forEach(function(postcode) {
      requestQueue.push({
        requestQueue: requestQueue,
        crawlSessionId: crawlSessionId,
        postcode: postcode.postcode
      }, function() {
        logger.debug('Request complete for postcode %s.', postcode);
      });
    });
  });
}

var requestAreaStats = function(requestParams, requestCallback) {
  var postcode = requestParams.postcode;
  var crawlSessionId = requestParams.crawlSessionId;
  var requestQueue = requestParams.requestQueue;
  rest.get('http://www.zoopla.co.uk/market/{0}/'.format(postcode), {
    timeout: 10000,
  }).on('complete', function(data) {
    async.series([
      function(callback) {
        parseAreaStats(crawlSessionId, postcode, data, callback);
      },
      function(callback) {
        parseSampleSize(crawlSessionId, postcode, data, function(sampleSize) {
          AreaSampleSize.create({
            'session_id': crawlSessionId,
            postcode: postcode,
            flats       : sampleSize.flats,
            semiDetached: sampleSize.semiDetached,
            detached    : sampleSize.detached,
            terraced    : sampleSize.terraced,
            unknown     : sampleSize.unknown
          });
          callback();
        });
      }
    ], function() {
      requestCallback();
    });
  }).on('timeout', function() {
    logger.warn('Request timeout!');
    requestQueue.push(requestParams, function() {
      logger.debug('Request complete for postcode %s.', postcode);
    });
  });
}

var parseSampleSize = function(crawlSessionId, postcode, data, requestCallback) {
  //1) Parse flat sample size from the existing data +
  //2) Initiate new get request to /for-sale/houses/<postcode> +
  //3) Gather the number of pages from the new request data
  //4) If the number of pages > 1, create get request for every page
  var sampleSize = {
    flats: parseFlatSampleSize(data),
    terraced: 0,
    semiDetached: 0,
    detached: 0,
    unknown: 0
  };

  rest.get('http://www.zoopla.co.uk/for-sale/houses/{0}/?new_homes=include&page_size=100'.format(postcode), {
    timeout: 10000
  }).on('complete', function(data) {
    //Check the number of pages
    parseHouseSampleSize(data, sampleSize);
    var numberOfPages = checkNumOfPages(data);
    if (numberOfPages > 1) {
      var currentPage = 2;
      async.whilst(
        function() {return currentPage <= numberOfPages},
        function(callback) {
          parseSampleSizePage(postcode, currentPage, sampleSize, callback);
          currentPage++;
        },
        function() {
          requestCallback(sampleSize);
        }
      );
    } else {
      requestCallback(sampleSize);
    }
  });
}

var parseSampleSizePage = function(postcode, page, sampleSize, callback) {
  rest.get('http://www.zoopla.co.uk/for-sale/houses/{0}/?new_homes=include&page_size=100&np={1}'.format(postcode, page), {
    timeout: 10000
  }).on('complete', function(data) {
    parseHouseSampleSize(data, sampleSize);
    callback();
  });
}

var checkNumOfPages = function(data) {
  var insidePages = false
  var pageCount = 0

  var pageNumberParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'span' && attributes.class === 'listing-results-utils-count') {
        insidePages = true;
      }
    },

    onclosetag: function(name) {
      if (insidePages && name === 'span') {
        insidePages = false;
      }
    },

    ontext: function(text) {
      if (insidePages) {
        var splittedText = text.split(' ');
        // [1, -, 15, of, 15]. We need only the [2] and [4]
        if (splittedText[2] === splittedText[4]) {
          pageCount = 1;
        } else {
          var pageSize = splittedText[2];
          var totalSize = splittedText[4];
          if (totalSize % pageSize === 0) {
            pageCount = totalSize / pageSize;
          } else {
            pageCount = Math.floor(totalSize / pageSize) + 1;
          }
        }
      }
    }
  });

  pageNumberParser.write(data);
  pageNumberParser.end();
  return pageCount;
}

var parseFlatSampleSize = function(data) {
  var insideLink = false
  var sampleSize = 0

  var flatSampleSizeParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'a' && attributes.hasOwnProperty('href') && attributes.href.startsWith('/for-sale/flats') && (attributes.href.hasSubstring('bedrooms') || attributes.href.hasSubstring('bedroom'))) {
        insideLink = true;
      }
    },

    onclosetag: function(name) {
      if (name === 'a' && insideLink) {
        insideLink = false;
      }
    },

    ontext: function(text) {
      if (insideLink) {
        sampleSize += parseInt(text);
      }
    }
  });

  flatSampleSizeParser.write(data);
  flatSampleSizeParser.end();
  return sampleSize;
}

var parseHouseSampleSize = function(data, sampleSize) {
  var insideLink = false;

  var houseSampleSizeParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'a' && attributes.hasOwnProperty('itemprop') && attributes.itemprop === 'name') {
        insideLink = true;
      }
    },

    onclosetag: function(name) {
      if (insideLink && name === 'a') {
        insideLink = false;
      }
    },

    ontext: function(text) {
      if (insideLink) {
        text = text.toLowerCase();
        //Look for text occurences here
        if (text.hasSubstring('terraced') || text.hasSubstring('end terrace') || text.hasSubstring('mews')) {
          sampleSize.terraced++;
        } else if (text.hasSubstring('semi-detached') || text.hasSubstring('town house')) {
          sampleSize.semiDetached++;
        } else if (text.hasSubstring('detached') || text.hasSubstring('cottage') || text.hasSubstring('barn') || text.hasSubstring('lodge') || text.hasSubstring('') || text.hasSubstring('country house') || text.hasSubstring('farmhouse') || text.hasSubstring('mobile/park') || text.hasSubstring('bungalow')) {
          sampleSize.detached++;
        } else if (text.hasSubstring('property') || text.hasSubstring('garage') || text.hasSubstring('block of flats') || text.hasSubstring('houseboat')) {
          sampleSize.unknown++;
        } else {
          sampleSize.unknown++;
          logger.warn('New property type found: %s', text);
        }
      }
    }
  });

  houseSampleSizeParser.write(data);
  houseSampleSizeParser.end();
}

var parseAreaStats = function(crawlSessionId, postcode, data, callback) {
  var insideAreaStatsTable = false;
  var insideHead = false;
  var textNode = false;
  var columnNumber = 0;
  var areaStats = ['', '', '', '', ''];

  var areaStatsParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'table' && attributes.class === 'stripe property-type-table') {
        insideAreaStatsTable = true;
      }

      if (insideAreaStatsTable && name === 'thead') {
        insideHead = true;
      }

      if (insideAreaStatsTable && !insideHead) {
        if (name === 'tr') {
          columnNumber = 0;
        }
        if (name === 'td') {
          textNode = true;
        }
      }
    },

    onclosetag: function(name) {
      if (insideAreaStatsTable) {
        if (name === 'table') {
          insideAreaStatsTable = false;
        }
        if (name === 'thead') {
          insideHead = false;
        }
        if (!insideHead) {
          if (name === 'tr') {
            //Save the area stats to the database
            if (areaStats[1]) {
              AreaStats.create({
                'session_id': crawlSessionId,
                postcode: postcode,
                type: areaStats[0],
                totalPrice: areaStats[1],
                priceSqFeet: areaStats[2] ? areaStats[2] : undefined,
                area: areaStats[2] ? (areaStats[1]/areaStats[2]).round(2) : undefined,
                bedrooms: areaStats[3] ? areaStats[3] : undefined,
              });
            }
            areaStats = ['', '', '', '', ''];
          }
          if (name === 'td') {
            textNode = false;
            columnNumber++;
          }
        }
      }
    },

    ontext: function(text) {
      if (textNode) {
        var textCandidate = text.replace('&pound;', '').replace(/,/g, '');
        if (textCandidate === '-') {
          areaStats[columnNumber] = undefined
        } else {
          areaStats[columnNumber] = textCandidate;
        }
      }
    }
  });

  areaStatsParser.write(data);
  areaStatsParser.end();

  callback();
}

var crawler = {
  longName: 'Zoopla area stats crawler',
  shortName: 'zoopla',
  status: 'off',
  initialQueueLength: 0,
  crawlingSession: {},
  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(requestParams, callback) {
    requestAreaStats(requestParams, callback);
  }, 2),

  startCrawl: function() {
    crawler.status = 'on';
    crawler.requestQueue.drain = function() {
      crawler.status = 'off';
      crawler.crawlingSession = {};
      logger.info('Zoopla crawling successful!');
    };

    logger.info('Started crawling Zoopla');
    //Create a new crawl session
    CrawlSession.create().then(function(session) {
      crawler.crawlingSession = session;
      checkPostcodes(function(noPostcodes) {
        if (noPostcodes) {
          logger.warn('Postcodes not found! Starting PostcodeCrawler');
          PostcodeCrawler.startCrawl(function() {
            populateRequestQueue(crawler.requestQueue, session.id);
          });
        } else {
          populateRequestQueue(crawler.requestQueue, session.id);
        }
      });
    });
  },

  pauseCrawl: function() {
    logger.info('Paused crawling Zoopla');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling Zoopla');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling Zoopla');
    crawler.status = 'off';
    crawler.crawlSession = {};
    crawler.requestQueue.kill();
  }
};

module.exports = crawler;
