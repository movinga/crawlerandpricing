var ImmobiliareModels = require('../models/immobiliareModels');
var Property = ImmobiliareModels.Property;
var Commune = ImmobiliareModels.Commune;
var Zone = ImmobiliareModels.Zone;
var CrawlSession = ImmobiliareModels.CrawlSession;

var rest = require('restler');
var htmlparser = require('htmlparser2');
var async = require('async');
require('../helpers.js');
var logger = require('../logger.js');

var parseGeoData = function(callback) {
  requestCookies(function(cookies) {
    requestCommunes(cookies, function() {
      requestZones(cookies, function() {
        callback();
      });
    });
  });
}

var requestCookies = function(callback) {
  rest.get('http://www.immobiliare.it/', {
    timeout: 10000
  }).on('complete', function(data, response) {
    callback(response.headers['set-cookie'][0]);
  }).on('timeout', function() {
    logger.warn('[Immobiliare] Timeout in requestCookies!');
    requestCookies(callback);
  });
}

var requestCommunes = function(cookies, callback) {
  var searchRequestArray = [];
  //Compose searchRequestArray
  var characters = 'abcdefghijklmnopqrstuvwxyz';
  for (var i = 0; i < characters.length; i++) {
    for (var j = 0; j < characters.length; j++) {
      if (i !== j) {
        searchRequestArray.push(characters[i] + characters[j]);
      }
    }
  }

  async.eachSeries(searchRequestArray, function(searchRequest, requestCallback) {
    sendCommunesRequest(cookies, searchRequest, requestCallback);
  }, function() {
    logger.info('[Immobiliare] Finished parsing communes.');
    callback();
  });
}

var sendCommunesRequest = function(cookies, searchRequest, requestCallback) {
  rest.post('http://www.immobiliare.it/comune_suggestion.php', {
    timeout: 10000,
    query: {
      s: searchRequest
    },
    data: {
      excludeVirtual: 0,
      idNaz: 'IT',
      maxRes: 50,
      order: 'best-match',
      showNation: 0,
      showProvince: 0,
      version: 2
    },
    headers: {
      Cookie: cookies,
      Accept: 'application/json',
      Host: 'www.immobiliare.it',
      Referer: 'http://www.immobiliare.it'
    }
  }).on('complete', function(data) {
    if (data.errors && data.errors.length > 0) {
      logger.warn(data.errors);
    } else {
      data.results.forEach(function(result) {
        if (result.type === 'comune') {
          Commune.upsert({
            id: result.idComune,
            name: result['comune_nome'],
            province: result['provincia_nome'],
            withZones: result.hasZone
          });
        }
      });
    }
    requestCallback();
  }).on('timeout', function() {
    logger.warn('[Immobiliare] Timeout in requestCommunes!');
    sendCommunesRequest(cookies, searchRequest, requestCallback);
  });
}

var requestZones = function(cookies, callback) {
  var communesWithZones = Commune.findAll({
    where: {withZones: true}
  }).then(function(communes) {
    async.eachSeries(communes, function(commune, requestCallback) {
      sendZonesRequest(cookies, commune, requestCallback);
    }, function() {
      logger.info('[Immobiliare] Finished parsing zones!');
      callback();
    });
  });
}

var sendZonesRequest = function(cookies, commune, requestCallback) {
  rest.get('http://www.immobiliare.it/services/geography/getGeography.php', {
    timeout: 10000,
    query: {
      action: 'getMacrozoneComune',
      idComune: commune.id
    },
    headers: {
      Cookie: cookies,
      Accept: 'application/json',
      Host: 'www.immobiliare.it',
      Referer: 'http://www.immobiliare.it'
    }
  }).on('complete', function(data) {
    for (var key in data.result) {
      var zone = data.result[key];
      Zone.upsert({
        'commune_id': commune.id,
        id: zone['macrozona_idMacrozona'],
        name: zone['macrozona_nome_sn']
      });
    }
    requestCallback();
  }).on('timeout', function() {
    logger.warn('[Immobiliare] Timeout at sendZonesRequest!');
    sendZonesRequest(cookies, commune, requestCallback);
  });
}

var populateRequestQueue = function(requestQueue, crawlSessionId) {
  Commune.findAll().then(function(communes) {
    logger.debug(communes.length);
    crawler.initialQueueLength = communes.length;
    communes.forEach(function(commune) {
      if (commune.withZones) {
        commune.getZones({raw: true}).then(function(zones) {
          crawler.initialQueueLength += zones.length - 1;
          zones.forEach(function(zone) {
            requestQueue.push({
              requestQueue: requestQueue,
              crawlSessionId: crawlSessionId,
              commune: commune,
              zone: zone
            }, function() {
              logger.debug('[Immobiliare] Completed %s, %s (%s)', commune.name, commune.province, zone.name);
            });
          });
        });
      } else {
        requestQueue.push({
          requestQueue: requestQueue,
          crawlSessionId: crawlSessionId,
          commune: commune,
          zone: undefined
        }, function() {
          logger.debug('[Immobiliare] Completed %s, %s', commune.name, commune.province);
        });
      }
    });
  });
}

var saveProperty = function(property) {
  if (property.type && property.area) {
    Property.create(property);
  }
}

var requestProperties = function(requestParams, callback) {
  var requestQueue = requestParams.requestQueue;
  var crawlSessionId = requestParams.crawlSessionId;
  var commune = requestParams.commune
  var zone = requestParams.zone;

  var communeName = commune.name.replace(/'| |-/g, '_');
  var provinceName = commune.province.replace(/'| |-/g, '_');;

  var provinceExceptionList = {
    'Bolzano-Bolzen': 'Bolzano',
    'Valle D\'Aostra': 'Aostra',
    'Verbano-Cusio-Ossola': 'Verbania',
    'Reggio Nell\'Emilia': 'Reggio_Emilia',
    'Reggio Di Calabria': 'Reggio_Calabria',
    'L\'Aquila': 'Aquila',
    'Monza e Brianza': 'Monza_Brianza',
  }

  var communeExceptionList = {
    'Trento' : {
      'Livo': 'Livo_TN',
      'Brione': 'Brione_TN'
    },
    'Como' : {
      'Livo': 'Livo_CO',
      'Peglio': 'Peglio_CO'
    },
    'Brescia': {
      'Brione': 'Brione_BS'
    },
    'Pesaro Urbino': {
      'Peglio': 'Peglio_PU',
    },
    'Firenze': {
      'Figline e Incisa Valdarno': 'Figline_Incisa_Valdarno'
    }
  }

  if (provinceExceptionList.hasOwnProperty(commune.province)) {
    provinceName = provinceExceptionList[commune.province];
  }

  if (communeExceptionList.hasOwnProperty(commune.province)) {
    if (communeExceptionList[commune.province].hasOwnProperty(commune.name)) {
      communeName = communeExceptionList[commune.province][commune.name];
    }
  }

  if (zone) {
    var url = 'http://www.immobiliare.it/{0}/vendita_case-{1}.html?criterio=rilevanza&idMZona[]={2}'.format(provinceName, communeName, zone.id);
  } else {
    var url = 'http://www.immobiliare.it/{0}/vendita_case-{1}.html?criterio=rilevanza'.format(provinceName, communeName);
  }
  rest.get(url, {
    timeout: 10000
  }).on('complete', function(data) {
    //Parse 1st page
    parseProperties(data, requestParams)
    //Calculate number of pages
    var numberOfPages = parseNumberOfPages(data)
    //Parse each remaining page
    if (numberOfPages > 1) {
      var currentPage = 2;
      async.whilst(
        function() {return currentPage <= numberOfPages},
        function(callback) {
          requestNextPage(url, currentPage, function() {
            parseProperties(data, requestParams);
            currentPage++;
            callback();
          });
        },
        function() {
          callback();
        }
      )
    } else {
      callback();
    }
  }).on('timeout', function() {
    logger.warn('[Immobiliare] Timeout in requestProperties!');
    requestProperties(requestParams, callback);
  });
}

var requestNextPage = function(url, page, callback) {
  url = url + '&pag={0}'.format(page);
  rest.get(url, {
    timeout: 10000
  }).on('complete', function(data) {
    callback(data);
  }).on('timeout', function() {
    logger.warn('[Immobiliare] Timeout in requestNextPage!')
    requestNextPage(url, page, callback);
  });
}

var parseProperties = function(data, requestParams) {
  var insideTitle = false;
  var insideBottom = false;
  var insideAlignLeft = false;
  var insideContent = false;
  var passedBottom = false;

  var propertyDetails = {
    type: undefined,
    price: undefined,
    area: undefined,
    bedrooms: undefined,
  }

  var parser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if ((name === 'div') && attributes.class === 'content') {
        insideContent = true;
      }

      if (insideContent) {
        if (name === 'div' && attributes.class === 'titolo_annuncio') {
          insideTitle = true;
        }

        if (name === 'div' && attributes.class === 'bottom') {
          insideBottom = true;
        }

        if (name === 'div' && attributes.class === 'align_left' && insideBottom) {
          insideAlignLeft = true;
        }
      }
    },

    onclosetag: function(name) {
      if (name === 'div') {
        if (insideAlignLeft) {
          insideAlignLeft = false;
        } else if (insideTitle) {
          insideTitle = false;
        } else if (insideBottom) {
          insideBottom = false;
          passedBottom = true;
        } else if (insideContent && passedBottom) {
          passedBottom = false;
          insideContent = false;
          //Validate parsed data here
          saveProperty({
            'session_id': requestParams.crawlSessionId,
            zone: requestParams.zone ? requestParams.zone.name : undefined,
            commune: requestParams.commune.name,
            province: requestParams.commune.province,
            type: propertyDetails.type,
            price: propertyDetails.price,
            area: propertyDetails.area,
            bedrooms: propertyDetails.bedrooms
          });
          propertyDetails = {
            type: undefined,
            price: undefined,
            area: undefined,
            bedrooms: undefined,
          }
        }
      }
    },

    ontext: function(text) {
      text = text.toLowerCase();
      if (insideTitle) {
        if (text.hasSubstring('appartament') || text.hasSubstring('attico') || text.hasSubstring('mansarda') || text.hasSubstring('loft') || text.hasSubstring('open space')) {
          propertyDetails.type = 'Appartamento';
        } else if (text.hasSubstring('villa') || text.hasSubstring('casa indipendente') || text.hasSubstring('rustico') || text.hasSubstring('casale')) {
          propertyDetails.type = 'Villa';
        } else if (text.hasSubstring('villetta') || text.hasSubstring('palazzo') || text.hasSubstring('stabile')) {
          propertyDetails.type = 'Villetta';
        }
      } else if (insideAlignLeft && propertyDetails.type) {
        text = text.replace(/&nbsp;|\t/g, '').trim();
        if (text.hasSubstring('&euro;')) {
          propertyDetails.price = parseInt(text.split(' ')[1].replace(/\.|,/g, ''));
        } else if (text.hasSubstring('m&sup2;')) {
          propertyDetails.area = parseInt(text.replace(/m&sup2;/g, ''));
        } else if (text.hasSubstring('locali')) {
          if (text.hasSubstring('più di')) {
            propertyDetails.bedrooms = 6;
          } else {
            propertyDetails.bedrooms = parseInt(text[0]);
          }
        }
      }
    }
  });

  parser.write(data);
  parser.end();
}

var parseNumberOfPages = function(data) {
  var insidePageCount = false;
  var insideStrong = false;
  var totalPages = 1;

  var parser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'div' && attributes.id === 'pageCount') {
        insidePageCount = true;
      }

      if (insidePageCount && name === 'strong') {
        insideStrong = true;
      }
    },

    onclosetag: function(name) {
      if (insidePageCount) {
        if (name === 'strong') {
          insideStrong = false;
        }
        if (name === 'div') {
          insidePageCount = false;
        }
      }
    },

    ontext: function(text) {
      if (insideStrong) {
        if (text !== '1') {
          totalPages = text;
        }
      }
    }
  });

  parser.write(data);
  parser.end();
  return totalPages;
}

var crawler = {
  longName: 'Immobiliare property details crawler',
  shortName: 'immobiliare',
  status: 'off',
  initialQueueLength: 0,
  crawlingSession: {},

  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(requestParams, callback) {
    requestProperties(requestParams, callback);
  }, 2),

  startCrawl: function() {
    crawler.status = 'on';

    crawler.requestQueue.drain = function() {
      crawler.status = 'off';
      crawler.crawlingSession = {};
      logger.info('Immobiliare crawling successful!');
    };

    logger.info('Started crawling Immobiliare');
    //Create a new crawl session
    CrawlSession.create().then(function(session) {
      crawler.crawlingSession = session;
      parseGeoData(function() {
        populateRequestQueue(crawler.requestQueue, session.id);
      });
    });
  },

  pauseCrawl: function() {
    logger.info('Paused crawling Immobiliare');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling Immobiliare');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling Immobiliare');
    crawler.status = 'off';
    crawler.crawlSession = {};
    crawler.requestQueue.kill();
  }
}

module.exports = crawler;
