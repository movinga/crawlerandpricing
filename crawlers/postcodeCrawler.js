var PostcodeModels = require('../models/postcodeModels');
var UKDistrictPostcode = PostcodeModels.uk.DistrictPostcode;
var UKFullPostcode = PostcodeModels.uk.FullPostcode;
var FRPostcode = PostcodeModels.FRPostcode;
var ITPostcode = PostcodeModels.ITPostcode;

var request = require('request');
var fs = require('fs');
var AdmZip = require('adm-zip');
var async = require('async');
require('../helpers.js');

var logger = require('../logger.js');

var countryParams = {
  uk: {
    uri: 'http://download.geonames.org/export/zip/GB_full.csv.zip',
    zipFilename: __dirname + '/UK_postcodes.zip',
    filename: __dirname + '/GB_full.csv',
    parseFunction: function(postcode, callback) {
      var dataPoint = postcode.split('\t');
      if (dataPoint[1]) {
        if (dataPoint[1].hasSubstring(' ')) {
          //It's a full postcode
          UKFullPostcode.upsert({
            postcode: dataPoint[1],
            address1: dataPoint[2],
            address2: dataPoint[5] === '' ? undefined : dataPoint[5],
            address3: dataPoint[7] === '' ? undefined : dataPoint[7],
            address4: dataPoint[3] === '' ? undefined : dataPoint[3],
            latitude: dataPoint[9] === '' ? undefined : dataPoint[9],
            longitude: dataPoint[10] === '' ? undefined : dataPoint[10]
          }).then(function() {
            callback();
          });
        } else {
          //It's a district postcode
          UKDistrictPostcode.upsert({
            postcode: dataPoint[1],
            address1: dataPoint[2],
            address2: dataPoint[5] === '' ? undefined : dataPoint[5]
          }).then(function() {
            callback();
          });
        }
      } else {
        callback();
      }
    }
  },
  fr: {
    uri: 'http://download.geonames.org/export/zip/FR.zip',
    zipFilename: __dirname + '/FR_postcodes.zip',
    filename: __dirname + '/FR.txt',
    parseFunction: function(postcode, callback) {
      var dataPoint = postcode.split('\t');
      if (dataPoint[1] && !dataPoint[1].hasSubstring('CEDEX')) {
        FRPostcode.upsert({
          postcode: dataPoint[1],
          address1: dataPoint[2],
          address2: dataPoint[5] === '' ? undefined : dataPoint[5],
          address3: dataPoint[7] === '' ? undefined : dataPoint[7],
          address4: dataPoint[3] === '' ? undefined : dataPoint[3],
          latitude: dataPoint[9] === '' ? undefined : dataPoint[9],
          longitude: dataPoint[10] === '' ? undefined : dataPoint[10]
        }).then(function() {
          callback();
        });
      } else {
        callback();
      }
    }
  },
  it: {
    uri: 'http://download.geonames.org/export/zip/IT.zip',
    zipFilename: __dirname + '/IT_postcodes.zip',
    filename: __dirname + '/IT.txt',
    parseFunction: function(postcode, callback) {
      var dataPoint = postcode.split('\t');
      if (dataPoint[1]) {
        //It's a full postcode
        ITPostcode.upsert({
          postcode: dataPoint[1],
          address1: dataPoint[2],
          address2: dataPoint[5] === '' ? undefined : dataPoint[5],
          address3: dataPoint[7] === '' ? undefined : dataPoint[7],
          address4: dataPoint[3] === '' ? undefined : dataPoint[3],
          latitude: dataPoint[9] === '' ? undefined : dataPoint[9],
          longitude: dataPoint[10] === '' ? undefined : dataPoint[10]
        }).then(function() {
          callback();
        });
      } else {
        callback();
      }
    }
  }
}

var populateRequestQueue = function(country, requestQueue, callback) {
  var zipFile = fs.createWriteStream(countryParams[country].zipFilename);
  console.log(countryParams[country].uri);
  var req = request(
    {
      method: 'GET',
      uri: countryParams[country].uri,
      timeout: 120000
    }
  )

  req.pipe(zipFile);
  req.on('end', function() {
    var zip = new AdmZip(countryParams[country].zipFilename);
    zip.extractAllTo(__dirname, true);
    logger.debug('Extraction complete!');

    var postcodes = fs.readFileSync(countryParams[country].filename).toString().split('\n');
    crawler.initialQueueLength += postcodes.length
    postcodes.forEach(function(postcode) {
      requestQueue.push({
        country: country,
        postcode: postcode
      }, function() {
        logger.debug('Finished parsing postcode %s for country %s', postcode, country);
      });
    });
    callback();
  });
}

var crawler = {
  longName: 'Postcode crawler (required for Zoopla and other services)',
  shortName: 'post',
  status: 'off',
  initialQueueLength: 0,
  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(params, callback) {
    var country = params.country;
    var postcode = params.postcode;
    countryParams[country].parseFunction(postcode, callback);
  }, 100),

  startCrawl: function(callback) {
    //country can be: uk, fr, it
    crawler.status = 'on';
    initialQueueLength = 0;
    crawler.requestQueue.empty = function() {
      crawler.status = 'off';
      console.log('Postcodes parsing complete. Deleting temporary files.')
      fs.unlinkSync(countryParams.uk.filename);
      fs.unlinkSync(countryParams.fr.filename);
      fs.unlinkSync(countryParams.it.filename);
      fs.unlinkSync(__dirname + '/readme.txt');
      callback();
    }

    logger.info('Started crawling postcodes');
    async.series([
      function(innerCallback) {
        populateRequestQueue('uk', crawler.requestQueue, function() {
          fs.unlinkSync(countryParams['uk'].zipFilename);
          innerCallback();
        });
      },
      function(innerCallback) {
        populateRequestQueue('it', crawler.requestQueue, function() {
          fs.unlinkSync(countryParams['it'].zipFilename);
          innerCallback();
        });
      },
      function(innerCallback) {
        populateRequestQueue('fr', crawler.requestQueue, function() {
          fs.unlinkSync(countryParams['fr'].zipFilename);
          innerCallback();
        });
      }
    ], function() {
      logger.info('Finished populating the queue.');
    });
  },

  pauseCrawl: function() {
    logger.info('Paused crawling postcodes');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling postcodes');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling postcodes');
    crawler.status = 'off';
    crawler.requestQueue.kill();
    ['uk', 'fr', 'it'].forEach(function(country) {
      var filename = countryParams[country].filename;
      var zipFilename = countryParams[country].zipFilename
      if (fs.existsSync(filename)) {
        fs.unlinkSync(filename);
      }
      if (fs.existsSync(zipFilename)) {
        fs.unlinkSync(zipFilename);
      }
    });
  }
};

module.exports = crawler;
