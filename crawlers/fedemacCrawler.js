var FEDEMACModels = require('../models/fedemacModels');
var City = FEDEMACModels.City;
var Company = FEDEMACModels.Company;
var CrawlSession = FEDEMACModels.CrawlSession;

var OPCityCrawler = require('../crawlers/overpassCityCrawler');

var rest = require('restler');
var htmlparser = require('htmlparser2');
var async = require('async');
require('../helpers.js');

var logger = require('../logger.js');

var requestVariables = {
  '__VIEWSTATE': '',
  '__VIEWSTATEGENERATOR': '',
  '__VIEWSTATEENCRYPTED': '',
  '__EVENTVALIDATION': '',
  'ScriptManager_TSM': ''
};
var cookies = '';

var checkCities = function(callback) {
  //Count distinct countues. If the number is > 10 -> no futher crawling needed.
  FEDEMACModels.sequelize.query('SELECT count(DISTINCT(country)) from cities').then(function(response) {
    var count = response[0][0].count;
    logger.debug('Distinct countries: %s', count);
    callback(count > 10 ? false : true);
  });
}

var getRequestVariables = function(data) {
  var sessionVariablesParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'input' && attributes.type === 'hidden') {
        if (attributes.name === '__VIEWSTATE' ||
            attributes.name === '__VIEWSTATEGENERATOR' ||
            attributes.name === '__VIEWSTATEENCRYPTED' ||
            attributes.name === '__EVENTVALIDATION') {
          requestVariables[attributes.name] = attributes.value;
        }
      }
      if (name === 'script' && attributes.hasOwnProperty('src') && attributes.src.startsWith('/Telerik.Web.UI.WebResource.axd')) {
        var parsedAttributes = attributes.src.split('_TSM_CombinedScripts_=');
        requestVariables['ScriptManager_TSM'] =  decodeURIComponent(parsedAttributes[1]).replace(/\+/g, ' ');
      }
    }
  });

  sessionVariablesParser.write(data);
  sessionVariablesParser.end();
}

var getCookies = function(callback) {
  rest.get('https://www.fedemac.eu/Find-Movers/Find-your-mover', {
    headers: {
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/601.2.7 (KHTML, like Gecko) Version/9.0.1 Safari/601.2.7',
      'Accept-Encoding': 'gzip, deflate'
    }
  })
  .on('complete', function(data, response) {
    getRequestVariables(data);
    cookies = response.headers['set-cookie'].splice(3, 3);
    callback();
  });
};

var performSearchQuery = function(requestParams, callback) {
  var crawlSessionId = requestParams.crawlSessionId;
  var city = requestParams.city.name;
  var country = requestParams.city.country;
  logger.debug("Performing search query for %s, %s. Request #%d.", city, country, crawler.requestCount);
  var requestCountry = country;
  if (country == 'United Kingdom') {
    requestCountry = 'Great Britain'
  }
  rest.post('https://www.fedemac.eu/Find-Movers/Find-your-mover', {
    headers: {
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/601.2.7 (KHTML, like Gecko) Version/9.0.1 Safari/601.2.7',
      'Accept-Encoding': 'gzip, deflate',
      'Cookie': cookies.join(';'),
    },
    multipart: true,
    data: {
      'StylesheetManager_TSSM': '',
      'ScriptManager_TSM': requestVariables['ScriptManager_TSM'],
      '__EVENTTARGET': 'dnn$ctr484$CompanySearch$btn_Search',
      '__EVENTARGUMENT': '',
      '__VIEWSTATE': requestVariables['__VIEWSTATE'],
      '__VIEWSTATEGENERATOR': requestVariables['__VIEWSTATEGENERATOR'],
      '__VIEWSTATEENCRYPTED': requestVariables['__VIEWSTATEENCRYPTED'],
      '__EVENTVALIDATION': requestVariables['__EVENTVALIDATION'],
      'dnn$SearchBoost1$hdnSearchTerms': 'Search',
      'dnn$SearchBoost1$hdnPortalFilterId': '',
      'dnn$SearchBoost1$hdnContentFilters': '',
      'dnn$ctr484$CompanySearch$UC_CompanyFilterMask$ddl_country': requestCountry,
      'dnn$ctr484$CompanySearch$UC_CompanyFilterMask$tb_city': city,
      'dnn$ctr484$CompanySearch$UC_CompanyFilterMask$tb_zipcode': '',
      'dnn$ctr484$CompanySearch$UC_CompanyFilterMask$ddl_radius': '30',
      'dnn$ctr484$CompanySearch$hf_kmx': '',
      'dnn$ctr484$CompanySearch$hf_kmy': '',
      'dnn$ctr484$CompanySearch$hf_userID': '-1',
      'ScrollTop': '',
      '__dnnVariable': ''
    }
  }).on('complete', function(data) {
    getRequestVariables(data);
    parseSearchResults(crawlSessionId, country, data, callback);
  });
};

var parseSearchResults = function(crawlSessionId, country, data, callback) {
  var insideCompanyDetails = false;
  var insideSpan = false;
  var insideWarning = false;
  var warningMessage = '';
  var divLevel = 0;
  var key = ''

  var companies = [];
  var company = {
    companyName: '',
    street: '',
    streetnumber: '',
    postcode: '',
    city: '',
    country: '',
    mobile: '',
    email: '',
    phone: '',
    web: '',
    services: []
  }

  var searchResultsParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'div') {
        if (attributes.class === 'list-group') {
          insideCompanyDetails = true;
        } else if (insideCompanyDetails) {
          divLevel++;
        }
      }

      if (insideCompanyDetails && name === 'span') {
        insideSpan = true;
        var splittedId = attributes.id.split('_');
        key = splittedId[splittedId.length - 2];
        if (key === 'companyService' && attributes.hasOwnProperty('title')) {
          key = '';
          company.services.push(attributes.title);
        }
      }

      if (insideCompanyDetails && name === 'a') {
        var splittedId = attributes.id.split('_');
        if (splittedId[splittedId.length - 2] === 'email') {
          company.email = attributes.href.split(':')[1];
        }
      }

      if (name === 'p' && attributes.class === 'bg-warning') {
        insideWarning = true;
      }

      if (name === 'span' && attributes.class === 'dnnModMessageHeading') {
        insideWarning = true;
      }
    },

    onclosetag: function(name) {
      if (insideCompanyDetails) {
        if (name === 'div') {
          divLevel--;
          if (divLevel < 0) {
            divLevel = 0;
            insideCompanyDetails = false;
            companies.push(company);
            company = {
              companyName: '',
              street: '',
              streetnumber: '',
              postcode: '',
              city: '',
              country: '',
              mobile: '',
              email: '',
              phone: '',
              web: '',
              services: []
            }
          }
        }

        if (name === 'span') {
          insideSpan = false;
        }
      } else {
        if (name === 'p' || name === 'span') {
          insideWarning = false;
        }
      }
    },

    ontext: function(text) {
      if (insideWarning) {
        logger.warn('Received a warning from FEDEMAC page: %s', text);
        warningMessage = text;
      }
      if (insideSpan && key !== '') {
        company[key] = text;
      }
    }
  });

  searchResultsParser.write(data);
  searchResultsParser.end();
  if (companies.length === 0 && warningMessage === '') {
    logger.warn('The companies array is empty!');
  }
  async.eachSeries(companies, function(company, companyCallback) {
    Company.upsert({
      'session_id': crawlSessionId,
      name: company.companyName,
      street: company.street,
      city: company.city,
      country: country,
      mobilePhone: company.mobile,
      workPhone: company.phone,
      email: company.email,
      website: company.web,
      services: company.services.join(', ')
    }).then(function() {
      companyCallback();
    });
  }, function() {
    callback();
  });
};

var populateRequestQueue = function(requestQueue, crawlSessionId) {
  City.findAll({
    where: {
      $or: {
        type: 'city',
        $and: {
          type: 'town',
          population: {
            $gt: 50000
          }
        }
      }
    },
    raw: true
  }).then(function(cities) {
    crawler.initialQueueLength = cities.length;
    cities.forEach(function(city) {
      requestQueue.push({
        city: city,
        crawlSessionId: crawlSessionId
      }, function() {
        logger.debug('Parsing complete for %s, %s', city.name, city. country);
      });
    });
  });
}

var crawler = {
  longName: 'FEDEMAC European Movers',
  shortName: 'fedemac',
  status: 'off',
  initialQueueLength: 0,
  requestCount: 0,
  crawlingSession: {},
  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(requestParams, callback) {
    if (crawler.requestCount % 350 === 0) {
      getCookies(function() {
        performSearchQuery(requestParams, callback);
      });
    } else {
      performSearchQuery(requestParams, callback);
    }
    crawler.requestCount++;
  }, 1),

  startCrawl: function() {
    crawler.status = 'on';
    crawler.requestQueue.drain = function() {
      crawler.status = 'off';
      crawler.crawlingSession = {};
      console.log('Parsing successful!');
    };

    logger.info('Started crawling FEDEMAC');
    //Create a new crawl session
    CrawlSession.create().then(function(session) {
      crawler.crawlingSession = session;
      checkCities(function(noCities) {
        if (noCities) {
          logger.warn('Cities from Overpass not found! Need to parse them first.');
          OPCityCrawler.startCrawl(function() {
            populateRequestQueue(crawler.requestQueue, session.id);
          });
        } else {
          populateRequestQueue(crawler.requestQueue, session.id);
        }
      });
    });
  },

  pauseCrawl: function() {
    logger.info('Paused crawling FEDEMAC');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling FEDEMAC');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling FEDEMAC');
    crawler.status = 'off';
    crawler.crawlSession = {};
    crawler.requestQueue.kill();
  }
};

module.exports = crawler;
