var SelogerModels = require('../models/selogerModels');
var Property = SelogerModels.Property;
var CrawlSession = SelogerModels.CrawlSession;

var PostcodeModels = require('../models/postcodeModels');
var FRPostcode = PostcodeModels.FRPostcode;

var rest = require('restler');
var htmlparser = require('htmlparser2');
var async = require('async');
require('../helpers.js');
var logger = require('../logger.js');

var cookies = [];

var checkPostcodes = function(callback) {
  //Count district postcodes. If the number is > 1000 -> no futher crawling needed.
  PostcodeModels.sequelize.query('SELECT count(DISTINCT(postcode)) from fr_postcodes').then(function(response) {
    var count = response[0][0].count;
    logger.debug('Distinct district postcodes: %s', count);
    callback(count > 1000 ? false : true);
  });
}

var populateRequestQueue = function(requestQueue, crawlSessionId) {
  FRPostcode.findAll({
    attributes: {
      include: ['postcode', 'address_1']
    },
    raw: true
  }).then(function(postcodes) {
    crawler.initialQueueLength = postcodes.length;
    postcodes.forEach(function(postcode) {
      requestQueue.push({
        crawlSessionId: crawlSessionId,
        city: postcode.address1,
        postcode: postcode.postcode,
      }, function() {
        logger.debug('Finished property parsing for %s (%s)', postcode.address1, postcode.postcode)
      });
    });
  });
}

var requestSelogerId = function(city, postcode, callback) {
  var selogerId = {
    code: 0,
    prefix: 'ci'
  };

  //Way to handle Paris, Marseille and Lyon arrondissements starting with 0 (Paris 0x, Marseille 0x, Lyon 0x).
  if (city.startsWith('Paris ') || city.startsWith('Lyon ') || city.startsWith('Marseille ')) {
    splittedCity = city.split(' ');
    if (splittedCity[1].startsWith('0')) {
      city = splittedCity[0] + ' ' + splittedCity[1][1];
    }
  }

  rest.get('http://www.seloger.com/ajax,location_list.json', {
    timeout: 10000,
    query: {
      term: city
    },
  }).on('success', function(data, response) {
    data.forEach(function(node) {
      if (node.label === 'Villes' || node.label === 'Ville' || node.label === 'Villes les plus populaires') {
        node.values.forEach(function(suggestion) {
            //Check the postcode & the city name.
          if (suggestion.value.hasSubstring(postcode)) {
            selogerId.code = suggestion.code;
            selogerId.prefix = suggestion.param;
          } else if (city.length > 17 && suggestion.value.startsWith(city.toUpperCase().substring(0, 17))) {
            selogerId.code = suggestion.code;
            selogerId.prefix = suggestion.param;
          }
        });
      }
    });
    callback(selogerId);
  }).on('error', function() {
    logger.warn('[Seloger] Connection reset in requestSelogerId!');
    requestSelogerId(city, postcode, callback);
  }).on('timeout', function() {
    logger.warn('[Seloger] Timeout in requestSelogerId!');
    requestSelogerId(city, postcode, callback);
  });
}

var saveProperty = function(property) {
  if (property.type && property.area) {
    Property.create(property);
  }
}

var requestProperties = function(requestParams, callback) {
  var crawlSessionId = requestParams.crawlSessionId;
  var city = requestParams.city;
  var postcode = requestParams.postcode;
  var selogerId = requestParams.selogerId;

  var url = 'http://www.seloger.com/list.htm?{0}={1}&idtt=2&idtypebien=2,13,14,1,9'.format(selogerId.prefix, selogerId.code);
  rest.get(url, {
    headers: {
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:43.0) Gecko/20100101 Firefox/43.0',
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'Accept-Language': 'en-US,en;q=0.5',
      'Accept-Encoding': 'gzip, deflate',
      'Connection': 'keep-alive',
      'Cookie': cookies.join(';')
    },
    timeout: 10000
  }).on('success', function(data, response) {
    if (parseError(data)) {
      logger.warn('[Seloger] Recived error message at requestProperties (No cookies?).');
      cookies = response.headers['set-cookie'];
      requestProperties(requestParams, callback);
      return;
    }
    //Parse 1st page
    parseProperties(data, requestParams);
    //Calculate number of pages
    var numberOfPages = parseNumberOfPages(data);
    //Parse each page
    if (numberOfPages > 1) {
      var currentPage = 2;
      async.whilst(
        function() {return currentPage <= numberOfPages},
        function(requestCallback) {
          requestNextPage(selogerId, currentPage, function(data) {
            parseProperties(data, requestParams);
            currentPage++;
            requestCallback();
          });
        },
        function() {
          callback();
        }
      );
    } else {
      callback();
    }
  }).on('timeout', function() {
    logger.warn('[Seloger] Timeout in requestProperties!');
    requestProperties(requestParams, callback);
  });
}

var requestNextPage = function(selogerId, page, callback) {
  var url = 'http://www.seloger.com/list.htm?{0}={1}&idtt=2&idtypebien=2,13,14,1,9&LISTING-LISTpg={2}'.format(selogerId.prefix, selogerId.code, page);
  rest.get(url, {
    headers: {
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:43.0) Gecko/20100101 Firefox/43.0',
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'Accept-Language': 'en-US,en;q=0.5',
      'Accept-Encoding': 'gzip, deflate',
      'Connection': 'keep-alive',
      'Cookie': cookies.join(';')
    },
    timeout: 10000
  }).on('complete', function(data) {
    callback(data);
  }).on('timeout', function() {
    logger.warn('[Seloger] Timeout in requestNextPage!');
    requestNextPage(selogerId, page, callback);
  });
}

var parseError = function(data) {
  var error = false;
  var parser = new htmlparser.Parser({
    ontext: function(text) {
      if (text.hasSubstring('Une erreur s\'est produite')) {
        error = true;
      }
    }
  });

  parser.write(data);
  parser.end();
  return error;
}

var parseProperties = function(data, requestParams) {
  var insideListingInfos = false;
  var insidePrice = false;
  var insideInnerDiv = false;
  var insidePropertyList = false;

  var propertyDetails = {
    type: undefined,
    price: undefined,
    area: undefined,
    bedrooms: undefined,
  }

  var parser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'div' && attributes.class === 'listing_infos') {
        insideListingInfos = true;
      }

      if (insideListingInfos) {
        if (name === 'div') {
          insideInnerDiv = true;
        } else if (name === 'a') {
          if (attributes.hasOwnProperty('title') && attributes.title) {
            //Check the title, we have found the property's type!
            var titleText = attributes.title.toLowerCase();
            if (titleText.hasSubstring('appartement')) {
              propertyDetails.type = 'Appartement';
            } else if (titleText.hasSubstring('maison') || titleText.hasSubstring('villa')) {
              propertyDetails.type = 'Maison';
            } else if (titleText.hasSubstring('loft') || titleText.hasSubstring('atelier') || titleText.hasSubstring('surface')) {
              propertyDetails.type = 'Loft';
            } else if (titleText.hasSubstring('château')) {
              propertyDetails.type = 'Château';
            } else if (titleText.hasSubstring('hôtel particulier')) {
              propertyDetails.type = 'Hôtel particulier';
            }
          } else if (attributes.hasOwnProperty('class') && attributes.class === 'amount') {
            insidePrice = true;
          }
        }
      }

      if (name === 'ul' && attributes.class === 'property_list') {
        insidePropertyList = true;
      }
    },

    onclosetag: function(name) {
      if (name === 'div') {
        if (insideInnerDiv) {
          insideInnerDiv = false;
        } else if (insideListingInfos) {
          insideListingInfos = false;
          //Save property details here!
          saveProperty({
            'session_id': requestParams.crawlSessionId,
            postcode: requestParams.postcode,
            city: requestParams.city,
            type: propertyDetails.type,
            price: propertyDetails.price,
            area: propertyDetails.area,
            bedrooms: propertyDetails.bedrooms
          });
          //console.log(propertyDetails);
          propertyDetails = {
            type: undefined,
            price: undefined,
            area: undefined,
            bedrooms: undefined,
          }
        }
      } else if (insidePrice && name === 'a') {
        insidePrice = false;
      } else if (insidePropertyList && name === 'ul') {
        insidePropertyList = false;
      }
    },

    ontext: function(text) {
      if (insidePrice) {
        //It's a price
        propertyDetails.price = parseInt(text.replace(/\s/g, '').slice(0, -1));
      }
      if (insidePropertyList) {
        if (text.hasSubstring('p')) {
          //It's number of bedroomos
          propertyDetails.bedrooms = parseInt(text.split(' ')[0])
        } else if (text.hasSubstring('m')) {
          //It's the area
          propertyDetails.area = parseFloat(text.split(' ')[0].replace(',', '.'));
        }
      }
    }
  });

  parser.write(data);
  parser.end();
}

var parseNumberOfPages = function(data) {
  insidePagination = false;
  var totalPages = 1;

  var parser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'p' && attributes.class === 'pagination_result_number') {
        insidePagination = true;
      }
    },

    onclosetag: function(name) {
      if (insidePagination && name === 'p') {
        insidePagination = false;
      }
    },

    ontext: function(text) {
      if (insidePagination) {
        totalPages = parseInt(text.split('/')[1]);
      }
    }
  });

  parser.write(data);
  parser.end();
  return totalPages;
}

var crawler = {
  longName: 'Seloger property details crawler',
  shortName: 'seloger',
  status: 'off',
  initialQueueLength: 0,
  crawlingSession: {},

  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(requestParams, callback) {
    requestSelogerId(requestParams.city, requestParams.postcode, function(selogerId) {
      if (selogerId.code === 0) {
        logger.warn('Prefix for %s (%s) not found!', requestParams.city, requestParams.postcode);
        callback();
      } else {
        requestParams.selogerId = selogerId;
        requestProperties(requestParams, callback);
      }
    });
  }, 1),

  startCrawl: function() {
    crawler.status = 'on';

    crawler.requestQueue.drain = function() {
      crawler.status = 'off';
      crawler.crawlingSession = {};
      logger.info('Seloger crawling successful!');
    };

    logger.info('Started crawling Seloger');
    //Create a new crawl session
    CrawlSession.create().then(function(session) {
      crawler.crawlingSession = session;
      checkPostcodes(function(noPostcodes) {
        if (noPostcodes) {
          logger.warn('Postcodes not found! Starting PostcodeCrawler');
          PostcodeCrawler.startCrawl(function() {
            populateRequestQueue(crawler.requestQueue, session.id);
          });
        } else {
          populateRequestQueue(crawler.requestQueue, session.id);
        }
      });
    });
  },

  pauseCrawl: function() {
    logger.info('Paused crawling Seloger');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling Seloger');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling Seloger');
    crawler.status = 'off';
    crawler.crawlSession = {};
    crawler.requestQueue.kill();
  }
}

module.exports = crawler;
