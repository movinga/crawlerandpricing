var BaggageModels = require('../models/baggagenlModels');
var CrawlSession = BaggageModels.CrawlSession;
var AirRate = BaggageModels.AirRate;
var SeaRate = BaggageModels.SeaRate;
var CourierRate = BaggageModels.CourierRate;
var RoadRate = BaggageModels.RoadRate;

var rest = require('restler');
var htmlparser = require('htmlparser2');
var async = require('async');
var logger = require('../logger.js');
require('../helpers.js');

var requestCountries = function(method, next) {
  var countries = []
  var continentsArray = [
    'North America',
    'Central America',
    'South America',
    'Europe',
    'Middle East',
    'Africa',
    'Asia',
    'Oceania'
  ];
  if (method === 'road') {
    requestCountry(method, countries, '', function() {
      next(countries);
    });
  } else {
    async.eachSeries(continentsArray, function(continent, callback) {
      requestCountry(method, countries, continent, callback);
    }, function() {
      next(countries);
    });
  }
}

var requestCountry = function(method, countries, continent, callback) {
  var url = (method === 'road') ?
    'http://www.baggage.nl/en/pages/calc/road1.php' :
    'http://www.baggage.nl/en/pages/calc/{0}1.php?continent={1}'.format(method, continent)
  rest.get(url, {
    timeout: 10000
  }).on('complete', function(data) {
    parseCountries(method, countries, data);
    callback();
  }).on('timeout', function() {
    logger.warn('[Baggage.nl] Timeout reached in requestCountry');
    requestCountry(method, countries, continent, callback);
  });
}

var parseCountries = function(method, countries, data) {
  var insideCountryLink = false
  var href = '';

  var parser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (method === 'road') {
        if (name === 'a' && attributes.href.startsWith('road1.php?')) {
          insideCountryLink = true;
          href = attributes.href;
        }
      } else {
        if (name === 'a' && attributes.href.startsWith('{0}2.php?'.format(method))) {
          insideCountryLink = true;
          href = attributes.href;
        }
      }
    },

    onclosetag: function(name) {
      if (insideCountryLink && name === 'a') {
        insideCountryLink = false;
      }
    },

    ontext: function(text) {
      if (insideCountryLink) {
        countries.push({
          name: text,
          href: href
        });
      }
    }
  });

  parser.write(data);
  parser.end();
}

var requestDestinations = function(method, countries, next) {
  logger.info('Started requesting destinations for method %s', method);
  logger.debug('Country list length - %s', countries.length);
  var destinations = [];
  async.eachSeries(countries, function(country, callback) {
    requestDestination(method, destinations, country, callback);
  }, function() {
    next(destinations);
  });
}

var requestDestination = function(method, destinations, country, callback) {
  var baseURL = 'http://www.baggage.nl/en/pages/calc/';
  var url = (method === 'road') ?
    baseURL + 'road1.php?destination={0}'.format(country.name) :
    baseURL + country.href
    //baseURL + '{0}2.php?country={1}'.format(method, countryName)
  rest.get(url, {
    timeout: 10000
  }).on('complete', function(data) {
    parseDestinations(method, destinations, country, data);
    callback();
  }).on('timeout', function() {
    logger.warn('[Baggage.nl] Timeout reached in requestDestination');
    requestDestination(method, destinations, country, callback);
  });
}

var parseDestinations = function(method, destinations, country, data) {
  var insideDestinationTag = false
  destinationId = '';

  var roadParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'input' && attributes.type === 'hidden' && attributes.name === 'id') {
        //We have found the 1st variant.
        destinations.push({
          countryName: country.name,
          name: "",
          id: attributes.value
        });
      } else if (name === 'a' && attributes.href.startsWith('road2.php?id=')) {
        insideDestinationTag = true;
        destinationId = attributes.href.split('=')[1];
      }
    },

    onclosetag: function(name) {
      if (insideDestinationTag && name === 'a') {
        insideDestinationTag = false;
      }
    },

    ontext: function(text) {
      if (insideDestinationTag) {
        destinations.push({
          countryName: country.name,
          name: text,
          id: destinationId
        });
      }
    }
  });

  var courierParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'input' && attributes.type === 'hidden') {
        if (attributes.name === 'id') {
          destinationId = attributes.value;
        } else if (attributes.name === 'zone') {
          destinations.push({
            countryName: country.name,
            name: "",
            zone: attributes.value,
            id: destinationId
          });
        }
      }
    }
  });

  var defaultParser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (name === 'option') {
        insideDestinationTag = true;
        destinationId = attributes.value;
      }
    },

    onclosetag: function(name) {
      if (insideDestinationTag && name === 'option') {
        insideDestinationTag = false;
      }
    },

    ontext: function(text) {
      if (insideDestinationTag) {
        destinations.push({
          countryName: country.name,
          name: text,
          id: destinationId
        });
      }
    }
  });

  switch (method) {
    case 'road':
      roadParser.write(data);
      roadParser.end();
      break;
    case 'courier':
      courierParser.write(data);
      courierParser.end();
    default:
      defaultParser.write(data);
      defaultParser.end();
  }
}

var populateRequestQueue = function(requestQueue, crawlSession) {
  //Iterate through all possible methods
  logger.info('Started populating request queue.');
  ['air', 'sea', 'courier', 'road'].forEach(function(method) {
    requestCountries(method, function(countries) {
      requestDestinations(method, countries, function(destinations) {
        logger.debug('Destinations length - %s', destinations.length);
        crawler.initialQueueLength += destinations.length;
        destinations.forEach(function(destination) {
          requestQueue.push({
            crawlSession: crawlSession,
            method: method,
            destination: destination
          }, function() {
            logger.debug('Finished requesting %s rates for destination %s, %s', method, destination.name, destination.countryName);
          });
        });
      });
    });
  });
}

var requestRates = function(requestParams, next) {
  var weightsArray = [];

  switch (requestParams.method) {
    case 'air':
      weightsArray = ['5', '10', '20', '50', '100', '250', '500', '1000'];
      break;
    case 'sea':
      weightsArray = ['1', '1.5', '2.0', '2.5', '3.0', '4.0', '5.0', '6.0'];
      break;
    default:
      weightsArray = ['1', '5', '10', '20', '50', '100', '250', '500'];
      break;
  }

  async.eachSeries(weightsArray, function(weight, callback) {
    requestRate(requestParams, weight, '1000', callback);
  }, function() {
    next();
  });
}

var requestRate = function(requestParams, weight, value, callback) {
  var crawlSession = requestParams.crawlSession;
  var method = requestParams.method;
  var countryName = requestParams.destination.countryName;
  var destination = requestParams.destination;
  var baseURL = 'http://www.baggage.nl/en/pages/calc/';

  var firstParam = (method === 'sea') ? 'cubic' : 'weight';
  var zoneParam = (method === 'courier') ? '&zone={0}'.format(destination.zone) : '';

  // url = baseURL + {method}2.php?id={id}{zoneParam}&{weightParam}={weight}&value=1000&submit=Calculate
  var url = baseURL + '{0}2.php?id={1}{2}&{3}={4}&value={5}&submit=Calculate'.format(
    method,
    destination.id,
    zoneParam,
    firstParam,
    weight,
    value
  );
  rest.get(url, {
    timeout: 10000
  }).on('complete', function(data) {
    var rate = parseRate(data);
    saveRate(method, countryName, destination.name, weight, value, rate, crawlSession);
    callback();
  }).on('timeout', function() {
    logger.warn('[Baggage.nl] Reached timeout in requestRate!');
    requestRate(requestParams, weight, value, callback);
  });
}

var saveRate = function(method, countryName, destinationName, weight, value, rate, crawlSession) {
  var model = undefined
  switch (method) {
    case 'air':
      model = AirRate;
      break;
    case 'sea':
      model = SeaRate;
      break;
    case 'courier':
      model = CourierRate;
      break;
    case 'road':
      model = RoadRate;
      break;
  }
  var rate = {
    country: countryName,
    zone: destinationName.replace(/;/g, ','),
    value: parseFloat(value),
    rate: parseFloat(rate),
    'session_id': crawlSession
  }
  var firstParam = method === 'sea' ? 'volume' : 'weight'
  rate[firstParam] = parseFloat(weight);
  model.create(rate);
}

var parseRate = function(data) {
  var insideFinalRow = false;
  var insideFinalCol = false;
  var insideFinalTag = false;
  var rate = 0.00;

  var parser = new htmlparser.Parser({
    onopentag: function(name, attributes) {
      if (insideFinalRow) {
        if (insideFinalCol && name === 'b') {
          insideFinalTag = true;
        } else if (name === 'td' && attributes.align === 'right') {
          insideFinalCol = true;
        }
      }
    },

    onclosetag: function(name) {
      if (insideFinalRow) {
        if (name === 'b') {
          insideFinalTag = false;
        } else if (name === 'td') {
          insideFinalCol = false;
        } else if (name === 'tr') {
          insideFinalRow = false;
        }
      }
    },

    ontext: function(text) {
      if (text === 'Grand total:') {
        insideFinalRow = true;
      }
      if (insideFinalTag) {
        rate = text.replace(/\./g, '').replace(/,/g, '.');
      }
    }
  });

  parser.write(data);
  parser.end();
  return rate;
}

var crawler = {
  longName: 'Baggage.nl freight rates crawler',
  shortName: 'baggagenl',
  status: 'off',
  initialQueueLength: 0,
  crawlingSession: {},

  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(requestParams, callback) {
    requestRates(requestParams, callback);
  }, 1),

  startCrawl: function() {
    crawler.status = 'on';

    crawler.requestQueue.drain = function() {
      crawler.status = 'off';
      crawler.crawlingSession = {};
      logger.info('Baggage.nl crawling successful!');
    };

    logger.info('Started crawling Baggage.nl');
    //Create a new crawl session
    CrawlSession.create().then(function(session) {
      crawler.crawlingSession = session;
      populateRequestQueue(crawler.requestQueue, session.id);
    });
  },

  pauseCrawl: function() {
    logger.info('Paused crawling Baggage.nl');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling Baggage.nl');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling Baggage.nl');
    crawler.status = 'off';
    crawler.crawlSession = {};
    crawler.requestQueue.kill();
  }
}

module.exports = crawler;
