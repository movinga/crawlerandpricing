var WFRModels = require('../models/wfrModels');
var City = WFRModels.City;
var Rate = WFRModels.Rate;
var CrawlSession = WFRModels.CrawlSession;

var fs = require('fs');
var rest = require('restler');
var async = require('async');
require('../helpers.js');

var logger = require('../logger.js');

var populateCityNames = function(success) {
  rest.get('http://worldfreightrates.com/calculator/ports', {
    parser: rest.parsers.json,
    query: {
      term: ' '
    }
  }).on('complete', function(data) {
    async.eachSeries(data, function(row, callback) {
      //First part - city, second part - country
      var splittedLabel = row.label.split(", ");
      var cityName = splittedLabel[0], countryName = splittedLabel[1];
      //Let's push the data to the database
      City.upsert({
        id: row.id,
        city: cityName,
        country: countryName
      }).then(function(city) {
          callback();
      });
    }, function(err) {
      success();
    });
  });
};

var fetchPortIdList = function(success) {
  // Read the portList.txt file and copy a list of ports to the hash array.
  var portList = []
  var portListString = fs.readFileSync(__dirname + '/portList.txt').toString().split('\n')
  async.eachSeries(portListString, function(cityName, callback) {
    City.findOne({where: {city: cityName}, raw: true}).then(function(city) {
      if (city) {
        portList.push(city);
        callback();
      } else {
        callback("City not found: " + cityName);
      }
    });
  }, function() {
    success(portList);
  });
};

var performRequest = function(requestQueue, requestParams, callback) {
  rest.get('http://worldfreightrates.com/en/calculator/ocean/rate', {
    parser: rest.parsers.json,
    timeout: 10000,
    query: {
      fromId: requestParams.from.id,
      toId: requestParams.to.id,
      oceanType: 'FCL',
      commodityName: 'Furniture (Used)',
      commodityValue: 50000,
      includeInsurance: requestParams.insurance,
      includeReefer: false,
      includeHazardous: false,
      containerSize: requestParams.size,
      _: Date.now()
    }
  }).on('complete', function(data, response) {
    if (data.success) {
      //Save the quote to the database
      Rate.create({
        'session_id': requestParams.sessionId,
        fromCity: requestParams.from.city,
        fromCountry: requestParams.from.country,
        toCity: requestParams.to.city,
        toCountry: requestParams.to.country,
        containerSize: requestParams.size,
        insurance: requestParams.insurance,
        rate: data.rate.replace(',', '')
      }, {
        raw: true
      });
    } else {
      logger.error('Worldfreightrates error! Failed request. Look for data below');
      logger.error(data);
    }
    callback();
  }).on('timeout', function(data) {
    logger.error('Worldfreightrates error! Reached request timeout');
    requestQueue.push(requestParams, function() {
      logger.debug('End of request.');
    });
    callback();
  });
}

var populateRequestQueue = function(requestQueue, sessionId, portList) {
  logger.info('portList length - %d', portList.length);
  logger.info('expected number of records - %d', portList.length * (portList.length - 1) * 6);
  portList.forEach(function(port1, index1, array1) {
    array1.forEach(function(port2, index2, array2) {
      if (index1 !== index2) {
        [20, 40, 45].forEach(function(size) {
          [true, false].forEach(function(insurance) {
            var requestParams = {
              sessionId, sessionId,
              from: port1,
              to: port2,
              insurance: insurance,
              size: size
            }
            requestQueue.push(requestParams, function() {
              logger.debug('End of request.');
            });
          });
        });
      }
    });
  });
};

var crawler = {
  longName: 'worldfreightrates.com',
  shortName: 'wfr',
  status: 'off',
  initialQueueLength: 0,
  crawlingSession: {},
  progress: function() {
    if (crawler.status !== 'off' && crawler.initialQueueLength > 0) {
      var completed = crawler.initialQueueLength - crawler.requestQueue.length();
      return Math.round((completed / crawler.initialQueueLength) * 100);
    } else {
      return 0
    }
  },

  requestQueue: async.queue(function(requestParams, callback) {
    performRequest(crawler.requestQueue, requestParams, callback);
  }, 1),

  startCrawl: function() {
    crawler.status = 'on';
    crawler.requestQueue.drain = function() {
      crawler.status = 'off';
      crawler.crawlingSession = {};
      console.log('Parsing successful!');
    };

    logger.info('Started crawling worldfreightrates');
    //Create a new crawl session
    CrawlSession.create().then(function(session) {
      crawler.crawlingSession = session;
      //Repopulate city names
      populateCityNames(function() {
        //Fetch focus port ids from the portList.txt file
        fetchPortIdList(function(portList) {
          crawler.initialQueueLength = portList.length * (portList.length - 1) * 6;
          populateRequestQueue(crawler.requestQueue, session.id, portList);
        });
      });
    });
  },

  pauseCrawl: function() {
    logger.info('Paused crawling worldfreightrates');
    crawler.status = 'paused';
    crawler.requestQueue.pause();
  },

  resumeCrawl: function() {
    logger.info('Resumed crawling worldfreightrates');
    crawler.status = 'on';
    crawler.requestQueue.resume();
  },

  stopCrawl: function() {
    logger.info('Stopped crawling worldfreightrates');
    crawler.status = 'off';
    crawler.crawlSession = {};
    crawler.requestQueue.kill();
  }
};

module.exports = crawler;
