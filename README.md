#Movinga Web Crawler

Web crawler for several websites:
  * [World Freight Rates](http://worldfreightrates.com/en/freight)
  * [FEDEMAC (European movers)](https://www.fedemac.eu/Find-Movers/Find-your-mover)
  * [International Association of Movers](https://online.iamovers.org/iamssa/iamcenssacustlkup.query_page)
  * [National Guild of Removers and Storers](http://www.ngrs.co.uk/find-a-remover?task=search&sid=54)
  * [Zoopla area stats](http://www.zoopla.co.uk/market/uk/)
  * [Immobiliare property details](http://www.immobiliare.it)
  * [Seloger property details](http://www.seloger.com)
  * [Baggage.nl freight rates](http://baggage.nl/english.php)
  * [ADAC car details](https://www.adac.de/infotestrat/autodatenbank/suchergebnis.aspx)
  * [Hamburg Süd THC rates](https://ecom.hamburgsud.com/ecom/en/ecommerce_portal/tariffs_and_surcharges/thc_calc/ep_thc_calculator.xhtml?lang=EN)
  * [Primelocation.com property details](http://www.primelocation.com)
  * [Baggagehub.com freight rates](http://www.baggagehub.com/get-a-quote)
  * [Paruvendu property details](http://www.paruvendu.fr)
Several helper crawlers are included as well:
  * Cities and towns crawler (for FEDEMAC crawler)
  * United Kingdom, France and Italy postcode crawler (required for all property -related crawlers)

##Installation instructions:

Working PostgreSQL database is required for this application. You can change the connection string
(url, port, username, password) for the database in `./settings.js` file. You need to manually create
one database per each crawler. The respective tables will be created automatically by
the ORM solution [Sequelize](http://docs.sequelizejs.com/en/latest/).
You can find the database's names in the example SQL code below:

```sql
CREATE ROLE movinga_crawler PASSWORD 'keyboardcat' NOSUPERUSER CREATEDB NOCREATEROLE INHERIT LOGIN;
CREATE DATABASE worldfreightrates OWNER movinga_crawler ENCODING 'UTF8';
CREATE DATABASE ngrs OWNER movinga_crawler ENCODING 'UTF8';
CREATE DATABASE iam OWNER movinga_crawler ENCODING 'UTF8';
CREATE DATABASE fedemac OWNER movinga_crawler ENCODING 'UTF8';
CREATE DATABASE zoopla OWNER movinga_crawler ENCODING 'UTF8';
CREATE DATABASE postcodes OWNER movinga_crawler ENCODING 'UTF8';
CREATE DATABASE immobiliare OWNER movinga_crawler ENCODING 'UTF8';
CREATE DATABASE seloger OWNER movinga_crawler ENCODING 'UTF8';
CREATE DATABASE baggagenl OWNER movinga_crawler ENCODING 'UTF8';
CREATE DATABASE adac OWNER movinga_crawler ENCODING 'UTF8';
CREATE DATABASE hamsud OWNER movinga_crawler ENCODING 'UTF8';
CREATE DATABASE primelocation OWNER movinga_crawler ENCODING 'UTF8';
CREATE DATABASE baggagehub OWNER movinga_crawler ENCODING 'UTF8';
CREATE DATABASE paruvendu OWNDER movinga_crawler ENCODING 'UTF8';
```  

Other dependencies are listed below:
* [Node.js](https://nodejs.org/en/) as a runtime environment and npm as a package manager
(Installation details are available [here](https://nodejs.org/en/download/package-manager/))
* [PM2](http://pm2.keymetrics.io) as a process manager for Node.js applications (`npm install pm2 -g`)

After the dependencies installation is complete, download the required modules bu running `npm install` command.
Finally, start the application by running the `pm2 start app.js` command.

The application will listen to the address `http:///0.0.0.0:3001`. You will need a
reverse proxy to handle initial requests to the application.
Example configuration file for Nginx server is listed below (Report generation
process for some crawlers can take a while, that's why it's required to set
a big request timeout):

```
server {
  listen 80;

  server_name <INSERT_DOMAIN_NAME_HERE>;

  location / {
    proxy_pass http://0.0.0.0:3001;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
    proxy_connect_timeout       300;
    proxy_send_timeout          300;
    proxy_read_timeout          300;
  }
}
```

## Usage hints

Crawler scheduling is done in a cron-like way. Crawling start time preferences are located in `./settings.js` file.
You can find the configuration guidelines in the comments.

Logging is enabled for this application. There are 3 levels of logging: error, info, debug.
If anything goes wrong, check these logs first. Right now there is no limit on log
size, that's why it's recommended to clean/backup log files at least once a month.
