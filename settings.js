module.exports = {
  // PostgreSQL database settings
  database: {
    username: 'movinga_crawler',
    password: 'PauMWurJ', //Change to 'testtest' on production
    host: 'localhost',
    port: '5432'
  },
  /*
  This crawler uses cron-like format for scheduling:

  *    *    *    *    *    *
  ┬    ┬    ┬    ┬    ┬    ┬
  │    │    │    │    │    |
  │    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
  │    │    │    │    └───── month (1 - 12)
  │    │    │    └────────── day of month (1 - 31)
  │    │    └─────────────── hour (0 - 23)
  │    └──────────────────── minute (0 - 59)
  └───────────────────────── second (0 - 59, OPTIONAL)

  more information - https://github.com/node-schedule/node-schedule/wiki/Cron-style-Scheduling
  */
  crawlers: {
    wfr:         '59 23 * * 6',    //Every Saturday  at 23:59 server time
    ngrs:        '50 23 * * 6',    //Every Saturday  at 23:50 server time
    iam:         '00 23 * * 6',    //Every Saturday  at 23:00 server time
    fedemac:     '59 00 * * 1',    //Every Monday    at 00:59 server time
    overpass:    '00 00 * * 1',    //Every Monday    at 00:00 server time
    zoopla:      '00 03 * * 6',    //Every Saturday  at 03:00 server time
    post:        '00 00 * * 6',    //Every Saturday  at 00:00 server time
    immobiliare: '00 04 * * 6',    //Every Saturday  at 04:00 server time
    seloger:     '00 05 * * 6',    //Every Saturday  at 05:00 server time
    baggagenl:   '00 02 * * 6',    //Every Saturday  at 02:00 server time
    adac:        '00 20 * * 6',    //Every Saturday  at 20:00 server time
    hamsud:      '00 19 * * 6',    //Every Saturday  at 19:00 server time
    pl:          '00 00 * * 3',    //Every Wednesday at 00:00 server time
    bhub:        '00 00 * * 4',    //Every Thursday  at 00:00 server time
    paruvendu:   '00 03 * * 4',    //Every Thursday  at 03:00 server time
  },

  cleardb   : '00 00 * * 2'  //Every Tuesday  at 00:00 server time
}
