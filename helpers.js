String.prototype.format = function () {
  var args = [].slice.call(arguments);
  return this.replace(/(\{\d+\})/g, function (a){
      return args[+(a.substr(1,a.length-2))||0];
  });
};

String.prototype.startsWith = function(prefix) {
  return this.slice(0, prefix.length) == prefix;
};

String.prototype.hasSubstring = function(substring) {
  return this.indexOf(substring) > -1;
};

String.prototype.toCamelCase = function() {
  return this.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
    return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
  }).replace(/[\s\.\-\/\\\(\)\%]/g, '');
};

String.prototype.replaceUmlaute = function() {
  return this.replace(/ä/g, 'ae').replace(/Ä/g, 'Ae').replace(/ö/g, 'oe').replace(/Ö/g, 'Oe').replace(/ü/g, 'ue').replace(/Ü/g, 'Ue').replace(/ß/g, 'ss');
};

Number.prototype.round = function(places) {
  return +(Math.round(this + "e+" + places)  + "e-" + places);
};
